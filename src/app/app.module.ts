import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from "@angular/router";

import { routes } from './app.routing';
import { StatesModule } from './states';
import { HomeGuard } from './guards/home.guard';
import { AuthGuard } from './guards/auth.guard';
import { SignupGuard } from './guards/signup.guard';




import { WebSocketService } from './websockets';
import { NotificationService } from './services/notifications/notifications.service';

import { SimpleNotificationsModule } from "angular2-notifications";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth/auth.interceptor';
import { SidebarService } from './components/sidebar/services/sidebar.service';
import { SessionService } from './services/sessions/sessions.service';
import { BsystemsRouteGuard } from './guards/bguard';






@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    // Ng2SearchPipeModule,
    StatesModule,
    RouterModule.forRoot(routes, { useHash: true }),
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot()
  ],
  providers: [
    AuthGuard,
    HomeGuard,
    SignupGuard,
    BsystemsRouteGuard,
    WebSocketService,
    NotificationService,
    SidebarService,
    SessionService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
