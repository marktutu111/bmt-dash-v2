
export interface InterfaceDashboard {
    totalRegistrations?: string;
    totalTransfers?: string;
    successfulTransfers?: string;
    failedTransfers?: string;
}



export interface DashboardInterfaceModel {
    paid?: number;
    pending?: number;
    failed?: number;
    total?: number;
}