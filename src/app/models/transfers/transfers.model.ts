import * as moment from "moment";
import { Store } from "@ngxs/store";
import {SetSeenForTransnfer } from "src/app/states/transfer/transfer.actions";



export interface TransferInterfaceModel {
    _id?:string;
    card_processed?: boolean;
    card_transaction_id?: string;
    userId?: string;
    totalReceived?: number;
    transactionId?: string;
    tfailed?: number;
    tsuccessful?: number;
    total_refund?: number;
    account_type?: string;
    charge?: number;
    totalBankTransferAmount?:number;
    totalBankTransfers?:number;
    totalMomoTransferAmount?:number;
    totalMomoTransfers?:number;
    account_number?:number;
    account_issuer?:number;
    account_name?:string;
    aggregator?: string;
    creditAggregator?: string;
    account_voucher?:string;
    cardNumber?: string;
    amount?: string;
    totalTransferAmount?: number;
    type?: string;
    status?: string;
    total_transfer_count?:number;
    debitStatus?: any;
    canceled?: boolean;
    seen?: boolean;
    payment_type?: string;
    totalCardTransfers?: number;
    createdAt?: string;
    updatedAt?: string;
    control?: 'user' | 'company';
    control_approval_status?: 'pending' | 'approved' | 'rejected';
    company_id?:string;
    invoice_paid?:boolean;
    date_invoiced_paid?:string;
    total_amount_failed?:number;
}




export class TransferModel implements TransferInterfaceModel {

    _id?;
    card_processed?: boolean;
    card_transaction_id: string;
    userId: string;
    totalReceived: number;
    transactionId: string;
    tfailed: number;
    tsuccessful: number;
    total_refund: number;
    account_type: string;
    charge: number;
    totalBankTransferAmount:number;
    totalBankTransfers:number;
    totalMomoTransferAmount:number;
    totalMomoTransfers:number;
    account_number:number;
    account_issuer:number;
    account_name:string;
    aggregator: string;
    creiditAggregator: string;
    account_voucher:string;
    cardNumber: string;
    amount: string;
    totalTransferAmount: number;
    type: string;
    status: string;
    total_transfer_count:number;
    debitStatus: any;
    canceled: boolean;
    seen: boolean;
    payment_type: string;
    totalCardTransfers: number;
    createdAt: string;
    updatedAt: string;
    control;
    control_approval_status;
    company_id;
    invoice_paid:boolean;
    date_invoiced_paid:string;
    total_amount_failed:number;

    constructor (transfer: TransferInterfaceModel, private store?: Store) {

        this.amount = transfer.amount;
        this.canceled = transfer.canceled;
        this.createdAt = transfer.createdAt;
        this.debitStatus = transfer.debitStatus;
        this.status = transfer.status;
        this.transactionId = transfer.transactionId;
        this.type = transfer.type;
        this.updatedAt = transfer.updatedAt;
        this.userId = transfer.userId;
        this._id = transfer._id;
        this.totalTransferAmount = transfer.totalTransferAmount;
        this.tfailed = transfer.tfailed;
        this.tsuccessful = transfer.tsuccessful;
        this.totalReceived = transfer.totalReceived;
        this.total_refund = transfer.total_refund;
        this.account_type = transfer.account_type;
        this.charge = transfer.charge;
        this.seen = transfer.seen;
        this.cardNumber = transfer.cardNumber;
        this.payment_type = transfer.payment_type;
        this.aggregator = transfer.aggregator;
        this.charge = transfer.charge;
        this.creiditAggregator = transfer.creditAggregator;
        this.account_issuer=transfer.account_issuer;
        this.account_name=transfer.account_name;
        this.account_number=transfer.account_number;
        this.account_voucher=transfer.account_voucher;
        this.totalBankTransfers=transfer.totalBankTransfers;
        this.totalMomoTransferAmount=transfer.totalMomoTransferAmount;
        this.totalMomoTransfers=transfer.totalMomoTransfers;
        this.totalBankTransferAmount=transfer.totalBankTransferAmount;
        this.total_transfer_count=transfer.total_transfer_count;
        this.control=transfer.control;
        this.control_approval_status=transfer.control_approval_status;
        this.control=transfer.control;
        this.company_id=transfer.company_id;
        this.invoice_paid=transfer.invoice_paid;
        this.date_invoiced_paid=transfer.date_invoiced_paid;
        this.total_amount_failed=transfer.total_amount_failed || 0;

    }

    getUser?=(key)=>{
        try {
            return this.userId[key];
        } catch (err) {
            return '';
        }
    }

    getAccountName() {
        try {
            return this.userId['fullname'];
        } catch (err) {
            return null;
        }
    }



    getDebitCode () {
        try {
            if (this.payment_type === 'momo') {
                return this.debitStatus['ResponseCode'];
            }

            if (this.payment_type === 'card') {
                return this.debitStatus['code'];
            }

        } catch (err) {
            return '';            
        }
    }


    getDebitStatus? () {
        try {

            if (this.payment_type === 'momo') {
                if (this.debitStatus['Data']) return this.debitStatus['Data']['Description']
                if (this.debitStatus['Message']) return this.debitStatus['Message'];
                if (this.debitStatus['responseMessage']) return this.debitStatus['responseMessage'];
            }

            if (this.payment_type === 'card') {
                return this.debitStatus['status'];
            }

        } catch (err) {
            return "";
        }
    }



    getMoment? () {
        return moment(this.createdAt).fromNow()
    }

    
    getDateString? () {
        var options = { weekday: 'short', day: 'numeric', year: 'numeric', month: 'short' };
       return new Date(this.createdAt).toLocaleDateString("en-US", options);
       
    }

    getIssuer=(issuers=[])=>{
        try {
            let issuer=issuers.find(issuer=>issuer.id===this.account_issuer);
            return issuer.name;
        } catch (err) {
            return '';
        }
    }


    getPhone () {
        try {
            return this.userId['phone'];
        } catch (err) {
            return null;
        }
    }


    onClick () {
        if (!this.seen) {
            try {
                this.seen = true;
                this.store.dispatch(
                    new SetSeenForTransnfer(
                        {
                            id: this._id,
                            data: {
                                seen: true
                            }
                        }
                    )
                )
            } catch (err) {
                
            }
        }
    }



}