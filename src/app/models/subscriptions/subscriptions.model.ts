import * as moment from "moment";



export interface PaymentAccountInterfaceModel {
    wallet: string;
    network: string;
    voucherCode: string;
}




export interface SubscriptionUpdateModel {
    id: string;
    data: SubscriptionsInterfaceModel;
}



export interface SubscriptionsInterfaceModel {
    _id?: string;
    packageId?: string;
    userId?: string;
    amount?: string;
    credit?: string;
    payment?: PaymentAccountInterfaceModel;
    status?: 'failed' | 'pending' | 'active';
    debitStatus?: any;
    createdAt?: string;
    updatedAt?: string;
    account_issuer?:string;
    account_number?:string;
}




export class SubscriptionModel implements SubscriptionsInterfaceModel {

    _id?;
    amount?;
    createdAt?;
    debitStatus?;
    packageId?;
    payment?;
    status?;
    updatedAt?;
    userId?;
    credit?;
    new?: boolean;
    account_issuer?;
    account_number?;

    constructor (subscription: SubscriptionsInterfaceModel) {

        this._id = subscription._id;
        this.amount = subscription.amount;
        this.createdAt = subscription.createdAt;
        this.debitStatus = subscription.debitStatus;
        this.packageId = subscription.packageId;
        this.payment = subscription.payment;
        this.status = subscription.status;
        this.updatedAt = subscription.updatedAt;
        this.userId = subscription.userId;
        this.credit = subscription.credit;
        this.account_issuer=subscription.account_issuer;
        this.account_number=subscription.account_number;

    }


    getPackageName? () {
        try {
            return this.packageId.name;
        } catch (err) {
            return "";
        }
    }

    getStatus? () {
        try {

            if (this.debitStatus['Message']) {
                return this.debitStatus['Message'];
            }

            return this.debitStatus['Data']['Description']
            
        } catch (err) {
            return "";
        }
    }


    resetNew? () {
        return this.new = false;
    }


    getMoment? () {
        return moment(this.createdAt).fromNow()
    }
    getDateString? () {
        var options = { weekday: 'short', day: 'numeric', year: 'numeric', month: 'short' };
       return new Date(this.createdAt).toLocaleDateString("en-US", options);
       
    }

}