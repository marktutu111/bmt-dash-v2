import * as moment from "moment";
import { API } from "src/app/constants/api";



export interface TransactionsInterfaceModel {
    _id?:string;
    bank_transaction_id?: string;
    userId?: string;
    historyId?: string;
    transferId?: string;
    amount?: string;
    transactionType?: string;
    itc_refund_id?:string;
    account_type?: string;
    status?: string;
    account_number?:string;
    account_issuer?:string;
    account_name?:string;
    account_voucher_code?:string;
    description?:string;
    creditStatus?: any;
    senderRefund?: string;
    senderRefundStatus?:string;
    transaction_type?:string;
    aggregator?: 'ITC' | 'GHIPPS' | 'HUBTEL';
    creditAggregator?: 'ITC' | 'GHIPPS' | 'HUBTEL';
    charge:'1%' | '1.5%' | '2%' | '2.5%' | '3%';
    createdAt?: string;
    updatedAt?: string;
}




export class TransactionsModel implements TransactionsInterfaceModel {

    _id:string;
    bank_transaction_id: string;
    userId: string;
    historyId: string;
    transferId: string;
    amount: string;
    transactionType: string;
    itc_refund_id:string;
    account_type: string;
    status: string;
    account_number:string;
    account_issuer:string;
    account_name:string;
    account_voucher_code:string;
    description:string;
    creditStatus: any;
    senderRefund: string;
    senderRefundStatus:string;
    transaction_type:string;
    aggregator: 'ITC' | 'HUBTEL' | 'GHIPPS';
    creditAggregator: 'ITC' | 'HUBTEL' | 'GHIPPS';
    charge:'1%' | '1.5%' | '2%' | '2.5%' | '3%';
    createdAt: string;
    updatedAt: string;

    loading:boolean=false;
    message:string='';
    private timer:number=1;

    constructor (data: TransactionsInterfaceModel,token='') {

        this._id = data._id;
        this.amount = data.amount;
        this.createdAt = data.createdAt;
        this.creditStatus = data.creditStatus;
        this.status = data.status;
        this.transactionType = data.transactionType;
        this.transferId = data.transferId;
        this.updatedAt = data.updatedAt;
        this.userId = data.userId;
        this.account_type = data.account_type;
        this.account_issuer=data.account_issuer;
        this.account_number=data.account_number;
        this.account_name=data.account_name;
        this.account_voucher_code=data.account_voucher_code;
        this.bank_transaction_id=data.bank_transaction_id;
        this.senderRefund=data.senderRefund;
        this.senderRefundStatus=data.senderRefundStatus;
        this.itc_refund_id=data.itc_refund_id;
        this.historyId=data.historyId;
        this.aggregator=data.aggregator;
        this.creditAggregator=data.creditAggregator;
        this.charge = data.charge;
        this.transaction_type=data.transaction_type;
        this.description=data.description;
        
        this.getStatus(token);
        this.getCreditStatus();


    }

    getIssuer?=(issuers=[])=>{
        try {
            let issuer=issuers.find(issuer=>issuer.id===this.account_issuer);
            return issuer.name;
        } catch (err) {
            return '';
        }
    }


    getCreditStatus? () {
        try {
            if (typeof this.creditStatus === 'string'){
                this.message=this.creditStatus;
                return this.message;
            }
            
            switch (this.aggregator) {
                case 'HUBTEL':
                    this.message= this.creditStatus.Message ? this.creditStatus.Message : this.creditStatus.Data.Message;
                    return this.message;
                case 'ITC':
                    this.message=this.creditStatus['responseMessage'];
                    return this.message;
                case 'GHIPPS':
                    this.message=this.creditStatus.ActCode;
                    return this.message;
                default:
                    break;
            }
        } catch (err) {
            return this.message;
            
        }
    }

    getMoment? () {
        return moment(this.createdAt).fromNow();
    }


    
    getAccount? () {
        return this.account_number;
    }

    getStatus?=(token:string)=>{
        try {
            if (this.status === 'pending'){
                this.loading=true;
                let interval= setInterval(() => {
                    fetch(`${API.TRANSACTIONS}/get/${this._id}`,{
                        headers:{
                            'Content-type':'Application/json',
                            'Authorization':`Bearer ${token}`
                        }
                    }).then(res=>res.json()).then((response:any)=>{
                        const {
                            status
                        }=response.data;
                        this.status=status;
                        this.getCreditStatus();
                        if (this.timer === 10 || status === 'paid'){
                            this.loading=false;
                            clearInterval(interval);
                        }
                        this.timer+=1;
                    }).catch(err=>clearInterval(interval));
                }, 1000*3);
            }
        } catch (err) {
            
        }
    }



}