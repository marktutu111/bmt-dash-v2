
export interface IUser {

    _id?: string;
    fullname?: string;
    email?: string;
    password?: string;
    merchant_id?: string;
    idNumber?: string;
    idType?: string;
    subscription?: string;
    subscriptionModel?: string;
    subscriptionPoints?: number;
    subscriptionActive?: boolean;
    credit?: number;
    blocked?: boolean;
    isEmailVerified?: boolean;
    createdAt?: string;
    updatedAt?: string;
    corporate_accounts?: any[];
    company_id?: string;
    company_role?: 'payer' | 'authorizer';
    country?: string;
    last_seen?: string;
    account_type?: 'bsystems-admin' | 'user' | 'company';
    phone?:string;
    account_is_active?:boolean;
    is_merchant?:boolean;
    company_logo?:string;
    company_name?:string;
}


export class UserModel implements IUser {

    _id?;
    fullname?;
    email?;
    password?;
    merchant_id?;
    idNumber?;
    idType?;
    subscription?;
    subscriptionModel?;
    subscriptionPoints?;
    subscriptionActive?;
    credit?;
    blocked?;
    isEmailVerified?;
    createdAt?;
    updatedAt?;
    corporate_accounts?;
    company_id?;
    last_seen?;
    is_bsystems_account?;
    account_type?;
    company_role?;
    phone?;
    account_is_active;
    is_merchant;
    company_logo;
    company_name;



    constructor (user: IUser) {
        this._id = user._id;
        this.fullname = user.fullname;
        this.email = user.email;
        this.password = user.password;
        this.merchant_id = user.merchant_id;
        this.idNumber = user.idNumber;
        this.idType = user.idType;
        this.subscription = user.subscription;
        this.subscriptionModel = user.subscriptionModel;
        this.subscriptionPoints = user.subscriptionPoints;
        this.subscriptionActive = user.subscriptionActive;
        this.credit = user.credit;
        this.blocked = user.blocked;
        this.isEmailVerified = user.isEmailVerified;
        this.createdAt = user.createdAt;
        this.updatedAt = user.updatedAt;
        this.corporate_accounts = user.corporate_accounts;
        this.company_id = user.company_id;
        this.last_seen = user.last_seen;
        this.account_type = user.account_type;
        this.company_role=user.company_role;
        this.phone=user.phone;
        this.account_is_active=user.account_is_active;
        this.is_merchant=user.is_merchant;
        this.company_logo=user.company_logo;
        this.company_name=user.company_name;
  }
}
