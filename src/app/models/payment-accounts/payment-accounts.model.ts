

export interface PaymentAccountInterfaceModel {
    _id?: string;
    userId?: string;
    wallet?: string;
    network?: string;
    type?: string;
    createdAt?: string;
    updatedAt?: string;
    card_number?: string;
    card_expiry?: string;
    bank_account?: string;
    bank_name?: string;
    amount?:string;
    account_number?:string;
    account_issuer?:string;
}