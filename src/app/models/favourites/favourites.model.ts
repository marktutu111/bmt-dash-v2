

export class AccountsModel {

    account_name:string;
    account_issuer:string;
    account_number:string;
    account_type:string;
    amount:string;
    description:string;


    constructor (account){
        this.description=account.description;
        this.account_issuer=account.account_issuer;
        this.account_name=account.account_name;
        this.account_number=account.account_number;
        this.amount=account.amount;
        this.description=account.description;
    }

    getIssuer?=(issuers=[])=>{
        try {
            let issuer=issuers.find(issuer=>issuer.id===this.account_issuer);
            return issuer.name;
        } catch (err) {
            return '';
        }
    }


}