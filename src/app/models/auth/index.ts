export interface LoginPayload {
    email: string;
    password: string;
    remember?: boolean;
}
export interface SingupInterfaceModel {
    email: string;
    password: string;
    phone:string;
    fullname: string;
    idNumber?: string;
    idType?: string;
    agreement?: string;
}

export interface IAdmin {
    _id?: string;
    fullname?: string;
    email?: string;
    password?: string;
    role?: 'default' | 'super';
    status?: 'blocked' | 'active';
    blocked?: boolean;
    updatedAt?: string;
    createdAt?: string;
    corporate_account_type?: string;
}



export class AdminModel implements IAdmin {

    _id;
    fullname;
    createdAt;
    password;
    role;
    updatedAt;
    email;
    status;
    blocked?;

    constructor (admin: IAdmin) {
        this._id = admin._id;
        this.fullname = admin.fullname;
        this.createdAt = admin.createdAt;
        this.password = admin.password;
        this.role = admin.role;
        this.updatedAt = admin.updatedAt;
        this.email = admin.email;
        this.status = admin.status;
        this.blocked = admin.blocked;

    }

}