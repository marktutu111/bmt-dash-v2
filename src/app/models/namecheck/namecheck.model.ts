

export class NameCheckModel {

    account_number:string;
    account_issuer:string;
    account_name:string;
    name_received:string='';
    name_match:string='';
    checked:boolean=false;
    loading:boolean=false;
    ratio:string='0%';

    constructor (account){
        this.account_number=account.account_number;
        this.account_issuer=account.account_issuer;
        this.account_name=account.account_name;
    };

    getData=(key:string | string[])=>{
        let data:any={};
        if (Array.isArray(key)){
            key.forEach(k=>{
                data[k]=this[k];
            })
        } else data=this[key];
        return data;
    }

    setData=(key,value)=>this[key]=value;

    getIssuer?=(issuers=[])=>{
        try {
            let issuer=issuers.find(issuer=>issuer.id===this.account_issuer);
            return issuer.name;
        } catch (err) {
            return '';
        }
    }


}