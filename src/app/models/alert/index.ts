

export interface IAlert {
    open: boolean;
    message: string;
    type: 'success' | 'error' | 'default'
}