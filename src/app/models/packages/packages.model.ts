
export interface PackageInterfaceModel {
    _id?: string;
    name?: string;
    amount?: string;
    description?: string;
    limit?:  number;
    createdAt?: string;
    updatedAt?: string;
}




export interface PackageUpdateInterface {
    id: string;
    data: PackageInterfaceModel;
}




export class PackageModel implements PackageInterfaceModel {

    _id?;
    name?;
    amount?;
    description?;
    limit?;
    createdAt?;
    updatedAt?;


    constructor (pack: PackageInterfaceModel) {

        this._id = pack._id;
        this.name = pack.name;
        this.amount = pack.amount;
        this.description = pack.description;
        this.limit = pack.limit;
        this.createdAt = pack.createdAt;
        this.updatedAt = pack.updatedAt;

    }


}