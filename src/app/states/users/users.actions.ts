import { IUser } from "../../models/users";

export class GetUsers {
    static readonly type = '[Users] GetUsers';
}


export class GetUserById {
    static readonly type = '[Users] GetUserById';
    constructor (public id: string) {};
}


export class SelectUser {
    static readonly type = '[Users] Select User';
    constructor (public user: IUser) {}
}



export class AddUser {
    static readonly type = '[Users] AddUser';
    constructor (public user: IUser) {}
}


export class ToggleUserEdit {
    static readonly type = '[Users] ToggleUserEdit';
    constructor (public toggle: boolean = true) {};
}


export class ToggleRoleEdit {
    static readonly type = '[Users] ToggleRoleEdit';
    constructor (public toggle: boolean = true) {};
}


export class DeleteUser {
    static readonly type = '[Users] DeleteUser';
    constructor (public id: string) {};
}


export class UpdateUser {
    static readonly type = '[Users] UpdateUser';
    constructor (public readonly id:string,public readonly data:IUser) {};
}




export class PushSearchResults {
    static readonly type = '[Users] PushSearchResults';
    constructor (public readonly results: IUser[]) {};
}



export class NewSignup {
    static readonly type = '[Users] NewSignup';
    constructor(public readonly user: IUser) {};
}



export class SetNewUser {
    static readonly type = '[Users] SetNewUser';
    constructor(public readonly user: IUser) {}
}



export class GetRoles {
    static readonly type = '[Users] GetRoles';
    constructor (public readonly userId: string, public readonly corporateId: string) {};
}



export class AddRole {
    static readonly type = '[Users] AddRole';
    constructor(public readonly data: { id: string, data: any }) {}
}


export class DeleteRole {
    static readonly type = '[Users] DeleteRole';
    constructor(public readonly id: string) {};
}



export class UpdateRole {
    static readonly type = '[Users] UpdateRole';
    constructor(public readonly data: { id: string, data: any }) {};
}


export class GetUserSummary {
    static readonly type = '[Users] GetUserSummary';
}



export class GetBsystemsAccounts {
    static readonly type = '[Users] GetBsystemsAccounts';
}


export class FilterUsers {
    static readonly type = '[Users] FilterUsers]';
    constructor(public readonly payload?: any) {}
}

export class FilterUsersByDate {
    static readonly type = '[Users] FilterUsersByDate';
    constructor(public readonly data: { id: string, data: any[] }) {};
}