import { State, Selector, StateContext, Action, Store } from '@ngxs/store';
import { IUser, UserModel } from '../../models/users';
import { UserService } from '../../services/auth/users';

declare const swal: any;

import { 
    GetUsers, 
    GetUserById, 
    SelectUser, 
    AddUser, 
    ToggleUserEdit, 
    DeleteUser, 
    UpdateUser, 
    PushSearchResults, 
    NewSignup,
    SetNewUser,
    GetRoles,
    AddRole,
    DeleteRole,
    GetUserSummary,
    GetBsystemsAccounts,
    FilterUsers
} from './users.actions';

import { NewAlert } from '../alerts/alerts.actions';
// import { ShowSignupProgress } from '../signup/signup.actions';
import { AuthState } from '../auth/auth.state';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';



interface alert {
    open: boolean;
    message: string;
    type: 'error' | 'success' | 'default';
}





export interface UsersStateModel {
    loading: boolean;
    selected: IUser;
    newUser: IUser;
    users: UserModel[];
    onEdit: boolean;
    roles: any[];
    summary: any;
    admin_accounts: IUser[];
}

@State<UsersStateModel>({
    name: 'Users',
    defaults: {
        loading: false,
        selected: null,
        newUser: {},
        users: [],
        onEdit: false,
        roles: [],
        summary: null,
        admin_accounts: []
    }
})
export class UsersState {


    @Selector()
    static loading (state: UsersStateModel) {
        return state.loading;
    }


    @Selector()
    static admin_accounts (state: UsersStateModel) {
        return state.admin_accounts;
    }

    @Selector()
    static summary (state: UsersStateModel) {
        return state.summary;
    }


    @Selector()
    static selected (state: UsersStateModel) {
        return state.selected;
    }


    @Selector()
    static roles (state: UsersStateModel) {
        return state.roles;
    }

    @Selector()
    static users (state: UsersStateModel) {
        return state.users;
    }


    @Selector()
    static onEdit (state: UsersStateModel) {
        return state.onEdit;
    }


    @Selector()
    static newUser (state: UsersStateModel) {
        return state.newUser;
    }


    constructor (private service: UserService, private store: Store) {};

    

    @Action(GetUsers)
    async getUsers ({ patchState,dispatch }: StateContext<UsersStateModel>, {}: GetUsers) {
        try {
            dispatch(new Toggle(true));
            patchState({ loading: true });
            const response: any = await this.service.getUsers();
            dispatch(new Toggle(false));
            if (response.success) {
                return patchState({
                    loading: false,
                    users:response.data.map(user => new UserModel(user))
                })
            }
            throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
            patchState({
                loading: false
            })
            
        }
    }



    @Action(PushSearchResults)
    pushSearchResults ({ patchState }: StateContext<UsersStateModel>, { results }: PushSearchResults) {
        return patchState({
            users: results.map(user => new UserModel(user))
        })
    }



    @Action(GetRoles)
    async getRoles ({ patchState, getState }: StateContext<UsersStateModel>, { userId, corporateId }: GetRoles) {
        try {

            patchState(
                {
                    loading: true
                }
            )
            
            const response = await this.service.get_roles(userId,corporateId);

            if (response.success) {

                const roles = response.data.map(user => new UserModel(user));

                return patchState(
                    {
                        loading: false,
                        roles: roles
                    }
                )

            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(GetUserById)
    async getUserById ({ patchState }: StateContext<UsersStateModel>, { id }: GetUserById) {
        try {

            patchState({ 
                loading: true 
            });

            const response: any = await this.service.getUserById(id);

            if (response.success) {

                return patchState({
                    loading: false,
                    selected: new UserModel(response.data)
                });

            }

            throw Error(response.message);
            
        } catch (err) {
            
            patchState({
                loading: false
            })
        }
    }



    @Action(AddRole)
    async addRole ({ patchState, dispatch }: StateContext<UsersStateModel>, { data }: AddRole) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.addRole(data);
            if (response.success) {
                const {
                    _id,
                    company_id
                } = this.store.selectSnapshot(AuthState.user);
                patchState(
                    {
                        loading: false
                    }
                )
                swal.fire(
                    {
                        html: '<h4>Role added successfully</h4>'
                    }
                );
                return dispatch(
                    new GetRoles(_id,company_id)
                );
            }
            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }


    @Action(DeleteRole)
    async deleteRole ({ patchState, dispatch }: StateContext<UsersStateModel>, { id }: DeleteRole) {
        try {
            patchState(
                {
                    loading: true
                }
            )
            const response = await this.service.deleteUser(id);
            if (response.success) {
                const {
                    _id,
                    company_id
                } = this.store.selectSnapshot(AuthState.user);
                patchState(
                    {
                        loading: false
                    }
                )
                dispatch(
                    new GetRoles(_id,company_id)
                )
                return swal.fire(
                    {
                        html: '<h4>Role deleted successfully</h4>'
                    }
                )
            } throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }


    @Action(SelectUser)
    selectUser ({ patchState }: StateContext<UsersStateModel>, { user }: SelectUser) {
        return patchState({
            selected: user
        })
    }


    @Action(FilterUsers)
    async filterUsers ({patchState,dispatch}:StateContext<UsersStateModel>,{payload}:FilterUsers){
        try {
            const response=await this.service.filterUser(payload);
            if (response.success){
                const users = response.data.map(user=>new UserModel(user));
                return patchState(
                    {
                        loading:false,
                        users:users
                    }
                )
            } throw Error(response.message);
        } catch (err) {
            patchState({loading:false});
        }
    }

    @Action(AddUser)
    async addUser ({ patchState, getState, dispatch }: StateContext<UsersStateModel>, { user }: AddUser) {
        try {
            
            dispatch(new NewAlert({
                message: 'Creating account..',
                toggle: true,
                type: 'loader',
                success:false
            }))

            const response = await this.service.addUser(user);
            if (response.success) {
                const _user = new UserModel(response.data);
                patchState({
                    users: [
                        _user,
                        ...getState().users
                    ]
                });
                return dispatch(new NewAlert({
                    message: response.message,
                    toggle: true,
                    type: 'alert',
                    success:true
                }))

            } throw Error(response.message);
        } catch (err) {
            dispatch(new NewAlert({
                message: err.message,
                type: 'alert',
                toggle: true
            }))

        }
    }


    @Action(DeleteUser)
    async deleteUser ({ patchState, dispatch, getState }: StateContext<UsersStateModel>, { id }: DeleteUser) {
        try {

            dispatch(new NewAlert({
                message: 'Deleting User',
                toggle: true,
                type: 'loader'
            }))

            const response = await this.service.deleteUser(id);

            if (response.success) {
                let { users } = getState();
                let _users = users.filter(user => user._id !== id);
                patchState({
                    users: _users,
                    selected: null
                });

                return dispatch(new NewAlert({
                    message: response.message,
                    toggle: true,
                    type: 'alert'
                }))
            }

            throw Error(response.message);
        } catch (err) {
            dispatch(new NewAlert({
                message: err.message,
                type: 'alert',
                toggle: true
            }))

        }
    }


    @Action(UpdateUser)
    async updateUser ({ patchState, getState, dispatch }: StateContext<UsersStateModel>, { id,data }: UpdateUser) {
        try {
            dispatch(new Toggle(true));
            const response = await this.service.updateUser({ id:id,data:data });
            if (response.success) {
                const _user = new UserModel(response.data);
                let { users } = getState();
                let _users = users.map(user=>user['_id'] === id ? _user:user);
                patchState({
                    selected:_user,
                    users:_users
                });
                return dispatch(
                    [
                        new Toggle(false),
                        new NewAlert(
                            {
                                message:'User account has been updated successfully.',
                                toggle:true,
                                type:'alert'
                            }
                        )
                    ]
                )
            }
            throw Error(response.message);
        } catch (err) {
            dispatch(
                [
                    new Toggle(false),
                    new NewAlert(
                        {
                            message:err.message,
                            type:'alert',
                            toggle:true
                        }
                    )
                ]
            )
        }
    }



    // @Action(NewSignup)
    // async newSignup ({ patchState, dispatch }: StateContext<UsersStateModel>, { user }: NewSignup) {
    //     try {

    //         dispatch(
    //             new ShowSignupProgress(true)
    //         )

    //         let response = await this.service.addUser(user);

    //         if (response.success) {

    //             dispatch(
    //                 new ShowSignupProgress(false)
    //             )

    //             return patchState(
    //                 {
    //                     newUser: new UserModel(response.data)
    //                 }
    //             )

    //         }

    //         throw Error(response.message);

            
    //     } catch (err) {

    //         patchState(
    //             {
    //                 loading: false
    //             }
    //         )

    //         dispatch(
    //             [
    //                 new ShowSignupProgress(false),
    //                 new NewAlert(
    //                     {
    //                         type: 'alert',
    //                         message: err.message,
    //                         toggle: true
    //                     }
    //                 )
    //             ]
    //         )
            
    //     }
    // }



    @Action(SetNewUser)
    setNewUser ({ patchState }: StateContext<UsersStateModel>, { user }: SetNewUser) {
        patchState(
            {
                newUser: user
            }
        )
    }

    
    @Action(ToggleUserEdit)
    toggleUserEdit ({ patchState, getState }: StateContext<UsersStateModel>, { toggle }: ToggleUserEdit) {
        patchState({
            onEdit: toggle
        })
    }



    @Action(GetUserSummary)
    async getUserSummary ({ patchState,dispatch }: StateContext<UsersStateModel>) {
        try {

            dispatch(new Toggle(true));
            const response = await this.service.get_summary();
            if (response.success) {
                return patchState(
                    {
                        summary: response.data[0]
                    }
                )
            }
            throw Error(response.message);
        } catch (err) {
            dispatch(new Toggle(false));
        }
    }


    @Action(GetBsystemsAccounts)
    async getBsystemsAccounts ({ patchState }: StateContext<UsersStateModel>) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.get_bsystems_admin();

            if (response.success) {

                const accounts = response.data.map(user => new UserModel(user));
                return patchState(
                    {
                        loading: false,
                        admin_accounts: accounts
                    }
                )

            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


}


