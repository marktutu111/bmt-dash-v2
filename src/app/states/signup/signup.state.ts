import { State, Selector, Action, StateContext } from '@ngxs/store';
import {UserService } from '../../services/auth/users';
import { NewSignup } from './signup.actions';
import { NotificationsService } from "angular2-notifications";
import { Router } from '@angular/router';
import { Navigate } from "@ngxs/router-plugin";


export interface SignupStateModel {
    loading: boolean;
    alert: {
        toggle: boolean;
        message: string;
    }
}

@State<SignupStateModel>({
    name: 'Signup',
    defaults: {
        loading: false,
        alert: {
            message: '',
            toggle: false
        }
    }
})
export class SignupState {

    constructor (
            private service: UserService,
            private notifications: NotificationsService,
            private router: Router
        ) {};

    @Selector()
    static loading (state: SignupStateModel) {
        return state.loading;
    }


    @Selector()
    static alert (state: SignupStateModel) {
        return state.alert;
    }

    @Action(NewSignup)
    async newSignup ({ patchState,dispatch }: StateContext<SignupStateModel>, { payload }: NewSignup) {
        try {

            patchState(
                {
                    loading: true,
                    alert: {
                        message: '',
                        toggle: false
                    }
                }
            )

            const response = await this.service.newSignup(payload);
            if (!response.success){
                throw Error(response.message);
            }

            patchState(
                {
                    loading: false,
                    alert: {
                        message: response.message,
                        toggle: true
                    }
                }
            );

            this.notifications.success(
                'Login',
                response.message,
                {
                    timeOut: 1000 * 3
                }
            )

            return dispatch(new Navigate(['../login']));
            
        } catch (err) {
            patchState(
                {
                    loading: false,
                    alert: {
                        message: err.message,
                        toggle: true
                    }
                }
            )
        }
    }

}

