import { State, Selector, Action, StateContext } from '@ngxs/store';
import { InterfaceDashboard, DashboardInterfaceModel } from 'src/app/models/dashboard';
import { GetDashboard } from './dashboard.actions';
import { DashboardService } from 'src/app/services/dashboard/dashboard.service';




export interface DashboardStateModel {
    transfers: DashboardInterfaceModel;
    subscriptions: DashboardInterfaceModel;
    loading: boolean;
}


@State<DashboardStateModel>({
    name: 'Dashboard',
    defaults: {
        transfers: {
            failed: 0,
            paid: 0,
            pending: 0,
            total: 0
        },
        subscriptions: {
            failed: 0,
            paid: 0,
            pending: 0,
            total: 0
        },
        loading: false
    }
})
export class DashboardState {

    
    @Selector()
    static transfers (state: DashboardStateModel) {
        return state.transfers;
    }


    @Selector()
    static subscriptions (state: DashboardStateModel) {
        return state.subscriptions;
    }


    @Selector()
    static loading (state: DashboardStateModel) {
        return state.loading;
    }


    constructor (private service: DashboardService) {};


    @Action(GetDashboard)
    async getDashboard ({ patchState }: StateContext<DashboardStateModel>, { user }: GetDashboard) {
        try {

            patchState(
                {
                    loading: false
                }
            );

            const response = await this.service.getDashboard(user);

            if (response.success) {

                const {
                    subscriptions,
                    transfers
                } = response.data;


                let _subscriptions = {};
                let _tranfers = {};

                subscriptions.forEach(subscription => _subscriptions[subscription['_id']] = subscription['count']);
                transfers.forEach(transfer => _tranfers[transfer['_id']] = transfer['count']);

                patchState(
                    {
                        loading: false,
                        subscriptions: _subscriptions,
                        transfers: _tranfers
                    }
                );

            } else {

                throw Error(response.message);

            }
            
        } catch (error) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


}
