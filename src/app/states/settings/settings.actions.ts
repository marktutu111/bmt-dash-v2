
export class GetSettings {
    static readonly type = '[Settings] GetSettings]';
}


export class UpdateSettings {
    static readonly type = '[Settings] UpdateSettings]';
    constructor(public readonly payload?: any) {};
}
