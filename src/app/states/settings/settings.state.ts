import { State, Action, StateContext, Selector } from '@ngxs/store';
import { SettingsService } from 'src/app/services/settings/settings.service';
import { GetSettings, UpdateSettings } from './settings.actions';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';
declare const swal: any;


interface StateModel {
    loading: boolean;
    settings: any;
    charge: any;
}


@State<StateModel>({
    name: 'Settings',
    defaults: {
        loading: false,
        settings: {},
        charge:{}
    }
})
export class SettingsState {

    @Selector()
    static loading (state: StateModel) {
        return state.loading;
    }

    
    @Selector()
    static settings (state: StateModel) {
        return state.settings;
    }

    @Selector()
    static charge (state: StateModel){
        return state.charge;
    }
    

    constructor (private service: SettingsService) {};


    @Action(GetSettings)
    async getSettings ({ patchState,dispatch }: StateContext<StateModel>) {
        try {
            dispatch(new Toggle(true));
            patchState({ loading: true });
            const response = await this.service.getSettings();
            dispatch(new Toggle(false))
            if (response.success) {
                return patchState(
                    {
                        loading: false,
                        settings: response.data
                    }
                )
            }
            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
        }
    }


    @Action(UpdateSettings)
    async updateSettings ({ patchState, dispatch }: StateContext<StateModel>, { payload }: UpdateSettings) {
        try {

            dispatch(new Toggle(true));
            patchState({ loading: true });
            const response = await this.service.updateSettings(payload);
            dispatch(new Toggle(false));
            if (response.success) {
                patchState(
                    {
                        loading: false
                    }
                )

                swal.fire({ html: '<h4>Update successful</h4>' });
                return dispatch(new GetSettings());

            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
        }
    }


}
