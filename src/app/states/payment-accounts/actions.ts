
export class NewPaymentAccount {
    static readonly type = '[PaymentAccount] NewPaymentAccount';
    constructor(public readonly data: any, public silent: boolean = false) {};
}

export class DeleteAccount {
    static readonly type = '[PaymentAccount] DeleteAccount';
    constructor(public readonly id: string) {};
}


export class GetAccounts {
    static readonly type = '[PaymentAccount] GetAccounts';
    constructor(public readonly id: string) {};
}
