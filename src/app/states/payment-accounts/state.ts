import { State, Selector, Action, StateContext } from '@ngxs/store';
import { TransferInterfaceModel } from 'src/app/models/transfers/transfers.model';
import { PaymentAccountService } from 'src/app/services/payment-account/payment-account.service';
import { GetAccounts, DeleteAccount, NewPaymentAccount } from './actions';
import { PaymentAccountInterfaceModel } from 'src/app/models/payment-accounts/payment-accounts.model';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';

declare const swal: any;


interface StateModel {
    loading: boolean;
    accounts: PaymentAccountInterfaceModel[]
}

@State<StateModel>({
    name: 'PaymentAccount',
    defaults: {
        accounts: [],
        loading: false
    }
})
export class PaymentAccountState {

    @Selector()
    static loading (state: StateModel) {
        return state.loading;
    }

    @Selector()
    static accounts (state: StateModel) {
        return state.accounts;
    }

    constructor (private service: PaymentAccountService) {};



    @Action(GetAccounts)
    async getAccounts ({ patchState,dispatch }: StateContext<StateModel>, { id }: GetAccounts) {
        try {

            dispatch(new Toggle(true));
            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.getAccounts(id);
            dispatch(new Toggle(false));
            if (response.success) {
                const accounts = response.data;
                return patchState(
                    {
                        loading: false,
                        accounts: accounts
                    }
                )

            }
            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(DeleteAccount)
    async deleteAccount ({ patchState, getState }: StateContext<StateModel>, { id }: DeleteAccount) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.deleteAccount(id);
            if (response.success) {

                const {
                    accounts
                } = getState();
                const _accounts = accounts.filter(account => account['_id'] !== id);
                patchState(
                    {
                        loading: false,
                        accounts: _accounts
                    }
                )

            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )

            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            );
            
        }
    }




    @Action(NewPaymentAccount)
    async newPaymentAccount ({ patchState, dispatch }: StateContext<StateModel>, { data, silent }: NewPaymentAccount) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.newAccount(data);

            if (response.success) {

                const account: PaymentAccountInterfaceModel = response.data;

                patchState(
                    {
                        loading: false
                    }
                )

                if (!silent) {
                    swal.fire(
                        {
                            html: '<h4>Account saved successfully</h4>'
                        }
                    )
                }

                return dispatch(
                    new GetAccounts(account['userId'])
                )

            }

            throw Error(response.message);
            
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )

            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            );
            
        }
    }


}
