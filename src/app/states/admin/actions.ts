import { IAdmin } from "src/app/models/auth";


export class GetAdmins {
    static readonly type = '[Admins] GetAdmins';
}


export class GetAdminById {
    static readonly type = '[Admins] GetAdminById';
    constructor (public id: string) {};
}


export class SelectAdmin {
    static readonly type = '[Admins] Select Admin';
    constructor (public user: IAdmin) {}
}



export class AddAdmin {
    static readonly type = '[Admins] AddAdmin';
    constructor (public user: IAdmin) {}
}


export class DeleteAdmin {
    static readonly type = '[Admins] DeleteAdmin';
    constructor (public id: string) {};
}

export class UpdateAdmin {
    static readonly type = '[Admins] UpdateAdmin';
    constructor (public id: string, public data: IAdmin) {};
}


export class ToggleAdminEdit {
    static readonly type = '[Admins] ToggleAdminEdit';
    constructor (public event: boolean = true) {};
}


export class PushAdminSearchResults {
    static readonly type = '[Admins] PushAdminSearchResults';
    constructor (public readonly results: IAdmin[]) {};
}
