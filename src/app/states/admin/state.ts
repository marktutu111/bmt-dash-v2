import { State, Selector, StateContext, Action } from '@ngxs/store';
import { AdminService } from '../../services/auth/admins';
import { GetAdmins, GetAdminById, SelectAdmin, AddAdmin, DeleteAdmin, UpdateAdmin, ToggleAdminEdit, PushAdminSearchResults } from './actions';
import { NewAlert } from '../alerts/alerts.actions';
import { IAdmin, AdminModel } from 'src/app/models/auth';



export interface AdminStateModel {
    loading: boolean;
    selected: IAdmin;
    users: AdminModel[];
    onEdit: boolean;
}

@State<AdminStateModel>({
    name: 'Admins',
    defaults: {
        loading: false,
        selected: undefined,
        users: [],
        onEdit: false
    }
})
export class AdminState {


    @Selector()
    static loading (state: AdminStateModel) {
        return state.loading;
    }

    @Selector()
    static selected (state: AdminStateModel) {
        return state.selected;
    }

    @Selector()
    static users (state: AdminStateModel) {
        return state.users;
    }

    @Selector()
    static onEdit (state: AdminStateModel) {
        return state.onEdit;
    }


    constructor (private service: AdminService) {}

    @Action(GetAdmins)
    async getUsers ({ patchState }: StateContext<AdminStateModel>) {
        try {

            patchState({ loading: true });
            const response: any = await this.service.getAdmins();
            if (response.success) {
                return patchState({
                    loading: false,
                    users: response.data.map(user => new AdminModel(user))
                })
            }

            throw Error(response.message);
            
        } catch (err) {

            patchState({
                loading: false
            })
            
        }
    }



    @Action(GetAdminById)
    async getUserById ({ patchState }: StateContext<AdminStateModel>, { id }: GetAdminById) {
        try {

            patchState({ loading: true });

            const response: any = await this.service.getAdminById(id);

            if (response.success) {
                return patchState({
                    loading: false,
                    selected: new AdminModel(response.data)
                })
            }

            throw Error(response.message);
            
        } catch (err) {
            
            patchState({
                loading: false
            })
        }
    }


    @Action(SelectAdmin)
    selectUser ({ patchState }: StateContext<AdminStateModel>, { user }: SelectAdmin) {
        return patchState({
            selected: user
        })
    }


    @Action(AddAdmin)
    async addUser ({ patchState, getState, dispatch }: StateContext<AdminStateModel>, { user }: AddAdmin) {
        try {
            
            dispatch(new NewAlert({
                message: 'Creating account..',
                toggle: true,
                type: 'loader'
            }))

            const response = await this.service.addAdmin(user);

            if (response.success) {

                const _user = new AdminModel(response.data);

                patchState({
                    loading: false,
                    users: [
                        _user,
                        ...getState().users,
                    ]
                })

                return dispatch(new NewAlert({
                    message: response.message,
                    toggle: true,
                    type: 'alert'
                }));

            }

            throw Error(response.message);


        } catch (err) {

            dispatch(new NewAlert({
                message: err.message,
                toggle: true,
                type: 'alert'
            }))

        }
    }


    @Action(DeleteAdmin)
    async deleteAdmin ({ patchState, dispatch, getState }: StateContext<AdminStateModel>, { id }: DeleteAdmin) {
        try {

            dispatch(new NewAlert({
                message: 'Deleting account..',
                toggle: true,
                type: 'loader'
            }));

            const res = await this.service.deleteAdmin(id);

            if (res.success) {

                const { users } = getState();
                const _users = users.filter(user => user['_id'] !== id);

                patchState({
                    users: _users,
                    selected: null
                })

                return dispatch(new NewAlert({
                    message: res.message,
                    toggle: true,
                    type: 'alert'
                }))

            }

            throw Error(res.message);

        } catch (err) {

            dispatch(new NewAlert({
                message: err.message,
                toggle: true,
                type: 'alert'
            }));

        }
    }


    @Action(UpdateAdmin)
    async updateAdmin ({ patchState, dispatch, getState }: StateContext<AdminStateModel>,{ data,id }: UpdateAdmin) {
        try {

            dispatch(new NewAlert({
                message: 'Updating account..',
                toggle: true,
                type: 'loader'
            }));

            const res = await this.service.updateAdmin({ id: id, data: data });

            if (res.success) {

                const { users } = getState();
                const _user = new AdminModel(res.data);
                const _users = users.map(user => user['_id'] === id ? _user : user);

                patchState({
                    users: _users,
                    selected: _user
                })

                return dispatch(new NewAlert({
                    message: res.message,
                    toggle: true,
                    type: 'alert'
                }))

            }
            
        } catch (err) {
            
            dispatch(new NewAlert({
                message: err.message,
                toggle: true,
                type: 'alert'
            }));
            
        }
    }


    @Action(ToggleAdminEdit)
    toggleAdminEdit ({ patchState }: StateContext<AdminStateModel>, { event }: ToggleAdminEdit) {
        return patchState({
            onEdit: event
        })
    }



    @Action(PushAdminSearchResults)
    pushAdminSearchResults ({ patchState }: StateContext<AdminStateModel>, { results }: PushAdminSearchResults) {
        return patchState({
            users: results.map(user => new AdminModel(user))
        })
    }


}
