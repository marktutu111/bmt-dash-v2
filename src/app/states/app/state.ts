import { State, Selector, Action, StateContext } from '@ngxs/store';
import { ToggleSideContent, ActivatePath, AccountType } from './actions';


export type InterfaceActivePath = 'transfers' | 'subscriptions' | 'dashboard' | '';


export interface AppStateModel {
    toggled: boolean;
    path: InterfaceActivePath;
    account_type: 'prepaid' | 'pay_as_you_go' | 'corporate' | 'bsystems' | 'company';
}

@State<AppStateModel>({
    name: 'App',
    defaults: {
        toggled: false,
        account_type: 'company',
        path: ''
    }
})
export class AppState {

    @Selector()
    static toggled (state: AppStateModel) {
        return state.toggled;
    }


    @Selector()
    static path (state: AppStateModel) {
        return state.path;
    }


    @Selector()
    static account_type (state: AppStateModel) {
        return state.account_type;
    }


    @Action(ToggleSideContent)
    toggleSideContent ({ patchState, getState }: StateContext<AppStateModel>) {
        return patchState({
            toggled: !getState().toggled
        })
    }
    

    @Action(ActivatePath)
    activePath ({ patchState }: StateContext<AppStateModel>, { path }: ActivatePath) {
        return patchState({
            path: path
        })
    }
    

    @Action(AccountType)
    accountType ({ patchState }: StateContext<AppStateModel>, { account }: AccountType) {
        return patchState(
            {
                account_type: account
            }
        )
    }


    

}
