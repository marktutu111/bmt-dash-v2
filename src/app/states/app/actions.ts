import { InterfaceActivePath } from "./state";


export class ToggleSideContent {
    static readonly type = '[App] ToggleSideContent';
}

export class ActivatePath {
    static readonly type = '[App] ActivatePath';
    constructor (public path: InterfaceActivePath) {};
}


export class ChangePath {
    static readonly type = '[App] ChangePath';
    constructor(public readonly path: string) {};
}


export class AccountType {
    static readonly type = '[App] AccountType';
    constructor(public readonly account: 'prepaid' | 'pay_as_you_go' | 'corporate' | 'bsystems' | 'company') {};
}