import { PackageInterfaceModel, PackageUpdateInterface, PackageModel } from "src/app/models/packages/packages.model";




export class GetPackages {
    static readonly type = '[Packages] GetPackages]';
}


export class GetPackagesById {
    static readonly type = '[Packages] GetPackagesById';
    constructor (public readonly id: string) {};
}


export class NewPackages {
    static readonly type = '[Packages] NewPackages';
    constructor (public readonly data: PackageInterfaceModel) {};
}



export class UpdatePackages {
    static readonly type = '[Packages] UpdatePackages';
    constructor (public readonly data: PackageUpdateInterface ) {};
}



export class DeletePackages {
    static readonly type = '[Packages] DeletePackages';
    constructor (public readonly id: string) {};
}



export class SelectPackages {
    static readonly type = '[Packages] SelectPackages';
    constructor (public readonly pack: PackageInterfaceModel) {};
}



export class TogglePackagesEdit {
    static readonly type = '[Packages] TogglePackagesEdit';
    constructor (public readonly event: boolean) {};
}



export class GetPackageDetails {
    static readonly type = '[Packages] GetPackageDetails]';
    constructor(public readonly pack: PackageModel) {}
}
