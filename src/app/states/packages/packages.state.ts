import { State, Selector, Action, StateContext } from '@ngxs/store';
import { NewAlert } from '../alerts/alerts.actions';
import { PackageModel } from 'src/app/models/packages/packages.model';
import { GetPackages, GetPackagesById, DeletePackages, NewPackages, UpdatePackages, SelectPackages, TogglePackagesEdit, GetPackageDetails } from './packages.actions';
import { PackageService } from 'src/app/services/packages/packages.service';





interface PackageStateModel {
    loading: boolean;
    packages: PackageModel[];
    selected: PackageModel;
    onEdit: boolean;
}

@State<PackageStateModel>(
    {
        name: 'Packages',
        defaults: {
            loading: false,
            selected: {},
            onEdit: false,
            packages: []
        }
    }
)
export class PackageState {

    @Selector()
    static loading (state: PackageStateModel) {
        return state.loading;
    }


    @Selector()
    static packages (state: PackageStateModel) {
        return state.packages;
    }


    @Selector()
    static selected (state: PackageStateModel) {
        return state.selected;
    }


    @Selector()
    static onEdit (state: PackageStateModel) {
        return state.onEdit;
    }



    constructor (private service: PackageService) {};



    @Action(GetPackages)
    async get ({ patchState }: StateContext<PackageStateModel>) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            let response = await this.service.get();

            if (response.success) {

                return patchState(
                    {
                        loading: false,
                        packages: response.data.map(pack => new PackageModel(pack))
                    }
                )

            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


    @Action(GetPackagesById)
    async getById ({ patchState }: StateContext<PackageStateModel>, { id }: GetPackagesById) {
        try {

            patchState(
                {
                    loading: true
                }
            );

            let response = await this.service.getById(id);

            if (response.success) {

                let packs = response.data.map(pack => new PackageModel(pack));
                return patchState(
                    {
                        loading: false,
                        packages: packs
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }

    @Action(DeletePackages)
    async delete ({ patchState, getState }: StateContext<PackageStateModel>, { id }: DeletePackages) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            let response = await this.service.delete(id);

            if (response.success) {

                const {
                    packages,
                    selected
                } = getState();

                let packs = packages.filter(pack => pack['_id'] !== id);

                return patchState(
                    {
                        loading: false,
                        packages: packs,
                        selected: selected['_id'] === id ? {} : selected
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(NewPackages)
    async new ({ patchState, getState }: StateContext<PackageStateModel>, { data }: NewPackages) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            let response = await this.service.newPackage(data);

            if (response.success) {

                return patchState(
                    {
                        loading: false,
                        packages: [
                            new PackageModel(response.data),
                            ...getState().packages
                        ]
                    }
                )

            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


    @Action(UpdatePackages)
    async update ({ patchState, getState, dispatch }: StateContext<PackageStateModel>, { data }: UpdatePackages) {
        try {

            dispatch(new NewAlert(
                {
                    message: 'Updating class...',
                    toggle: true,
                    type: 'loader'
                }
            ));

            let response = await this.service.update(data);

            if (response.success) {

                let {
                    packages,
                    selected,
                } = getState();

                let update = new PackageModel(response.data);
                let packs = packages.map(pack => pack['_id'] === update['_id'] ? update : pack);

                patchState(
                    {
                        selected: selected['_id'] === update['_id'] ? update : selected,
                        packages: packs
                    }
                );

                return dispatch(
                    new NewAlert(
                        {
                            message: 'Updated successfully',
                            toggle: true,
                            type: 'alert'
                        }
                    )
                )


            }


            throw Error(response.message);
        

        } catch (err) {

            return dispatch(
                new NewAlert(
                    {
                        message: err.message,
                        toggle: true,
                        type: 'alert'
                    }
                )
            )
            
        }
    }
    


    @Action(SelectPackages)
    select ({ patchState }: StateContext<PackageStateModel>, { pack }: SelectPackages) {
        patchState(
            {
                selected: pack
            }
        )    
    }




    @Action(TogglePackagesEdit)
    toggleEdit ({ patchState }: StateContext<PackageStateModel>, { event }: TogglePackagesEdit) {
        return patchState(
            {
                onEdit: event
            }
        )
    }


    

}
