

export class GetFavourites {
    static readonly type = '[Favourites] GetFavourites';
    constructor(public readonly id: string) {};
}




export class SaveFavourite {
    static readonly type = '[Favourites] SaveFavourite';
    constructor(public readonly data: any, public silent: boolean = false) {};
}



export class UpdateFavourite {
    static readonly type = '[Favourites] UpdateFavourite';
    constructor(public readonly data: { id: string, data: any }) {};
}


export class DeleteFavourite {
    static readonly type = '[Favourites] DeleteFavourite';
    constructor(public readonly id: string) {};
}


export class onFavouriteUpdate {
    static readonly type = '[Favourites] onFavouriteUpdate';
    constructor(public readonly event: boolean = true) {};
}



export class SelectFavourite {
    static readonly type = '[Favourites] SelectFavourite';
    constructor(public readonly data: any) {}
}
