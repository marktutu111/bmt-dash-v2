import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { FavouriteService } from 'src/app/services/favourites/favourites.service';
import { GetFavourites, UpdateFavourite, DeleteFavourite, SaveFavourite, onFavouriteUpdate, SelectFavourite } from './favourites.state';
import { AuthState } from '../auth/auth.state';
import { AccountsModel } from 'src/app/models/favourites/favourites.model';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';

declare const swal: any;


interface StateModel {
    loading: boolean;
    favourites: any[];
    onUpdate: boolean;
    updates: { id: string, accounts: any[] };
    selected: any;
}


@State<StateModel>({
    name: 'Favourites',
    defaults: {
        loading: false,
        favourites: [],
        onUpdate: false,
        updates: null,
        selected: null
    }
})
export class FavState {


    constructor (private service: FavouriteService, private store: Store) {};


    @Selector()
    static loading (state: StateModel) {
        return state.loading;
    }

    @Selector()
    static favourites (state: StateModel) {
        return state.favourites;
    }

    @Selector()
    static onUpdate (state: StateModel) {
        return state.onUpdate;
    }

    @Selector()
    static updates (state: StateModel) {
        return state.updates;
    }

    @Selector()
    static selected (state: StateModel) {
        return state.selected;
    }

    @Action(GetFavourites)
    async getFavourites ({ patchState,dispatch }: StateContext<StateModel>, { id }: GetFavourites) {
        try {

            dispatch(new Toggle(true));
            patchState(
                {
                    loading: true,
                    favourites:[],
                    selected:null
                }
            )
            
            const respose = await this.service.getUserHistory(id);
            dispatch(new Toggle(false));
            if (!respose.success){
                throw Error(respose.message);
            }
            let fav:any[] = respose.data;
            let _fav=fav.map(f=>{
                return {...f,accounts:f.accounts.map(
                    acc=>new AccountsModel(acc)
                )}
            });
            return patchState(
                {
                    loading: false,
                    favourites: _fav
                }
            )
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
        }
    }



    @Action(UpdateFavourite)
    async updateFavourite ({ patchState, dispatch }: StateContext<StateModel>, { data }: UpdateFavourite) {
        try {
            
            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.updateHistory(data);
            if (response.success) {
                const userId: string = this.store.selectSnapshot(AuthState.user)._id;;
                patchState(
                    {
                        loading: false,
                        selected: response['data']
                    }
                )

                swal.fire(
                    {
                        html: '<h4>Update successfully</h4>'
                    }
                )

                return dispatch(new GetFavourites(userId));

            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )

            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
            
        }
    }



    @Action(DeleteFavourite)
    async deleteFavourite ({ patchState, dispatch, getState }: StateContext<StateModel>, { id }: DeleteFavourite) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.deleteHistory(id);
            if (response.success) {
                const userId: string = this.store.selectSnapshot(AuthState.user)._id;
                patchState(
                    {
                        loading: false,
                        selected: null
                    }
                )
                
                swal.fire(
                    {
                        html: `<h4>Delete successfully</h4>`
                    }
                )

                return dispatch(
                    new GetFavourites(userId)
                )

            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }


    @Action(SaveFavourite)
    async saveFourite ({ patchState, dispatch }: StateContext<StateModel>, { data, silent }: SaveFavourite) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.save(data);
            if (response.success) {
                patchState(
                    {
                        loading: false
                    }
                )

                if (!silent) {
                    swal.fire(
                        {
                            html: '<h4>Saved successfully</h4>'
                        }
                    )
                }

                return dispatch(
                    new GetFavourites(data['userId'])
                )

            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }



    @Action(onFavouriteUpdate)
    onFavouriteUpdate ({ patchState }: StateContext<StateModel>, { event }: onFavouriteUpdate) {
        patchState(
            {
                onUpdate: event
            }
        )
    }


    @Action(SelectFavourite)
    selectFavourite ({ patchState }: StateContext<StateModel>, { data }: SelectFavourite) {
        patchState(
            {
                selected: data
            }
        )
    }



}
