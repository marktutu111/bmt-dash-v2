import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxsModule } from "@ngxs/store";
import { NgxsRouterPluginModule } from "@ngxs/router-plugin";


import { AuthState } from './auth/auth.state';
import { AuthService } from '../services/auth';
import { HttpClientModule } from "@angular/common/http";
import { UsersState } from './users/users.state';
import { UserService } from '../services/auth/users';
import { AppState } from './app/state';
import { AdminService } from '../services/auth/admins';
import { AdminState } from './admin/state';
import { AlertState } from './alerts/alerts.state';
import { SearchState } from '../components/search-component/state/search.state';
import { SearchService } from '../services/search/search.service';
import { DashboardState } from './dahsboard/dashboard.state';
import { DashboardService } from '../services/dashboard/dashboard.service';
import { SignupState } from './signup/signup.state';
import { SubscriptionState } from './subscriptions/subscriptions.state';
import { SubscriptionService } from '../services/subscriptions/subscriptions.service';
import { PackageState } from './packages/packages.state';
import { PackageService } from '../services/packages/packages.service';
import { TransferService } from '../services/transfers/transfers.service';
import { TransfersState } from './transfer/transfer.state';
import { ListGridState } from '../components/transaction-list-grid/transaction-list-grid.state';
import { PaymentAccountService } from '../services/payment-account/payment-account.service';
import { PaymentAccountState } from './payment-accounts/state';
import { FavState } from './favourites/favourites.actions';
import { FavouriteService } from '../services/favourites/favourites.service';
import { SidebarState } from '../components/sidebar/state/store';
import { SettingsService } from '../services/settings/settings.service';
import { SettingsState } from './settings/settings.state';
import { ProgressBarState } from '../components/progress-bar/progress-bar.state';



@NgModule({
    declarations: [],
    imports: [ 
        CommonModule,
        HttpClientModule,
        NgxsModule.forRoot(
            [
                AuthState,
                UsersState,
                AppState,
                AdminState,
                AlertState,
                SearchState,
                DashboardState,
                SignupState,
                SubscriptionState,
                PackageState,
                TransfersState,
                ListGridState,
                PaymentAccountState,
                FavState,
                SidebarState,
                SettingsState,
                ProgressBarState
            ]
        ),
        NgxsRouterPluginModule.forRoot()
    ],
    exports: [],
    providers: [
        AuthService,
        UserService,
        AdminService,
        SearchService,
        DashboardService,
        SubscriptionService,
        PackageService,
        TransferService,
        PaymentAccountService,
        FavouriteService,
        SettingsService
    ],
})
export class StatesModule {}