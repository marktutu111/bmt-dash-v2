import { LoginPayload } from "../../models/auth";
import { IUser } from "src/app/models/users";


export class Login {
    static readonly type = '[Authentication] Login';
    constructor (public data: LoginPayload) {}
}

export class Logout {
    static readonly type = '[Authentication] Logout]';
}


export class UpdateUser {
    static readonly type = '[Authentication] UpdateUser';
    constructor(public readonly user: IUser) {}
}


export class GetUserFromStore {
    static readonly type = '[Authentication] GetUserFromStore';
}


export class SetLastSeen {
    static readonly type = '[Authentication] SetLastSeen';
    constructor() {};

}



export class ResetPassword {
    static readonly type = '[Authentication] ResetPassword';
    constructor(public readonly payload: any) {};
}



export class UpdatePassword {
    static readonly type = '[Authentication] UpdatePassword';
    constructor(public readonly payload: any) {};
}




export class ForgotPassword {
    static readonly type = '[Authentication] ForgotPassword';
    constructor(public readonly payload: any) {};
}


export class SetUser {
    static readonly type = '[Authentication] SetUser]';
    constructor(public readonly payload: { token: string, user: IUser }) {}
}
