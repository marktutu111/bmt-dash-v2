import { State, Selector, StateContext, Action } from "@ngxs/store";
import { Navigate } from "@ngxs/router-plugin";

import { Login, Logout, UpdateUser, GetUserFromStore, SetLastSeen, ForgotPassword, ResetPassword, UpdatePassword, SetUser } from "./auth.actions";
import { AuthService } from "../../services/auth";
import { UserModel, IUser } from "src/app/models/users";
import { AccountType } from "../app/actions";


declare const swal: any;



export interface AuthAlertInterface {
    message: string;
    toggle: boolean;
}


interface AuthStateModel {
    loading: boolean;
    alert: AuthAlertInterface;
    user: IUser;
    token: string;
}


@State<AuthStateModel>({
    name: 'Authentication',
    defaults: {
        loading: false,
        user: null,
        token: null,
        alert: {
            message: '',
            toggle: false
        }
    }
})
export class AuthState {


    @Selector()
    static loading (state: AuthStateModel) {
        return state.loading;
    }

    @Selector()
    static alert (state: AuthStateModel) {
        return state.alert
    }

    @Selector()
    static user (state: AuthStateModel) {
        return state.user;
    }


    @Selector()
    static token (state: AuthStateModel) {
        return state.token;
    }


    constructor (private service: AuthService) {};




    @Action(Login)
    async login ({ patchState, dispatch }: StateContext<AuthStateModel>, { data }: Login) {
        try {

            const loginData = data;
            patchState(
                {
                    loading: true,
                    alert: {
                        message: '',
                        toggle: false
                    }
                }
            )

            const response = await this.service.login(
                {
                    email: data['email'],
                    password: data['password']
                }
            );

            if (response.success) {
                const {
                    token,
                    data
                } = response;

                const user = new UserModel(data);
                if (loginData['remember']) {
                    localStorage.setItem('bmt_user_auth_data', JSON.stringify(loginData));
                }

                patchState(
                    {
                        loading: false,
                        user: user,
                        token: token
                    }
                );

                if (user && user['_id']) {
                    const {account_type,company_role}=user;
                    switch (account_type) {
                        case 'bsystems-admin':
                            return dispatch(
                                new Navigate(['bsystems'])
                            )
                        case 'user':
                            return dispatch(
                                [
                                    new Navigate(['0']),
                                    new AccountType('pay_as_you_go')
                                ]
                            )
                        case 'company':
                            if (company_role==='authorizer'){
                                return dispatch(
                                    [
                                        new Navigate(['0/payments']),
                                        new AccountType('company')
                                    ]
                                )
                            }
                            return dispatch(
                                [
                                    new Navigate(['0/send-money']),
                                    new AccountType('company')
                                ]
                            )
                        default:
                            break;
                    }
                }
            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false,
                    alert: {
                        message: err.message,
                        toggle: true
                    }
                }
            )
        }

    }



    @Action(Logout)
    logout ({ patchState, dispatch }: StateContext<AuthStateModel>) {
        localStorage.removeItem('bmt_user_auth_data');
        patchState({ user: null,token: null});
        return dispatch(new Navigate(['login']));
    }



    @Action(UpdateUser)
    updateUser ({ patchState }: StateContext<AuthStateModel>, { user }: UpdateUser) {
        return patchState(
            {
                user: user
            }
        )
    }




    @Action(GetUserFromStore)
    getUserFromStore ({ patchState, dispatch }: StateContext<AuthStateModel>) {
        const data = localStorage.getItem('bmt_user_auth_data');
        if (typeof data === 'string') {
            const user = JSON.parse(data);
            dispatch(
                new Login(
                    {
                        email: user['email'],
                        password: user['password']
                    }
                )
            )
        }
    }



    @Action(SetLastSeen)
    async setLastSeen ({ getState }: StateContext<AuthStateModel>) {
        try {

            const userId: string = getState().user._id;
            this.service.set_last_seen(userId);

        } catch (err) {


        }
    }

    @Action(ForgotPassword)
    async forgotPassword ({ patchState, dispatch }: StateContext<AuthStateModel>, { payload }: ForgotPassword) {
        try {

            patchState(
                {
                    loading: false,
                    alert: {
                        message: '',
                        toggle: false
                    }
                }
            )

            const response = await this.service.forgotPassword(payload);


            if (response.success) {

                patchState(
                    {
                        loading: false,
                        alert: {
                            toggle: true,
                            message: 'Please enter the reset password that was sent to your email and new password to reset account'
                        }
                    }
                )


                return dispatch(
                    new Navigate(['resetpassword'])
                )

            }


            throw Error(response['message']);


        } catch (err) {

            patchState(
                {
                    loading: false,
                    alert: {
                        toggle: true,
                        message: err.message
                    }
                }
            )

        }
    }



    @Action(ResetPassword)
    async resetPassword ({ patchState, dispatch }: StateContext<AuthStateModel>, { payload }: ResetPassword) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.resetPassword(payload);

            if (response.success) {
                patchState(
                    {
                        loading: false
                    }
                )

                swal.fire(
                    {
                        html: `<h4>Account reset successfull, Please login to continue</h4>`
                    }
                )

                return dispatch(
                    new Navigate(['login'])
                )


            }

            throw Error(response.message);


        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )

            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            ).catch(err => null);

        }
    }


    @Action(UpdatePassword)
    async updatePassword ({ patchState }: StateContext<AuthStateModel>, { payload }: UpdatePassword) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.updatePassword(payload);

            if (response.success) {
                return patchState(
                    {
                        loading: false
                    }
                )
            }

            throw Error(response.message);


        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )

        }
    }



    @Action(SetUser)
    setUser ({ patchState, dispatch }: StateContext<AuthStateModel>, { payload }: SetUser) {
        try {

            const {
                user,
                token
            } = payload;

            patchState(
                {
                    user: user,
                    token: token
                }
            )

        } catch (err) {

        }
    }




}
