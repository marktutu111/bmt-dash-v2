import { State, Selector, Action, StateContext } from '@ngxs/store';
import { CancelAlert, NewAlert, AlertAction, IAlertAction } from './alerts.actions';
import { LAlert } from '../../components/lazy-alert/model';



export interface AlertStateModel {
    alert: LAlert;
    action: IAlertAction
}

@State<AlertStateModel>({
    name: 'alerts',
    defaults: {
        alert: {
            data: null,
            message: '',
            toggle: false,
            success:false,
            type: 'alert'
        },
        action: {
            data: null,
            name: '',
            type:'default'
        }
    }
})
export class AlertState {

    @Selector()
    static alert (state: AlertStateModel): LAlert {
        return state.alert;
    }

    @Selector()
    static action (state: AlertStateModel): IAlertAction {
        return state.action;
    }
    

    @Action(CancelAlert)
    cancelAlert ({ patchState }: StateContext<AlertStateModel>) {
        return patchState({
            alert: this.resetAlert()
        })
    }

    
    @Action(NewAlert)
    openAlert ({ patchState }: StateContext<AlertStateModel>, { alert }: NewAlert) {
        return patchState({
            alert: alert
        })
    }



    @Action(AlertAction)
    alertAction ({ patchState }: StateContext<AlertStateModel>, { action }: AlertAction) {
        return patchState({
            action: action
        })
    }


    resetAlert (): LAlert {
        return {
            data: null,
            message: '',
            toggle: false,
            success:false,
            type: 'default'
        }
    }

}

