import { IAlert } from "../../models/alert";
import { LAlert, LAlertData } from "../../components/lazy-alert/model";


export interface IAlertAction {
    type: 'cancel' | 'ok' | 'confirm' | 'default';
    name?: string;
    data: LAlertData
}


export class CancelAlert {
    static readonly type = '[alerts] CancelAlert';
}


export class NewAlert {
    static readonly type = '[alerts] OpenAlert';
    constructor (public alert: LAlert) {};
}


export class AlertAction {
    static readonly type = '[alerts] AlertAction';
    constructor (public action: IAlertAction) {};
}
