import { State, Selector, Action, StateContext } from '@ngxs/store';
import { NewAlert } from '../alerts/alerts.actions';
import { SubscriptionModel } from 'src/app/models/subscriptions/subscriptions.model';
import { SubscriptionService } from 'src/app/services/subscriptions/subscriptions.service';
import { 
    GetSubscriptions,
    GetSubscriptionsById, 
    DeleteSubscriptions, 
    NewSubscriptions, 
    UpdateSubscriptions, 
    SelectSubscriptions, 
    ToggleSubscriptionsEdit, 
    PushSubscriptionSearchResults,
    GetSubscriptionByUser,
    UpdateSubscriptionStatus,
    NewTopup,
    FilterDate
} from './subscriptions.actions';


import { GetTransferCredit } from '../transfer/transfer.actions';
declare const swal: any;



interface SubscriptionStateModel {
    loading: boolean;
    subscriptions: SubscriptionModel[];
    selected: SubscriptionModel;
    onEdit: boolean;
}

@State<SubscriptionStateModel>(
    {
        name: 'Subscriptions',
        defaults: {
            loading: false,
            selected: {},
            onEdit: false,
            subscriptions: []
        }
    }
)
export class SubscriptionState {

    @Selector()
    static loading (state: SubscriptionStateModel) {
        return state.loading;
    }


    @Selector()
    static subscriptions (state: SubscriptionStateModel) {
        return state.subscriptions;
    }


    @Selector()
    static selected (state: SubscriptionStateModel) {
        return state.selected;
    }


    @Selector()
    static onEdit (state: SubscriptionStateModel) {
        return state.onEdit;
    }


    constructor (private service: SubscriptionService) {};



    @Action(UpdateSubscriptionStatus)
    updateTransferStatus ({ patchState, getState }: StateContext<SubscriptionStateModel>, { subscription }: UpdateSubscriptionStatus) {
        const {
            selected,
            subscriptions
        } = getState();

        let _subscriptions = subscriptions.map(_subscription => _subscription['_id'] === subscription._id ? subscription : _subscription);
        return patchState(
            {
                subscriptions: _subscriptions,
                selected: selected['_id'] === subscription['_id'] ? subscription : selected,
            }
        )
    }



    @Action(GetSubscriptions)
    async get ({ patchState }: StateContext<SubscriptionStateModel>) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            let response = await this.service.getSubscriptions();

            if (response.success) {
                return patchState(
                    {
                        loading: false,
                        subscriptions: response.data.map(subscription => new SubscriptionModel(subscription))
                    }
                )
            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(GetSubscriptionsById)
    async getById ({ patchState }: StateContext<SubscriptionStateModel>, { id }: GetSubscriptionsById) {
        try {

            patchState(
                {
                    loading: true
                }
            );

            let response = await this.service.getById(id);

            if (response.success) {

                let subscriptions = response.data.map(subscription => new SubscriptionModel(subscription));
                return patchState(
                    {
                        loading: false,
                        subscriptions: subscriptions
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(GetSubscriptionByUser)
    async getSubscriptionByUser ({ patchState }: StateContext<SubscriptionStateModel>, { userId }: GetSubscriptionByUser) {
        try {

            patchState(
                {
                    loading: true
                }
            );

            let response = await this.service.getByUser(userId);

            if (response.success) {

                let subscriptions = response.data.map(subscription => new SubscriptionModel(subscription));
                return patchState(
                    {
                        loading: false,
                        subscriptions: subscriptions
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


    @Action(DeleteSubscriptions)
    async delete ({ patchState, getState }: StateContext<SubscriptionStateModel>, { id }: DeleteSubscriptions) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            let response = await this.service.delete(id);

            if (response.success) {

                const {
                    subscriptions,
                    selected
                } = getState();

                let _subscriptions = subscriptions.filter(subscription => SubscriptionModel['_id'] !== id);

                return patchState(
                    {
                        loading: false,
                        subscriptions: _subscriptions,
                        selected: selected['_id'] === id ? {} : selected
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(NewSubscriptions)
    async new ({ patchState, getState, dispatch }: StateContext<SubscriptionStateModel>, { data }: NewSubscriptions) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.newSubscription(data);

            if (response.success) {

                let subscription = new SubscriptionModel(response.data);
                subscription.new = true;

                patchState(
                    {
                        loading: false,
                        subscriptions: [
                            subscription,
                            ...getState().subscriptions
                        ]
                    }
                )

                return swal(
                    `<h4>${response.message}</h4>`,
                    '',
                    'success'
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )

            return swal(
                `<h4>${err.message}</h4>`,
                '',
                'success'
            )
            
        }
    }


    @Action(UpdateSubscriptions)
    async update ({ patchState, getState, dispatch }: StateContext<SubscriptionStateModel>, { data }: UpdateSubscriptions) {
        try {

            dispatch(new NewAlert(
                {
                    message: 'Updating class...',
                    toggle: true,
                    type: 'loader'
                }
            ));

            let response = await this.service.update(data);

            if (response.success) {

                let {
                    subscriptions,
                    selected,
                } = getState();

                let update = new SubscriptionModel(response.data);
                let _subscriptions = subscriptions.map(subscription => subscription['_id'] === update['_id'] ? update : subscription);

                patchState(
                    {
                        selected: selected['_id'] === update['_id'] ? update : selected,
                        subscriptions: _subscriptions
                    }
                );

                return dispatch(
                    new NewAlert(
                        {
                            message: 'Updated successfully',
                            toggle: true,
                            type: 'alert'
                        }
                    )
                )


            }


            throw Error(response.message);
        

        } catch (err) {

            return dispatch(
                new NewAlert(
                    {
                        message: err.message,
                        toggle: true,
                        type: 'alert'
                    }
                )
            )
            
        }
    }
    


    @Action(SelectSubscriptions)
    select ({ patchState }: StateContext<SubscriptionStateModel>, { subscription }: SelectSubscriptions) {

        subscription.new = false;
        patchState(
            {
                selected: subscription
            }
        );
        
        
    }




    @Action(ToggleSubscriptionsEdit)
    toggleEdit ({ patchState }: StateContext<SubscriptionStateModel>, { event }: ToggleSubscriptionsEdit) {
        return patchState(
            {
                onEdit: event
            }
        )
    }



    @Action(PushSubscriptionSearchResults)
    pushTransferSearchResults ({ patchState }: StateContext<SubscriptionStateModel>, { subscriptions }: PushSubscriptionSearchResults) {
        return patchState({
            subscriptions: subscriptions.map(subscription => new SubscriptionModel(subscription))
        })
    }



    @Action(NewTopup)
    async newTopup ({ patchState, getState, dispatch }: StateContext<SubscriptionStateModel>, { topup }: NewTopup) {
        try {

            dispatch(
                new NewAlert(
                    {
                        message: 'Sending request..',
                        toggle: true,
                        type: 'loader'
                    }
                )
            )


            const response = await this.service.topup(topup);

            if (response.success) {

                let subscription = new SubscriptionModel(response.data);
                subscription.new = true;

                patchState(
                    {
                        subscriptions: [
                            subscription,
                            ...getState().subscriptions
                        ]
                    }
                )

                dispatch(
                    [
                        new NewAlert(
                            {
                                toggle: false
                            }
                        ),
                        new GetTransferCredit(topup['userId'])
                    ]
                );


                return swal.fire(
                    {
                        type: 'info',
                        title: `<h4>${response.message}</h4>`,
                        showConfirmButton: false,
                        timer: 1500
                    }
                );
                

            }


            throw Error(response.message);
        

        } catch (err) {

            dispatch(
                new NewAlert(
                    {
                        toggle: false,
                    }
                )
            )

            return swal.fire(
                {
                    type: 'error',
                    title: `<h4>${err.message}</h4>`,
                    showConfirmButton: false,
                    timer: 1500
                }
            );
            
        }
    }



    @Action(FilterDate)
    async filterDate ({ patchState }: StateContext<SubscriptionStateModel>, { date }: FilterDate) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.filter_by_date(date);

            if (response.success) {

                return patchState(
                    {
                        loading: false,
                        subscriptions: response.data.map(subscription => new SubscriptionModel(subscription))
                    }
                )

            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }





    

}
