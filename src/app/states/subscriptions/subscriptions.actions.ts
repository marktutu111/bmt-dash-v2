import { SubscriptionsInterfaceModel, SubscriptionUpdateModel, SubscriptionModel } from "src/app/models/subscriptions/subscriptions.model";



export class GetSubscriptions {
    static readonly type = '[Subscriptions] GetSubscriptions]';
}


export class GetSubscriptionsById {
    static readonly type = '[Subscriptions] GetSubscriptionsById';
    constructor (public readonly id: string) {}
}


export class NewSubscriptions {
    static readonly type = '[Subscriptions] NewSubscriptions';
    constructor (public readonly data: SubscriptionsInterfaceModel) {};
}



export class UpdateSubscriptions {
    static readonly type = '[Subscriptions] UpdateSubscriptions';
    constructor (public readonly data: SubscriptionUpdateModel ) {};
}



export class DeleteSubscriptions {
    static readonly type = '[Subscriptions] DeleteSubscriptions';
    constructor (public readonly id: string) {};
}



export class SelectSubscriptions {
    static readonly type = '[Subscriptions] SelectSubscriptions';
    constructor (public readonly subscription: SubscriptionModel) {};
}



export class ToggleSubscriptionsEdit {
    static readonly type = '[Subscriptions] ToggleSubscriptionsEdit';
    constructor (public readonly event: boolean) {};
}



export class PushSubscriptionSearchResults {
    static readonly type = '[Subscriptions] PushSubscriptionSearchResults';
    constructor(public readonly subscriptions?: SubscriptionsInterfaceModel[]) {}
}


export class GetSubscriptionByUser {
    static readonly type = '[Subscriptions] GetSubsriptionByUser';
    constructor(public readonly userId: string) {}
}


export class UpdateSubscriptionStatus {
    static readonly type = '[Subscriptions] UpdateSubscriptionStatus';
    constructor(public readonly subscription: SubscriptionsInterfaceModel) {}
}



export class NewTopup {
    static readonly type = '[Subscriptions] NewTopup';
    constructor(public readonly topup?: SubscriptionsInterfaceModel) {}
}



export class FilterDate {
    static readonly type = '[Subscriptions] FilterDate';
    constructor(public readonly date: any[]) {};
}
