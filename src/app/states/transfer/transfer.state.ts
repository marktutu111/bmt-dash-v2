import { State, Selector, Action, StateContext, Store } from '@ngxs/store';
import { NewAlert } from '../alerts/alerts.actions';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { TransferService } from 'src/app/services/transfers/transfers.service';
import { GetTransfers, GetTransfersById, DeleteTransfers, NewTransfers, UpdateTransfers, SelectTransfers, ToggleTransfersEdit, GetTransfersByUser, GetTransferTransactions, UpdateTransferStatus, UpdateTransactionStatus, SelectTransaction, PushTransferSearchResults, GetExcel, GetTransferCredit, GetTotal, SaveTransfer, GetUserHistory, GetTransferHistory, ClearHistory, FilterTransferDate, OnSelectedTransfer, SetSeenForTransnfer, FilterUserTranfersByDate, Get_Admin_Summary, SearchFilter, Get_User_Transfer_Summary, HandleVbv, FilterTransfers, AuthorizePayment, GetIssuers, GetTransferStatus, PayInvoice, QueNameChecks } from './transfer.actions';
import { TransactionsModel } from 'src/app/models/transactions/transactions.model';
import { UserService } from 'src/app/services/auth/users';
import { Get_Unseen } from 'src/app/components/sidebar/state/actions';
import { SetUser } from '../auth/auth.actions';
import { Navigate } from '@ngxs/router-plugin';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';
import { AuthState } from '../auth/auth.state';
import { NameCheckModel } from 'src/app/models/namecheck/namecheck.model';

declare const swal: any;


interface TransferStateModel {
    loading: boolean;
    transfers: TransferModel[];
    transactions: TransactionsModel[];
    selected: TransferModel;
    loadingTransactions: boolean;
    selectedTransaction: TransactionsModel;
    onEdit: boolean;
    csvRecipients: any[];
    transferCredits: number;
    totalTransfers: number;
    history: any[];
    transferHistory: any[];
    selected_transfer_id: string;
    summary: any;
    issuers:any[];
    pending_transfers: TransferModel[],
    namechecks:any[]
}

@State<TransferStateModel>(
    {
        name: 'Transfers',
        defaults: {
            issuers:[],
            selectedTransaction: null,
            loading: false,
            selected: null,
            onEdit: false,
            loadingTransactions: false,
            transfers: [],
            pending_transfers: [],
            transactions: [],
            csvRecipients: [],
            transferCredits: 0,
            totalTransfers: 0,
            history: [],
            transferHistory: [],
            selected_transfer_id: null,
            summary: null,
            namechecks:[]
        }
    }
)
export class TransfersState {

    @Selector()
    static loading (state: TransferStateModel) {
        return state.loading;
    }

    @Selector()
    static namecheck (state: TransferStateModel) {
        return state.namechecks;
    }

    @Selector()
    static summary (state: TransferStateModel) {
        return state.summary;
    }

    @Selector()
    static issuers (state: TransferStateModel) {
        return state.issuers;
    }

    @Selector()
    static transferHistory (state: TransferStateModel) {
        return state.transferHistory;
    }



    @Selector()
    static selected_transfer_id (state: TransferStateModel) {
        return state.selected_transfer_id;
    }


    @Selector()
    static history (state: TransferStateModel) {
        return state.history;
    }

    @Selector()
    static totalTransfers (state: TransferStateModel) {
        return state.totalTransfers;
    }


    @Selector()
    static transferCredits (state: TransferStateModel) {
        return state.transferCredits;
    }



    @Selector()
    static csvRecipients (state: TransferStateModel) {
        return state.csvRecipients;
    }


    @Selector()
    static transfers (state: TransferStateModel) {
        return state.transfers;
    }


    @Selector()
    static pendingTransfers (state: TransferStateModel) {
        return state.pending_transfers;
    }


    @Selector()
    static selected (state: TransferStateModel) {
        return state.selected;
    }


    @Selector()
    static selectedTransaction (state: TransferStateModel) {
        return state.selectedTransaction;
    }


    @Selector()
    static onEdit (state: TransferStateModel) {
        return state.onEdit;
    }


    @Selector()
    static loadingTransactions (state: TransferStateModel) {
        return state.loadingTransactions;
    }


    @Selector()
    static transactions (state: TransferStateModel) {
        return state.transactions;
    }


    constructor (private service: TransferService, private userService: UserService, private store: Store) {};




    @Action(UpdateTransferStatus)
    updateTransferStatus ({ patchState, getState }: StateContext<TransferStateModel>, { transfer }: UpdateTransferStatus) {
        const {
            selected,
            transfers
        } = getState();
        const new_transfer = new TransferModel(transfer,this.store)
        let _transfers = transfers.map(_transfer => _transfer['_id'] === transfer._id ? new_transfer : _transfer);
        return patchState(
            {
                transfers: _transfers,
                selected: selected['_id'] === transfer['_id'] ? new_transfer : selected,
            }
        )
        
    }


    @Action(QueNameChecks)
    queNameChecks ({patchState}:StateContext<TransferStateModel>,{data}:QueNameChecks){
        patchState(
            {
                namechecks:data
            }
        )
    }


    @Action(FilterTransfers)
    async filterTranfers ({patchState,dispatch}:StateContext<TransferStateModel>,{filter}:FilterTransfers){
        try {
            dispatch(new Toggle(true));
            patchState({loading:true,pending_transfers:[],selected:null});
            const response= await this.service.filter(filter);
            dispatch(new Toggle(false));
            if (response.success){
                const transfers =response.data.map(transfer=>new TransferModel(transfer));
                return patchState(
                    {
                        loading:false,
                        pending_transfers:transfers,
                        transfers:transfers
                    }
                )
            } throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading:false
                }
            )
        }
    }


    @Action(UpdateTransactionStatus)
    updateTransactionStatus ({ patchState, getState }: StateContext<TransferStateModel>, { transaction }: UpdateTransactionStatus) {
        const {
            selectedTransaction,
            transactions
        } = getState();

        const _transactions = transactions.map(_transaction => _transaction['_id'] === transaction._id ? transaction : _transaction);
        return patchState(
            {
                transactions: _transactions,
                selectedTransaction: selectedTransaction['_id'] === transaction['_id'] ? selectedTransaction : selectedTransaction,
            }
        )
    }


    @Action(PushTransferSearchResults)
    pushTransferSearchResults ({ patchState }: StateContext<TransferStateModel>, { transfers }: PushTransferSearchResults) {
        return patchState({
            transfers: transfers.map(transfer => new TransferModel(transfer, this.store))
        })
    }


    @Action(GetIssuers)
    async getIssuers ({patchState,dispatch}:StateContext<TransferStateModel>){
        try {
            dispatch(new Toggle(true));
            const response=await this.service.getIssuers();
            dispatch(new Toggle(false));
            if (response.success){
                const issuers=response.data;
                return patchState(
                    {
                        issuers:issuers
                    }
                )
            } throw Error(response.message);
        } catch (err) {
            
        }
    }


    @Action(AuthorizePayment)
    async authorizePayment({patchState,dispatch}:StateContext<TransferStateModel>,{payload}:AuthorizePayment){
        try {
            dispatch(new Toggle(true));
            patchState({loading:true});
            const response =await this.service.authorizePayment(payload);
            dispatch(new Toggle(false));
            if (!response.success){
                throw Error(response.message);
            }
            patchState(
                {
                    loading:false
                }
            )
            return swal.fire(
                {
                    title:'Transaction successful',
                    html: `<h3>${response.message}</h3>`
                }
            )
        } catch (err) {
            patchState({loading:false});
            swal.fire(
                {
                    title:'Transaction Error',
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }
    



    @Action(GetTransfers)
    async get ({ patchState,dispatch }: StateContext<TransferStateModel>) {
        try {

            patchState({loading: true});
            dispatch(new Toggle(true));
            let response = await this.service.get();
            dispatch(new Toggle(false));
            if (response.success) {
                return patchState(
                    {
                        loading: false,
                        transfers: response.data.map(transfer => new TransferModel(transfer, this.store))
                    }
                )
            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


    @Action(PayInvoice)
    async payInvoice ({dispatch}:StateContext<TransferStateModel>,{payload}:PayInvoice){
        try {
            dispatch(new Toggle(true));
            const response =await this.service.payinvoice(payload);
            if (!response.success){
                throw Error(response.message);
            }
            swal.fire(
                {
                    type: 'success',
                    title: `<h4>${response.message}</h4>`,
                    showConfirmButton: false
                }
            );
            this.store.dispatch(new GetTransfers());
            return dispatch(new Toggle(false));
        } catch (err) {
            dispatch(new Toggle(false));
            return swal.fire(
                {
                    type: 'error',
                    title: `<h4>${err.message}</h4>`
                }
            );
        }
    }


    @Action(GetTransfersById)
    async getById ({ patchState }: StateContext<TransferStateModel>, { id }: GetTransfersById) {
        try {

            patchState(
                {
                    loading: true
                }
            );

            let response = await this.service.getById(id);
            if (response.success) {
                let transfers = response.data.map(transfer => new TransferModel(transfer, this.store));
                return patchState(
                    {
                        loading: false,
                        transfers: transfers
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(GetTransfersByUser)
    async getByUser ({ patchState }: StateContext<TransferStateModel>, { id }: GetTransfersByUser) {
        try {

            patchState(
                {
                    loading: true
                }
            );

            let response = await this.service.getByUser(id);
            if (response.success) {
                let transfers = response.data.map(transfer => new TransferModel(transfer,this.store));
                return patchState(
                    {
                        loading: false,
                        transfers: transfers
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(GetTransferTransactions)
    async getTransferTransactions ({ patchState, dispatch }: StateContext<TransferStateModel>, { id }: GetTransferTransactions) {
        try {

            dispatch(new Toggle(true));
            let response = await this.service.getTransactions(id);
            if (!response.success) {
                throw Error(response.message);
            }

            const token=this.store.selectSnapshot(AuthState.token);
            let transactions = response.data.map(transaction => new TransactionsModel(transaction,token));
            dispatch(new Toggle(false));
            return patchState(
                {
                    transactions: transactions
                }
            )

        } catch (err) {
            dispatch(new Toggle(false));
        }
    }




    @Action(DeleteTransfers)
    async delete ({ patchState, getState }: StateContext<TransferStateModel>, { id }: DeleteTransfers) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            let response = await this.service.delete(id);

            if (response.success) {

                const {
                    transfers,
                    selected
                } = getState();

                let _transfers = transfers.filter(transfer => transfer['_id'] !== id);

                return patchState(
                    {
                        loading: false,
                        transfers: _transfers,
                        selected: selected['_id'] === id ? null : selected
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(NewTransfers)
    async new ({ patchState, getState, dispatch }: StateContext<TransferStateModel>, { data }: NewTransfers) {
        try {

            dispatch(
                new NewAlert(
                    {
                        type: 'loader',
                        message: 'Procesing request..',
                        toggle: true
                    }
                )
            )

            patchState(
                {
                    loading: true
                }
            );

            const response = await this.service.new(data);
            if (response.success) {
                const transfer = new TransferModel(response.data,this.store);
                patchState(
                    {
                        loading: false,
                        transfers: [
                            transfer,
                            ...getState().transfers
                        ]
                    }
                )

                dispatch([
                    new GetUserHistory(data['userId']),
                    new NewAlert(
                        {
                            toggle: false,
                        }
                    ),
                    new Toggle(true),
                    new GetTransferStatus(transfer._id)
                ]);

                return swal.fire(
                    {
                        type: 'success',
                        title: `<h4>${response.message}</h4>`,
                        showConfirmButton: true
                    }
                );

            }

            if (response['data'] && response['data']['code'] && response['data']['code'] === 200) {
                patchState(
                    {
                        loading: false
                    }
                )

                return swal.fire(
                    {
                        type: 'alert',
                        html: `<h4>Your card must be verfied by Visa, Please confirm to procede.</h4>`,
                        showConfirmButton: true,
                        confirmButtonText: 'Verify Card',
                        confirmButtonColor: '#ddd',
                        showCancelButton: true,
                    }
                ).then(({ value }) => {

                    if (value) {

                        window.location.href = `${response['data']['link']}`;

                    }

                }).catch(err => null);

            } throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            
            dispatch(
                new NewAlert(
                    {
                        toggle: false
                    }
                )
            )

            return swal.fire(
                {
                    type: 'error',
                    html: `<h4>${err.message}</h4>`,
                    showConfirmButton: false,
                }
            );

        }


    }


    @Action(UpdateTransfers)
    async update ({ patchState, getState, dispatch }: StateContext<TransferStateModel>, { data }: UpdateTransfers) {
        try {

            dispatch(new NewAlert(
                {
                    message: 'Updating transfer...',
                    toggle: true,
                    type: 'loader'
                }
            ));

            let response = await this.service.update(data);

            if (response.success) {

                let {
                    transfers,
                    selected,
                } = getState();

                let update = new TransferModel(response.data,this.store);
                let _transfers = transfers.map(transfer => transfer['_id'] === update['_id'] ? update : transfer);

                patchState(
                    {
                        selected: selected['_id'] === update['_id'] ? update : selected,
                        transfers: _transfers
                    }
                );

                return dispatch(
                    new NewAlert(
                        {
                            message: 'Updated successfully',
                            toggle: true,
                            type: 'alert'
                        }
                    )
                )


            }


            throw Error(response.message);
        

        } catch (err) {

            return dispatch(
                new NewAlert(
                    {
                        message: err.message,
                        toggle: true,
                        type: 'alert'
                    }
                )
            )
            
        }
    }
    


    @Action(SelectTransfers)
    select ({ patchState }: StateContext<TransferStateModel>, { transfer }: SelectTransfers) {
        patchState(
            {
                selected: transfer
            }
        )    
    }



    @Action(SelectTransaction)
    selectTransaction ({ patchState }: StateContext<TransferStateModel>, { transaction }: SelectTransaction) {
        patchState(
            {
                selectedTransaction: transaction
            }
        )    
    }




    @Action(ToggleTransfersEdit)
    toggleEdit ({ patchState }: StateContext<TransferStateModel>, { event }: ToggleTransfersEdit) {
        return patchState(
            {
                onEdit: event
            }
        )
    }



    @Action(GetExcel)
    async getExcel ({ patchState }: StateContext<TransferStateModel>, { data }: GetExcel) {
        try {

            patchState(
                {
                    loading: true
                }
            )


            const response = await this.service.getExcel(data);
            

            if (response.success) {

                return patchState(
                    {
                        loading: false,
                        csvRecipients: response.data
                    }
                )

            }

            throw Error(response.message);

            
        } catch (err) {

            swal(
                'Oops!',
                err.message,
                'error'
            )
            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(GetTransferCredit)
    async getTransferCredit ({ patchState }: StateContext<TransferStateModel>, { userId }: GetTransferCredit) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.userService.getUserById(userId);

            if (response.success) {

                const user  = response.data;

                return patchState(
                    {
                        loading: false,
                        transferCredits: user['credit']
                    }
                )


            }


            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    @Action(GetTotal)
    async getTotal ({ patchState }: StateContext<TransferStateModel>, { userId }: GetTotal) {
        try {

            const response = await this.service.getTotal(userId);


            if (response.success) {
                return patchState(
                    {
                        totalTransfers: response.data
                    }
                )
            }
            
        } catch (err) {
            
            
        }
    }


    @Action(SaveTransfer)
    async saveTransfer ({ dispatch }: StateContext<TransferStateModel>, { transfer }: SaveTransfer) {
        try {

            const response = await this.service.saveTransfer(transfer);

            if (response.success) {
                return dispatch(
                    new GetUserHistory(
                        transfer['userId']
                    )
                )
            }
            
        } catch (err) {
            
        }
    };




    @Action(GetUserHistory)
    async getUserHistory ({ patchState }: StateContext<TransferStateModel>, { userId }: GetUserHistory) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.getUserHistory(userId);
            

            if (response.success) {

                const { accounts } = response.data;

                return patchState(
                    {
                        loading: false,
                        history: accounts
                    }
                )
            }
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }

    }



    @Action(GetTransferHistory)
    async getHistory ({ patchState }: StateContext<TransferStateModel>, { transferId }: GetTransferHistory) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.getTransferHistory(transferId);
            

            if (response.success) {
                return patchState(
                    {
                        loading: false,
                        transferHistory: response.data
                    }
                )
            }

            throw Error(
                response.message
            )
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }


    @Action(ClearHistory)
    clearHistory ({ patchState }: StateContext<TransferStateModel>) {
        return patchState(
            {
                transferHistory: []
            }
        )
    }




    @Action(FilterTransferDate)
    async transferTransferDate ({ patchState }: StateContext<TransferStateModel>, { date }: FilterTransferDate) {
        try {

            patchState(
                {
                    loading: true
                }
            )
            
            const response = await this.service.filter_by_date(date);

            if (response.success) {
                return patchState(
                    {
                        loading: false,
                        transfers: response.data.map(transfer => new TransferModel(transfer,this.store))
                    }
                )
            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }




    @Action(FilterUserTranfersByDate)
    async filterUserTransferDate ({ patchState }: StateContext<TransferStateModel>, { data }: FilterUserTranfersByDate) {
        try {

            patchState(
                {
                    loading: true
                }
            )
            
            const response = await this.service.filter_date_by_user(data);

            if (response.success) {
                return patchState(
                    {
                        loading: false,
                        transfers: response.data.map(transfer => new TransferModel(transfer,this.store))
                    }
                )
            }

            throw Error(response.message);
            
        } catch (err) {

            patchState(
                {
                    loading: false
                }
            )
            
        }
    }



    
    @Action(OnSelectedTransfer)
    onSelectedTransfer ({ patchState }: StateContext<TransferStateModel>, { id }: OnSelectedTransfer) {
        patchState(
            {
                selected_transfer_id: id
            }
        )
    }



    @Action(SetSeenForTransnfer)
    async setSeenForTransfer ({ patchState, dispatch }: StateContext<TransferModel>, { data }: SetSeenForTransnfer) {
        try {

            await this.service.update(data);
            dispatch(
                new Get_Unseen('transfers')
            )

        } catch (err) {
            
        }
    }

    @Action(GetTransferStatus)
    getTransferStatus({dispatch}:StateContext<TransferStateModel>,{id}:GetTransferStatus){
        let interval=setInterval(async () => {
            try {
    
                const response=await this.service.getTransfer(id);
                if (!response.success){
                    throw Error(response.message);
                }

                const _transfer=response.data;
                const userId: string = _transfer['userId']['_id'];
                const transfer = new TransferModel(_transfer);

                if (transfer.status === 'pending'){
                    return;
                }

                dispatch(
                    [
                        new SelectTransfers(transfer),
                        new UpdateTransferStatus(transfer),
                        new Get_Unseen('transfers'),
                        new GetTransferCredit(userId),
                        new GetTotal(userId),
                        new GetTransfersByUser(userId),
                        new Toggle(false),
                        new Navigate([`0/transfers/${transfer['_id']}`]),
                        new Toggle(false)
                    ]
                ); 
    
                swal(
                    `<h2>Transaction status</h2>`,
                    `<h5>${transfer.getDebitStatus()}</h5>`,
                    'info'
                );

                return clearInterval(interval);

            } catch (err) {
                clearInterval(interval);
            }
        }, 1000*2);
    }


    @Action(Get_Admin_Summary)
    async get_admin_summary ({ patchState }: StateContext<TransferStateModel>) {
        try {

            const response = await this.service.get_summary();
            if (response.success) {
                return patchState(
                    {
                        summary: response.data[0]
                    }
                )
            }
            throw Error(response.message);
            
        } catch (err) {
        }
    }



    @Action(Get_User_Transfer_Summary)
    async get_user_transfer_history ({ patchState }: StateContext<TransferStateModel>, { userId }: Get_User_Transfer_Summary) {
        try {

            const response = await this.service.get_user_summary(userId);
            if (response.success) {
                return patchState(
                    {
                        summary: response.data[0]
                    }
                )
            }

            throw Error(response.message);
        } catch (err) {
        }
    }




    @Action(SearchFilter)
    async searchFilter ({ patchState }: StateContext<TransferStateModel>, { search }: SearchFilter) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.search_filter(search);
            if (response.success) {
                const transactions = response.data.map(d => new TransferModel(d,this.store));
                return patchState(
                    {
                        loading: false,
                        transfers: transactions
                    }
                )

            }

            throw Error(response.message);
        } catch (err) {
            patchState(
                {
                    loading: false
                }
            )
            
        }
    }




    @Action(HandleVbv)
    async handlerVbv ({ patchState, dispatch }: StateContext<TransferStateModel>, { payload }: HandleVbv) {
        try {

            patchState(
                {
                    loading: true
                }
            )

            const response = await this.service.handlerVbv(payload);
            const {
                data,
                token
            } = response;

            dispatch(
                [
                    new SetUser(
                        {
                            user: data['userId'],
                            token: token
                        }
                    ),
                    new SetSeenForTransnfer(
                        {
                            id: data['_id'],
                            data: {
                                seen: true
                            }
                        }
                    ),
                    new SelectTransfers(data),
                    new Navigate([`0/transfers/${data['_id']}`])
                ]
            )

            swal.fire(
                {
                    html: `<h3>${response.message}</h3>`
                }
            )
        } catch (err) {
            dispatch(new Navigate(['login']));
            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }
    }




    

}




