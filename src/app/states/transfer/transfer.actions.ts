import { TransferModel, TransferInterfaceModel } from "src/app/models/transfers/transfers.model";
import { SubscriptionUpdateModel } from "src/app/models/subscriptions/subscriptions.model";
import { TransactionsModel } from "src/app/models/transactions/transactions.model";




export class GetTransfers {
    static readonly type = '[Transfers] GetTransfers]';
}

export class GetTransferStatus {
    static readonly type = '[Transfers] GetTransfers]';
    constructor (public readonly id: string) {}
}


export class GetTransfersById {
    static readonly type = '[Transfers] GetTransfersById';
    constructor (public readonly id: string) {}
}


export class GetTransfersByUser {
    static readonly type = '[Transfers] GetTransfersByUser';
    constructor (public readonly id: string) {}
}


export class NewTransfers {
    static readonly type = '[Transfers] NewTransfers';
    constructor (public readonly data: TransferModel) {};
}



export class UpdateTransfers {
    static readonly type = '[Transfers] UpdateTransfers';
    constructor (public readonly data: SubscriptionUpdateModel) {};
}



export class DeleteTransfers {
    static readonly type = '[Transfers] DeleteTransfers';
    constructor (public readonly id: string) {};
}



export class SelectTransfers {
    static readonly type = '[Transfers] SelectTransfers';
    constructor (public readonly transfer: TransferModel) {};
}


export class QueNameChecks {
    static readonly type = '[Transfers] QueNameChecks';
    constructor (public readonly data: any[]) {};
}

export class GetTransferTransactions {
    static readonly type = '[Transfers] GetTransferTransactions';
    constructor(public readonly id: string) {}
}



export class ToggleTransfersEdit {
    static readonly type = '[Transfers] ToggleTransfersEdit';
    constructor (public readonly event: boolean) {};
}



export class UpdateTransferStatus {
    static readonly type = '[Transfers] UpdateTransferStatus';
    constructor(public readonly transfer: TransferModel) {}
}



export class UpdateTransactionStatus {
    static readonly type = '[Transfers] UpdateTransactionStatus';
    constructor (public readonly transaction: TransactionsModel) {};
}



export class SelectTransaction {
    static readonly type = '[Transfers] SelectTransaction';
    constructor(public readonly transaction: TransactionsModel) {}
}



export class PushTransferSearchResults {
    static readonly type = '[Transfers] PushTransferSearchResults';
    constructor(public readonly transfers?: TransferInterfaceModel[]) {}
}



export class GetExcel {
    static readonly type = '[Transfers] GetExcel';
    constructor(public readonly data: { csv: any }) {};
}



export class GetTransferCredit {
    static readonly type = '[Transfers] GetTransferCredit';
    constructor(public readonly userId?: string) {}
}


export class GetTotal {
    static readonly type = '[Transfers] GetTotal';
    constructor(public readonly userId: string) {}
}


export class SaveTransfer {
    static readonly type = '[Transfers] SaveTransfer';
    constructor(public readonly transfer: { transferId: string, name: string, userId: string }) {}
}



export class GetUserHistory {
    static readonly type = '[Transfers] GetHistory';
    constructor(public readonly userId: string) {};
}



export class GetTransferHistory {
    static readonly type = '[Transfers] GetTransferHistory';
    constructor(public readonly transferId: string) {};
}


export class ClearHistory {
    static readonly type = '[Transfers] ClearHistory';
}


export class FilterTransferDate {
    static readonly type = '[Transfers] FilterTransferDate';
    constructor(public readonly date: any[]) {}
}



export class OnSelectedTransfer {
    static readonly type = '[Transfers] OnSelectedTransfer';
    constructor(public readonly id?: string) {};
}


export class SetSeenForTransnfer {
    static readonly type = '[Transfers] SetSeenForTransnfer';
    constructor(public readonly data: { id: string, data: any }) {};
}



export class FilterUserTranfersByDate {
    static readonly type = '[Transfers] FilterUserTranfersByDate';
    constructor(public readonly data: { id: string, data: any[] }) {};
}



export class Get_Admin_Summary {
    static readonly type = '[Transfers] Get_Admin_Summary';
}


export class Get_User_Transfer_Summary {
    static readonly type = '[Transfers] Get_User_Transfer_Summary]';
    constructor(public readonly userId: string) {};
}

export class GetIssuers {
    static readonly type = '[Transfers] GetIssuers]';
}


export class FilterTransfers {
    static readonly type = '[Transfers] FilterTransfers]';
    constructor(public readonly filter: {filter: TransferInterfaceModel}) {};
}


export class SearchFilter {
    static readonly type = '[Transfers] SearchFilter]';
    constructor(public readonly search: string) {};
}


export class AuthorizePayment {
    static readonly type = '[Transfers] AuthorizePayment]';
    constructor(public readonly payload?: any) {}
}



export class HandleVbv {
    static readonly type = '[Transfers] HandleVbv]';
    constructor(public readonly payload: any) {};
}


export class PayInvoice {
    static readonly type = '[Transfers] PayInvoice]';
    constructor(public readonly payload?: any) {}
}
