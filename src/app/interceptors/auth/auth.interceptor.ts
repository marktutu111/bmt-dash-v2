import { Injectable } from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { AuthState } from 'src/app/states/auth/auth.state';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor (private store: Store, private router: Router) {}
    intercept(request: HttpRequest<any>, next: HttpHandler) {
        const { token } = this.store.selectSnapshot(AuthState);
        request = request.clone({
                setHeaders: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token || ''}`
                }
        })

        return next.handle(request);
    }

}