import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { AuthState } from '../states/auth/auth.state';



@Injectable()
export class HomeGuard implements CanActivate {

    constructor (private store: Store, private router: Router) {};

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {

        const user = this.store.selectSnapshot(AuthState.user);
        if (user && user['_id']) {
            return true;
        }

        this.router.navigate(['login']);
        return false;

        
    }
}
