import { Injectable } from '@angular/core';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { AuthState } from '../states/auth/auth.state';

@Injectable()
export class AuthGuard implements CanActivateChild {

    constructor (private store: Store) {};

    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {

        const user = this.store.selectSnapshot(AuthState.user);
        if (user && user['_id']) {
            return false;
        }

        return true;

    }
}
