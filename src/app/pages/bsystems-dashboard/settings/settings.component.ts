import { Component, OnInit } from '@angular/core';
import { Select, Selector, Store } from '@ngxs/store';
import { SettingsState } from 'src/app/states/settings/settings.state';
import { Observable, Subscription } from 'rxjs';
import { GetSettings, UpdateSettings } from 'src/app/states/settings/settings.actions';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { UsersState } from 'src/app/states/users/users.state';
import { GetUsers, UpdateUser } from 'src/app/states/users/users.actions';
import { ProgressBarState } from 'src/app/components/progress-bar/progress-bar.state';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  aggregators:string[]=[
    'HUBTEL',
    'GHIPPS',
    'ITC'
  ]

  @Select(ProgressBarState.loading) loading$: Observable<boolean>;
  @Select(SettingsState.settings) settings$: Observable<any>;
  @Select(SettingsState.settings) charge$: Observable<any>;
  @Select(UsersState.users) users$:Observable<any[]>;

  formGroup: FormGroup;
  private subscription$: Subscription;
  

  constructor(private store:Store,private fb:FormBuilder) {}

  ngOnInit() {

      this.formGroup=this.fb.group(
        {
          settings:new FormGroup(
            {
              debit_aggregator: new FormControl(null,Validators.required),
              credit_aggregator: new FormControl(null,Validators.required),
              charge: new FormControl(null,Validators.required)
            }
          ),
          topup:new FormGroup(
            {
              id:new FormControl(null,Validators.required),
              amount:new FormControl(null,Validators.required)
            }
          )
        }
      )

      this.subscription$ = this.settings$.subscribe(v => {
        if (v) {
          this.formGroup.patchValue(
            {
              settings:v
            }
          )
        }
      })

      this.store.dispatch(
        [
          new GetSettings(),
          new GetUsers()
        ]
      );

  };


  save = () => {
    if (this.formGroup.get('settings').valid) {
      this.store.dispatch(
        new UpdateSettings(this.formGroup.get('settings').value)
      )
    }
  }

  topup(){
    const topup=this.formGroup.get('topup');
    if(topup.valid){
      const {id,amount}=topup.value;
      this.store.dispatch(
        new UpdateUser(
          id,
          {credit:amount}
        )
      )
    }
  }


  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription$.unsubscribe();

  }


}
