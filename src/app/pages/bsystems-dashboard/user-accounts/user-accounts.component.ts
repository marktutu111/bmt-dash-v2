import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { GetUsers } from 'src/app/states/users/users.actions';
import { UsersState } from 'src/app/states/users/users.state';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/models/users';



@Component({
    selector: 'app-user-accounts',
    templateUrl: './user-accounts.component.html',
    styleUrls: ['./user-accounts.component.scss']
})
export class UserAccountsComponent implements OnInit {

    @Select(UsersState.users) users$: Observable<IUser[]>;
    @Select(UsersState.loading) loading$: Observable<boolean>;

    p: number = 1;

    constructor(private store: Store) {};

    ngOnInit(): void {
        this.store.dispatch(
            new GetUsers()
        )
    };


    onDateChange () {

    }



}
