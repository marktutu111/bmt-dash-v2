import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { SelectTransfers, GetTransfersByUser, FilterTransferDate, GetTransfers, Get_Admin_Summary, FilterTransfers, SearchFilter, GetTransferTransactions } from 'src/app/states/transfer/transfer.actions';
import * as moment from "moment";



@Component({
  selector: 'app-transfer-history',
  templateUrl: './transfer-history.component.html',
  styleUrls: ['./transfer-history.component.scss']
})
export class TransferHistoryComponent implements OnInit {

  
  @Select(TransfersState.loading) loading$: Observable<boolean>;
  @Select(TransfersState.transfers) transfers$: Observable<TransferModel[]>;
  @Select(TransfersState.summary) summary$: Observable<any>;
  @Select(TransfersState.selected) transfer$: Observable<any>;
  @Select(TransfersState.transactions) transactions$: Observable<any[]>;


  p: number = 1;
  // p2: number = 1;

  search: string = '';
  


  constructor(private  store: Store) {};



  ngOnInit() {

    document.onkeypress = (e) => {
      if (e.charCode && e.charCode === 13) {
        this.store.dispatch(
          new SearchFilter(this.search)
        )
      }
    }

    this.store.dispatch(
      [
        new GetTransfers(),
        new Get_Admin_Summary()
      ]
    );

  }


  onDateChange (v) {
    this.store.dispatch(
      new FilterTransferDate(v)
    )
  }

  view (d) {
    this.store.dispatch(
      new SelectTransfers(d)
    )
  }


  openTransactions () {
    const selected_transfer = this.store.selectSnapshot(TransfersState.selected);
    if (selected_transfer && selected_transfer['_id']) {
        this.store.dispatch(
            new GetTransferTransactions(selected_transfer._id)
        )
    }
  }


  getTime (date) {
    return moment(date).fromNow();
    // 
  }



  onSearch (e) {
    
  }


}
