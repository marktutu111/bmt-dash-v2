import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { TransferHistoryComponent } from './transfer-history.component';
import { SidebarModule } from 'src/app/components/sidebar/sidebar.module';
import { NavbarModule } from 'src/app/components/navbar/navbar.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule } from '@angular/forms';
import { DateFilterModule } from 'src/app/components/date-filter.component/date-filter.module';




const route: Routes = [
    {
        path: '',
        component: TransferHistoryComponent
    }
]




@NgModule({
    declarations: [
        TransferHistoryComponent
    ],
    imports: [ 
        SidebarModule,
        NavbarModule,
        NgxPaginationModule,
        FormsModule,
        DateFilterModule,
        CommonModule,
        RouterModule.forChild(route)
    ],
    exports: [],
    providers: [],
})
export class TransferHistoryModule {}