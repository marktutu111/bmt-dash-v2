import { Routes, RouterModule } from '@angular/router';
import { TransferHistoryComponent } from './transfer-history.component';
import { TransactionListComponent } from '../../main/transfers/transaction-list/transaction-list.component';
// import {} from '../transfer-history/transfer-history.module'

export const THroutes: Routes = [
    {
        path:'',
        component: TransferHistoryComponent
    },
    {
        path: 'history',
        loadChildren:'../transfer-history/transfer-history.module#TransferHistoryModule',
        
        children:[
            {
                path:':id',
                component:TransactionListComponent
            }
        ]
    },
];


