import { FormGroup, FormControl, Validator, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UsersState } from 'src/app/states/users/users.state';
import { Observable } from 'rxjs';
import { IUser } from 'src/app/models/users';
import { GetBsystemsAccounts, AddUser } from 'src/app/states/users/users.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { AlertState } from 'src/app/states/alerts/alerts.state';
import { LAlert } from 'src/app/components/lazy-alert/model';


declare const swal: any;


@Component({
    selector: 'app-admin-roles',
    templateUrl: './admin-roles.component.html',
    styleUrls: ['./admin-roles.component.scss']
})
export class AdminRolesComponent implements OnInit {

    formGroup: FormGroup;

    @Select(AlertState.alert) alert$:Observable<LAlert>;
    @Select(UsersState.admin_accounts) users$: Observable<IUser>;
    @Select(UsersState.loading) loading$: Observable<boolean>;

    // p: number = 1;
    selectedFile : File=null;
    URL = "../../../../assets/images/computer.png"

    constructor(private store: Store) {};

    
    ngOnInit(): void {
        this.formGroup = new FormGroup({
            fullname: new FormControl(null,Validators.required),
            phone:new FormControl(null,Validators.required),
            email: new FormControl(null, Validators.required),
            company_name: new FormControl(null, Validators.required),
            password: new FormControl(null, Validators.required),
            company_logo: new FormControl('../../../../assets/images/preview.png', Validators.required)
        })
      
    };

    
    onFileSelected(event){
      this.selectedFile = <File>event.target.files[0];
      if(event.target.files){
        var reader = new FileReader();
        reader.onloadend =(event:any)=>{
          const base64 = reader.result;
          this.formGroup.patchValue({company_logo:base64})
        }

        reader.readAsDataURL(<File>event.target.files[0]);

      }
    }

    onSubmit(){
      
        if(this.formGroup.valid){
            const id:string = this.store.selectSnapshot(AuthState.user)._id;
            return this.store.dispatch(
              new AddUser(
                {
                  ...this.formGroup.value,
                  account_type:'company',
                  company_role:'authorizer'
                }
              )

            )
      
        }

        this.URL="../../../../assets/images/computer.png";
    }
 





}
