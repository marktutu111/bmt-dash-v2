import { Component, OnInit } from '@angular/core';
import sidebar from './config';
import { Store } from '@ngxs/store';
import { AccountType } from 'src/app/states/app/actions';



@Component({
    selector: 'app-bsystems-dashboard',
    templateUrl: './bsystems-dashboard.component.html',
    styleUrls: ['./bsystems-dashboard.component.scss']
})
export class BsystemsDashboardComponent implements OnInit {

    sidebar: any[] = sidebar;


    constructor (private store: Store) {};


    ngOnInit(): void {
        this.store.dispatch(
            new AccountType('bsystems')
        )
    }



}
