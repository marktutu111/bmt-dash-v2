import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { SelectTransfers, GetTransfersByUser, FilterTransferDate, GetTransfers, Get_Admin_Summary, FilterTransfers, SearchFilter } from 'src/app/states/transfer/transfer.actions';



@Component({
  selector: 'app-transfers-table',
  templateUrl: './transfers-table.component.html',
  styleUrls: ['./transfers-table.component.scss']
})
export class TransfersTableComponent implements OnInit {


      @Select(TransfersState.loading) loading$: Observable<boolean>;
      @Select(TransfersState.transfers) transfers$: Observable<TransferModel[]>;
      @Select(TransfersState.summary) summary$: Observable<any>;


      p: number = 1;
      search: string = '';
      


      constructor(private  store: Store) {};



      ngOnInit() {

        document.onkeypress = (e) => {
          if (e.charCode && e.charCode === 13) {
            this.store.dispatch(
              new SearchFilter(this.search)
            )
          }
        }

        this.store.dispatch(
          [
            new GetTransfers(),
            new Get_Admin_Summary()
          ]
        );

      }



      onDateChange (v) {
        this.store.dispatch(
          new FilterTransferDate(v)
        )
      }

      
}
