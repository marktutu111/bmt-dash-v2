import { InvoiceListComponent } from './invoice-list.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Listroutes } from './invoice-list.routing';

@NgModule({
    declarations: [
        InvoiceListComponent
    ],
    imports: [ 
        CommonModule,
        RouterModule.forChild(Listroutes)
    ],
    exports: [],
    providers: [],
})
export class InvoicelistModule {}