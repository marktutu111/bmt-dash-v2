import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { BsystemsDashboardComponent } from './bsystems-dashboard.component';
import { SidebarModule } from 'src/app/components/sidebar/sidebar.module';
import { NavbarModule } from 'src/app/components/navbar/navbar.module';
import { TransfersTableComponent } from './transfers/transfers-table.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { DateFilterModule } from 'src/app/components/date-filter.component/date-filter.module';
import { UserAccountsComponent } from './user-accounts/user-accounts.component';
import { AdminRolesComponent } from './admin-roles/admin-roles.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchModule } from 'src/app/components/search-component/search.module';
import { SettingsComponent } from './settings/settings.component';
import { ProgressBarModule } from 'src/app/components/progress-bar/progress-bar.module';
import { LazyAlertModule } from 'src/app/components/lazy-alert/lazy-alert.module';


const routes: Routes = [
    {
        
        path: '',
        component: BsystemsDashboardComponent,
        children: [
            {
                path: '',
                redirectTo: 'transactions',
                pathMatch: 'full'
            },
            {
                path: 'transactions',
                component: TransfersTableComponent
            },
            {
                path: 'accounts',
                component: UserAccountsComponent
            },
            {
                path: 'roles',
                component: AdminRolesComponent
            },
            {
                path:'settings',
                component:SettingsComponent
            },
            {
                path: 'history',
                loadChildren: './transfer-history/transfer-history.module#TransferHistoryModule'
            },
            {
                path: 'invoice',
                loadChildren: '../main/invoice/invoice.module#InvoiceModule'
            }
        ]
    },
   
]



@NgModule({
    declarations: [
        BsystemsDashboardComponent,
        TransfersTableComponent,
        UserAccountsComponent,
        AdminRolesComponent,
        SettingsComponent
    ],
    imports: [ 
        CommonModule,
        SidebarModule,
        NavbarModule,
        NgxPaginationModule,
        FormsModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        SearchModule,
        DateFilterModule,
        ProgressBarModule,
        LazyAlertModule
    ],
    exports: [],
    providers: [],
})
export class BsystemsDashboardModule {}