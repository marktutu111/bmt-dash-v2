
const sidebar = [
    {
        path: 'roles',
        name: 'Manage Roles',
        icon: 'fa fa-users'
    },
    {
        path: 'transfers',
        name: 'Transaction History',
        icon: "fa fa-clock-o"
    }
]


export default sidebar;