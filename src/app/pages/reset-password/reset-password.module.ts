import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResetPasswordComponent } from './reset-password.component';
import { RouterModule } from '@angular/router';
import { reesetPasswordRoutes } from './reset-password.routing';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ResetPasswordComponent
    ],
    imports: [
         CommonModule,
         ReactiveFormsModule,
         RouterModule.forChild(reesetPasswordRoutes)
        ],
    exports: [],
    providers: [],
})
export class resetPasswordModule {}