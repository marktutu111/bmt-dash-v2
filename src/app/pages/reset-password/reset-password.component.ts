import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { ResetPassword } from 'src/app/states/auth/auth.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { Observable } from 'rxjs';

declare const swal: any;

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['../login/login.component.css']
})
export class ResetPasswordComponent implements OnInit {


  @Select(AuthState.alert) alert$: Observable<any>;
  @Select(AuthState.loading) loading$: Observable<boolean>;
  formGroup: FormGroup;


  constructor(private store: Store, private route:Router) {};



  ngOnInit() {
    this.formGroup  = new FormGroup(
      {
        otp: new FormControl(null, Validators.required),
        newpassword: new FormControl(null,Validators.required),
        retypepassword: new FormControl(null,Validators.required),
      }
    );
  }


  resetpassword () {
    if (this.formGroup.valid) {

      const {
        otp,
        newpassword,
        retypepassword
      } = this.formGroup.value;

      if (newpassword !== retypepassword) {
        return swal.fire(
          {
            html: `<h4>Your password does not match</h4>`
          }
        )
      }
     

      this.store.dispatch(
        new ResetPassword(
          {
            otp: otp,
            password: retypepassword
          }
        )
      )

    }
  }
  Login(){
        this.route.navigate(['./login'])
  }

  


}
