import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { forgotpasswordroutes } from './forgot-password.routing';
import { ForgotPasswordComponent } from './forgot-password.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ForgotPasswordComponent
    ],
    imports: [
         CommonModule,
         ReactiveFormsModule,
         RouterModule.forChild(forgotpasswordroutes)
         ],
    exports: [],
    providers: [],
})
export class forgotPasswordModule {}