import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { ForgotPassword } from 'src/app/states/auth/auth.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { Observable } from 'rxjs';
// import {} from '../login/login.component'
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['../login/login.component.css']
})
export class ForgotPasswordComponent implements OnInit {


  @Select(AuthState.loading) loading$: Observable<boolean>;
  @Select(AuthState.alert) alert$: Observable<any>;
  
  formGroup:FormGroup;


  constructor(private store: Store) {};


  ngOnInit() {
    this.formGroup=new FormGroup({
      email: new FormControl('',Validators.required),
    });
  }

  forgotpassword () {
    if (this.formGroup.valid) {
      this.store.dispatch(
        new ForgotPassword(
          this.formGroup.value
        )
      )
    }
  }


}
