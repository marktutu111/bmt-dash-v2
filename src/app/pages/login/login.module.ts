import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { routes } from './login.routes';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

@NgModule({
    declarations: [
        LoginComponent,
        // ForgotPasswordComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(routes)
    ],
    exports: [],
    providers: [],
})
export class AuthModule {}