import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { AuthState, AuthAlertInterface } from 'src/app/states/auth/auth.state';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Login } from 'src/app/states/auth/auth.actions';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Select(AuthState.loading) loading$: Observable<boolean>;
  @Select(AuthState.alert) alert$: Observable<AuthAlertInterface>;

  formGroup: FormGroup;

  constructor(private store: Store) {};


  ngOnInit() {
    this.formGroup  = new FormGroup({
      email: new FormControl('',Validators.required),
      password: new FormControl('',Validators.required),
      remember: new FormControl(false)
    });

  }

  login () {
    if (this.formGroup.valid) {
      const { email, password, remember } = this.formGroup.value;
      this.store.dispatch(
        new Login(
            {
              email: email.trim(),
              password: password.trim(),
              remember: remember
            }
          )
      )
    }
  }



}
