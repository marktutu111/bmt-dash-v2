import { Routes } from "@angular/router";
import { LoginComponent } from "./login.component";
// import {} from './forgot-password/'
import { ForgotPasswordComponent } from "../forgot-password/forgot-password.component";

export const routes: Routes = [
    {
        path: '',
        component: LoginComponent,
    } 
]