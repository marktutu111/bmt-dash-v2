import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { HandleVbv } from 'src/app/states/transfer/transfer.actions';


@Component({
    selector: 'app-redirect',
    templateUrl: './redirect.component.html',
    styleUrls: ['./redirect.component.scss']
})
export class RedirectComponent implements OnInit {


    constructor(private route: ActivatedRoute, private store: Store) {};

    
    ngOnInit(): void {
        this.route.queryParams.subscribe(({ code, transaction_id, reason, status }) => {

            const data = {
                code: code,
                reason: reason,
                transaction_id: transaction_id,
                status: status
            }

            this.store.dispatch(
                new HandleVbv(data)
            )

        })
    };




}
