import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { signUproutes } from './signup.routing';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignUpComponent } from './signup.component';

@NgModule({
    declarations: [
        SignUpComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule.forChild(signUproutes)
    ],
    exports: [],
    providers: [],
})
export class SignupModule {}