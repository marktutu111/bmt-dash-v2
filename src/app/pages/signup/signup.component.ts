import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { SignupState } from '../../states/signup/signup.state';
// ../states/signup/signup.state
import { Observable } from 'rxjs';
import { NewSignup } from '../../states/signup/signup.actions';
// ../states/signup/signup.action
import { SingupInterfaceModel } from '../../models/auth/index';
import { NotificationsService } from "angular2-notifications";
// import { Countries } from '../constants/countries';
import { MustMatch } from '../_helpers/must-match.validator';

declare const $: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignUpComponent implements OnInit {


  @Select(SignupState.loading) loading$: Observable<boolean>;
  @Select(SignupState.alert) alert$: Observable<any>;

  
  // countries: any[] = Countries;
  signupForm: FormGroup;

  submitted=false;
  passwordType: string = 'password';
  passwordShown: boolean = false;


  constructor(private store: Store, private notifications: NotificationsService, private formBuilder:FormBuilder) {};

  ngOnInit() {

    this.signupForm = this.formBuilder.group(
      {
        fullname: new FormControl(null, Validators.required),
        email: new FormControl(null, [Validators.required, Validators.email]),
        password: new FormControl(null, [Validators.required, Validators.minLength(6)]),
        repeatPassword: new FormControl(null, Validators.required),
        phone: new FormControl(null, [Validators.required, Validators.pattern('^[0-9]*$'), Validators.minLength(10)]),
        agreement: new FormControl(true)
      },
      {
        validator: MustMatch('password', 'repeatPassword')
    }
    );

    // $('document').ready(function(){
    //   $('select').select2({
    //           dropdownParent: $('.select-dropdown')
    //       });
    // });

  }
    // convenience getter for easy access to form fields
    get f() { return this.signupForm.controls; }

  signup () {

    this.submitted=true;
    if (this.signupForm.valid) {

      let {
        email,
        phone,
        password,
        repeatPassword,
        agreement,
        fullname
      } = this.signupForm.value;

      if (password !== repeatPassword) {
        return this.notifications.error(
          'Signup Error',
          'Password does not match',
          {
            timeOut: 1000 * 4,
            showProgressBar: false,
            clickToClose: true,
          }
        )
      }

      const data: SingupInterfaceModel = {
          fullname: fullname,
          phone:phone,
          password: password.trim(),
          email: email.trim(),
          
      }

      this.store.dispatch(
        new NewSignup(data)
      )

      if (this.signupForm.invalid) {
        return;
      }

    }
  }

  showPassword() {
    if(this.passwordShown){
      this.passwordShown=false;
      this.passwordType= 'password';
    }
    else{
      this.passwordShown=true;
      this.passwordType='text';
    }
   }

}