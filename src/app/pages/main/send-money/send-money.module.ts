import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SendMoneyComponent } from './send-money.module.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SideBarWideModule } from 'src/app/components/side-content-full/side-content-full.module';
import { TransfersFormGridComponent } from './transfers-form-grid/transfers-form-grid.component';
import { CheckImageModule } from 'src/app/components/check-image-component/check-image.module';
import { ContactsFormModule } from 'src/app/components/contacts-form/contacts-form.module';
import { TheTellerComponentModule } from 'src/app/components/theteller/theteller.component.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { NamecheckComponent } from './namecheck/namecheck.component';


const routes: Routes = [
    {
        path: '',
        component: SendMoneyComponent
    },
    {
        path:'namecheck',
        component:NamecheckComponent
    }
]


@NgModule({
    declarations: [
        SendMoneyComponent,
        TransfersFormGridComponent,
        NamecheckComponent
    ],
    imports: [ 
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        FormsModule,
        SideBarWideModule,
        CheckImageModule,
        ContactsFormModule,
        TheTellerComponentModule,
        Ng2SearchPipeModule,
        NgxPaginationModule,
    ],
    exports: [],
    providers: [],
})
export class SendMoneyModule {}