import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { Observable } from 'rxjs';
import { NameCheckModel } from 'src/app/models/namecheck/namecheck.model';
import { API } from 'src/app/constants/api';
import { AuthState } from 'src/app/states/auth/auth.state';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';
import { Location } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { AccountsModel } from 'src/app/models/favourites/favourites.model';
import { QueNameChecks } from 'src/app/states/transfer/transfer.actions';
declare const swal:any;

@Component({
    selector: 'app-namecheck',
    templateUrl: './namecheck.component.html',
    styleUrls: ['./namecheck.component.scss']
})
export class NamecheckComponent implements OnInit {

    @Select(TransfersState.issuers) issuers$:Observable<any[]>;


    names:any[]=[];
    index:number = 0;
    uploading=true;
    ratios:string[]=[];
    _ratios:any[]=[];
    onfilter:boolean=false;

    page: number = 1;
    //   searchText;
      totalRec : number;


    constructor(private store:Store, private router: Router,private route:ActivatedRoute) {};
    back () {
        this.router.navigate(['./'])
    }

    ngOnInit() {
        this.names=this.store.selectSnapshot(TransfersState.namecheck).map(d=>new NameCheckModel(d));
        this.checkName();
    };


    checkName=async()=>{

        let names=[...this.names];
        let name:NameCheckModel=null;

        const updateArray=(_name:NameCheckModel)=>{
            this.names=this.names.map((name:NameCheckModel)=>name.account_number===_name.account_number?_name:name);
        }

        const reque=()=>{
            if (names.length <= 0){
                swal({html: `<h4>Name Enquiry completed</h4>`});
                this.pushRatioToArray();
                this.store.dispatch(new Toggle(false));
                return this.uploading=false;
            } else que();
        }

        const que=async()=>{
            try {

                this.store.dispatch(new Toggle(true));
                this.uploading=true;
                name=names.shift();
                const {_id,fullname}=this.store.selectSnapshot(AuthState.user);

                name.setData('loading',true);
                name.setData('checked',false);
                name.setData('ratio','0%');

                const data=name.getData(['account_number','account_issuer','account_name']);
                const response=await this.fetchNec({...data,nameToDebit:fullname,userId:_id});
                name.setData('loading',false);

                switch (response.success) {
                    case true:
                        name.setData('name_received',response.data);
                        const d = this.compare(data['account_name'],response.data);
                        name.setData('checked',d['match']);
                        name.setData('ratio',d['ratio']);
                        break;
                    default:
                        name.setData('name_received','invalid');
                        name.setData('checked',false);
                        name.setData('ratio','0%');
                        break;
                }

                updateArray(name);
                reque();
            } catch (err) {
                name.setData('loading',false);
                name.setData('name_received',err.message);
                name.setData('checked',false);
                name.setData('ratio','0%');
                reque();
            }
        }

        que();
        
    }

    fetchNec=(data)=>{
        const url:string=`${API.NEC}/get?account_number=${data['account_number']}&account_issuer=${data['account_issuer']}&nameToDebit=${data['nameToDebit']}&userId=${data['userId']}`;
        return fetch(url,{
            headers:{
                'Authorization':this.store.selectSnapshot(AuthState.token)
            },
        }).then(res=>res.json()).catch(err=>err);
    }

    compare=(account_name:string,name_received:string):any=>{
        try {
            let match_ratio:number=0;
            let names = account_name.split(' ');
            let recieved=name_received.split(' ');
            names.forEach((name:string)=>{
                recieved.forEach(r=>{
                    if (r.toLowerCase() === name.toLowerCase()){
                        match_ratio++;
                    }
                })
            });
    
            let diff=recieved.length - match_ratio;
    
            if (diff===0){
                return {
                    ratio:'100%',
                    match:true
                };
            }
    
            if (diff === 1){
                return {
                    ratio:'50%',
                    match:true
                };
            }
    
            if (diff > 1){
                return {
                    ratio:'30%',
                    match:true
                };
            }
    
            return {
                ratio:'0%',
                match:false
            };
            
        } catch (err) {
            return {
                ratio:'0%',
                match:false
            };
        }
    }

    pushRatioToArray=()=>{
        this.names.forEach((name:NameCheckModel)=>{
            if (this.ratios.indexOf(name.ratio) <= -1){
                this.ratios.push(name.ratio);
            }
        });

        this.store.dispatch(new Toggle(false));

    }


    filter=(e)=>{
        const ratio=e.target.value;
        if (ratio === ''){
            this.onfilter=false;
            return this._ratios=[];
        }
       this.onfilter=true;
       this._ratios=this.names.filter((name:NameCheckModel) => name.ratio===ratio);
    };


    remove=(i:number)=>{
        this.names = this.names.filter((n,index)=>i!==index);
    }


    que=()=>{
        this.store.dispatch(new Toggle(true));
        let filter:any[]=[];
        this.store.selectSnapshot(TransfersState.namecheck).forEach(account=>{
            this.names.forEach((name:NameCheckModel)=>{
                if (name.account_number === account.account_number){
                    filter.push(
                        {
                            ...account,
                            account_name:name.name_received
                        }
                    );
                }
            })
        });

        this.store.dispatch(
            [
                new QueNameChecks(filter),
                new Toggle(false)
            ]
        ).toPromise().then(()=>this.router.navigate(['../'], {relativeTo:this.route})).catch(err=>null);

    }



}
