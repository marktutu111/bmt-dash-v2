import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { CheckImageModel } from 'src/app/components/check-image-component/check-image.model';
import { TransferInterfaceModel } from 'src/app/models/transfers/transfers.model';
import { AuthState } from 'src/app/states/auth/auth.state';
import { IUser } from 'src/app/models/users';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ToggleSideContent } from 'src/app/states/app/actions';
import { NewTransfers, GetTransferCredit, ClearHistory, GetIssuers, QueNameChecks } from 'src/app/states/transfer/transfer.actions';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { PaymentAccountState } from 'src/app/states/payment-accounts/state';
import { GetAccounts, NewPaymentAccount } from 'src/app/states/payment-accounts/actions';
import { AppState } from 'src/app/states/app/state';
import { SaveFavourite, GetFavourites } from 'src/app/states/favourites/favourites.state';
import { FavState } from 'src/app/states/favourites/favourites.actions';
import charge_system from 'src/app/bmt-charge';
import calculate_bank_charge from 'src/app/bmt-charge/calculate-bank-charge';
import { API } from 'src/app/constants/api';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';
declare const swal: any;




@Component({
    selector: 'app-transfers-form-grid',
    templateUrl: './transfers-form-grid.component.html',
    styleUrls: ['./transfers-form-grid.scss']
})
export class TransfersFormGridComponent implements OnInit {

    @Select(TransfersState.loading) loading$: Observable<boolean>;
    @Select(TransfersState.transferCredits) transferCredits$: Observable<number>;
    @Select(PaymentAccountState.accounts) accounts$: Observable<any[]>;
    @Select(TransfersState.issuers) issuers$:Observable<any[]>;

    // FAVOURITES
    @Select(FavState.favourites) history$: Observable<any>;
    @Select(AuthState.user) user$: Observable<IUser>;

    // CHARGES
    charges: string = '0';
    paymentType: 'manual' | 'auto' | 'none' | 'card-account' = 'none';
    
    private onHistory: boolean = false;

    searchText;
    page: number = 1;
  totalRec : number;
    networks: CheckImageModel[] = [
        {
            image: '../../../../../dashboard/assets/img/networks/mtn.jpg',
            name: 'MTN'
        },
        {
            image: '../../../../../dashboard/assets/img/networks/vodafone.png',
            name: 'VODAFONE'
        },
        {
            image: '../../../../../dashboard/assets/img/networks/tigo.png',
            name: 'TIGO'
        },
        {
            image: '../../../../../dashboard/assets/img/networks/Airtel-money.png',
            name: 'AIRTEL'
        }
    ]


    @Input() formData: TransferInterfaceModel = {
        amount: null,
        type: 'push-funds',
        canceled: null,
        account_issuer:null,
        account_name:null,
        account_number:null,
        account_voucher:null
    }


    formGroup: FormGroup;

    private _csvRecipients: Subscription;
    private _transferHistory$: Subscription;
    private amount$: Subscription;
    private temp: any = [];

    constructor (
        private store: Store,
        private formBuilder: FormBuilder,
        private router: Router, private location: Location, private route:ActivatedRoute)
    {};


    ngOnInit(): void {

        const { _id, fullname } = this.store.selectSnapshot(AuthState.user);
        this.formGroup =  this.formBuilder.group(
            {
                amount: new FormControl(this.formData.amount, Validators.required),
                payment_type: 'momo',
                account_number:new FormControl(null,Validators.required),
                account_issuer: new FormControl(null,Validators.required),
                account_name: new FormControl(fullname,Validators.required),
                account_voucher:new FormControl(null),
                accounts: this.formBuilder.array([]),
                totalTransferAmount: 0,
                totalBankTransfers: 0,
                totalBankTransferAmount: 0,
                totalMomoTransferAmount: 0,
                totalMomoTransfers: 0,
                total_transfer_count: 0,
                cardDetails: this.formBuilder.group(
                    {
                        cardNumber: new FormControl(null, Validators.required),
                        cardHolder: new FormControl(null, Validators.required),
                        expirationMonth: new FormControl(null, Validators.required),
                        expirationYear: new FormControl(null, Validators.required),
                        ccv: new FormControl(null, Validators.required)

                    }
                )
            }
        );


        this.amount$ = this.formGroup.controls['accounts'].valueChanges.subscribe(() => this.calculateAmount());
        this.store.dispatch(
            [
                new GetTransferCredit(_id),
                new GetFavourites(_id),
                new GetAccounts(_id),
                new GetIssuers()
            ]
        );


        this.getAccountsFromEnquiryQue()


    }


    getAccountsFromEnquiryQue=()=>{
        const accounts = this.store.selectSnapshot(TransfersState.namecheck);
        if (accounts.length > 0){
            return accounts.forEach(account => {
                const d={
                    account_number:account.account_number || '',
                    account_issuer:account.account_issuer || '',
                    account_type:account.account_type || '',
                    account_name:account.account_name || '',
                    amount:account.amount || '',
                    description:account.description || ''
                }
                const accountForm = this.formBuilder.group(d);
                this.accounts.push(accountForm);
            });
        }

        this.on_init_prompt();

    }


    cancel(){
        this.router.navigate(['./sendmoney'])
    }

    on_init_prompt () {
        const status = this.getStatus();
        swal.fire(
            {
                title: '<h4><b>How many people would you like to send money to?</b></h4>',
                input: 'number',
                inputPlaceholder: '1',
                showCancelButton: true,
                inputValue: 1
            }
        ).then(({ value }) => {

            if (!value) {
                return;
            }

            if (status) {
                const count: number = parseInt(value);
                if (count > 0) {
                    this.clearArray();
                    return this.generateForms(count);
                }

                swal(
                    '<h4>Oops!</h4>',
                    '<h5>Invalid number</h5>',
                    'error'
                )

            } else {
                return swal(
                    {
                        html: '<h4>You don\'t have enough prepaid to perform transaction, Please topup your account</h4>',
                    }
                )
            }


        });


    }



    networkChange (issuer: string) {
        this.formGroup.patchValue(
            {
                account_issuer:issuer
            }
        );
    }

    get accounts () {
        return this.formGroup.get('accounts') as FormArray;
    }

    clearArray () {
        while (this.accounts.length !== 0) {
            this.accounts.removeAt(0);
        }
    }

    // clearArray () {
    //     while (this.accounts.length !== 0) {
    //         this.accounts.removeAt(0);
    //     }

    onRecipientChange (e) {
        this.onHistory = false;
        const count: number = e.target.value;
        this.generateForms(count);
    }


    addMore () {
        const status = this.getStatus();
        if (status) {
            return swal.fire(
                {
                    title: '<h4><b>How many people would you like to add?</b></h4>',
                    input: 'number',
                    inputPlaceholder: '0',
                    showCancelButton: true
                }
            ).then(({ value }) => {
                if (!value) {
                    return;
                }

                const count: number = parseInt(value);
                if (count > 0) {
                    return this.generateForms(count);
                }

                swal(
                    'Oops!',
                    'Invalid number',
                    'error'
                )

            });
        }
        return swal(
            {
                html: '<h4>You don\'t have enough credit to perform transaction, Please topup your account</h4>',
            }
        )
    }



    // GET TRANSFER HISTORY FROM SELECTION
    onHistoryChange (e) {
        const id: string = e.target.value;
        const favourites = this.store.selectSnapshot(FavState.favourites);
        const fav = favourites.find(fav => fav['_id'] === id);

        this.clearArray();
        this.temp = [];

        if (!fav || !fav['_id'])return;
        const { accounts } = fav;
        if (!accounts || accounts.length <= 0)return;
        accounts.forEach(account => {
            const d={
                account_number:account.account_number || '',
                account_issuer:account.account_issuer || '',
                account_type:account.account_type || '',
                account_name:account.account_name || '',
                amount:account.amount || '',
                description:account.description || ''
            }
            const accountForm = this.formBuilder.group(d);
            this.accounts.push(accountForm);
            this.temp.push(account);
            this.onHistory = true;
        });

        this.calculateAmount();

    }

    onPaymentAccount (e) {
        const _id = e.target.value;
        if (typeof _id === 'string' && _id !== '' && _id !== 'none') {
            const account = this.store.selectSnapshot(PaymentAccountState.accounts).find(account => account['_id'] === _id);
            if (!account || !account._id)return;
            this.formGroup.patchValue(
                {
                    account_number:account['account_number'],
                    account_issuer:account['account_issuer']
                }
            )
        }
    }

    generateForms (count: number) {
        const account={
            account_number:[],
            account_issuer:[],
            account_type:[],
            account_name:[],
            amount:[],
            description:[]
        }
        for (let index = 0; index < count; index++) {
            const accountForm = this.formBuilder.group(account);
            this.accounts.push(accountForm);
        }

        this.calculateAmount();

    }



    calculateAmount () {
        try {

            let accounts = this.accounts.value;
            let totalMomoTransferAmount: number = 0;
            let totalBankTransferAmount: number = 0;
            let totalBankTransfers: number = 0;
            let totalMomoTransfers:number =0;
    
            if (accounts.length >= 1) {
                accounts.forEach(acc => {
                    let amount =parseFloat(acc['amount']) || 0;
                    switch (acc.account_type) {
                        case 'momo':
                            totalMomoTransferAmount+=amount;
                            totalMomoTransfers+=1;
                            break;
                        default:
                            totalBankTransfers+=1;
                            totalBankTransferAmount+=amount;
                            break;
                    }
                });
            }
    
            const rounded = (totalBankTransferAmount + totalMomoTransferAmount) * 10  / 10;
            this.formGroup.patchValue(
                {
                    totalTransferAmount: rounded.toString(),
                    amount: rounded.toString(),
                    totalBankTransfers: totalBankTransfers,
                    totalBankTransferAmount: totalBankTransferAmount,
                    totalMomoTransferAmount: totalMomoTransferAmount,
                    totalMomoTransfers: totalMomoTransfers,
                    total_transfer_count:accounts.length
                }
            );

            if (rounded > 0) {
                let momoCharge = charge_system(totalMomoTransferAmount);
                let bankCharge = calculate_bank_charge(totalBankTransfers);
                this.charges = (momoCharge + bankCharge).toString();
            } else {
                this.charges = '0';
            }

        } catch (err) {

        }

    }



    getCredit (credit: number) {
        return new Array(credit);
    }



    removeAccount (index: number) {
        // Get row from array
        const account = this.accounts.value.find((account, i) => i === index);
        // Check if all input fields are empty
        const not_empty = Object.keys(account).find(key => account[key] !== null);
        if (typeof not_empty === 'string') {
            swal(
                {
                    title: '<h4><b>Are you sure want to remove?</b></h4>',
                    showCancelButton: true
                }
            ).then(({ value }) => {    
                if (value) {
                    this.accounts.removeAt(index);
                }
            });

        } else {

            this.accounts.removeAt(index);

        }

        this.calculateAmount();


    }




    goBack () {
        this.location.back();
    }





    save () {

        let _null: string;
        let _index:string;
        let accounts = this.accounts.value;
        const banks = this.store.selectSnapshot(TransfersState.issuers);

        accounts = accounts.map(account=>{
            banks.forEach(bank=>{
                if (bank.id === account.account_issuer){
                    account.account_type=bank.type || '';
                }
            })
            return account;
        });

        accounts.forEach((account,i) => {
            Object.keys(account).forEach(key=>{
                if (key && account[key] === null || account[key] === ''){
                    _index=i;
                    return _null=key;
                }
            })
        });

        if (typeof _null === 'string') {
          return swal.fire(
              {
                  title: 'Oops!',
                  html: `
                    <h4>
                        Please check recipient account on row number <b>${_index+1}</b>, you have not
                        provided all details required for <b>${_null}</b> field.
                    </h4>
                  `
              }
          );
        }

        if (this.accounts.length > 0) {
            switch (this.getData().account_type) {
                case 'company':
                    const data=this.getData({control:'company'});
                    return swal.fire({
                            title: 'Confirm Transaction',
                            html: `
                                <h3 style="margin: 20px;">
                                    You have requested to send an amount of Ghs${parseFloat(data.amount).toFixed(2)} to ${this.accounts.length} number of
                                    recipients, Please confirm your transaction.
                                </h3>
                            `,
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Yes, Transfer'
                        }
                    ).then(({value})=>{
                        if (value){
                            return this.store.dispatch(
                                new NewTransfers(data)
                            )
                        }
                    }).catch(err=>null);
                default:
                    this.store.dispatch(
                        new ToggleSideContent()
                    );
                    break;
            }

        }

    }

    onCSVChange (e) {
        try {

            const { name } = e.target.files[0];
            const file_type = name.split('.')[1];

            if (file_type === 'csv') {

                this.clearArray();
                this.temp = [];

                const fileReader = new FileReader();
                fileReader.onloadend = () => {

                    const buffer = fileReader.result;
                    const csv = buffer.toString().split(/\r\n|\n/);

                    csv.forEach(account => {

                        const _account = account.split(',');
                        if (_account.length >= 5) {

                            let wallet = _account[1] ? _account[1].toString() : '';
                            // if (wallet[0] !== '0') wallet = `0${wallet}`;

                            wallet.substring(0,10);
                            const d = {
                                account_name: _account[0],
                                account_number: wallet,
                                amount: _account[2],
                                account_issuer: _account[3].toString().toLowerCase(),
                                description: _account[4],
                            }

                            const accountForm = this.formBuilder.group(d);
                            this.accounts.push(accountForm);
                            this.temp.push(d);

                        }

                    });

                    this.calculateAmount();

                }

                return fileReader.readAsBinaryString(e.target.files[0]);

            }

            throw Error('Unsupported file type');
        } catch (err) {
            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            );
        }

    }



    async confirmTransfer () {

        this.calculateAmount();
        
        const {
            amount,
            accounts,
            payment_type,
            account_issuer,
            account_number,
            account_voucher
        } = this.formGroup.value;

        if (payment_type !== 'momo' && payment_type !== 'bank'){
            if (this.formGroup.get('cardDetails').invalid) {
                return swal.fire(
                    {
                        title: `<h4><b>Invalid Payment account, Please check you have provide valid card details</b></h4>`,
                    }
                )
            }
        }

        // if (account_issuer === '300594'){
        //     if (!account_voucher || account_voucher === ''){
        //         return swal.fire(
        //             {
        //                 title: `Voucher Code`,
        //                 html:`<h4><b>Invalid Payment account, Please generate a voucher code for Vodafone cash payments</b></h4>`
        //             }
        //         )
        //     }
        // }

        this.store.dispatch(new ToggleSideContent());
        const data = this.getData(
            {
                control:'user'
            }
        );

        if (this.paymentType === 'manual') {
            let { value } = await swal.fire({
                    html: '<h4>Would you like to save sender&#146s account details?</h4>',
                    showCancelButton: true,
                    cancelButtonColor: 'red',
                    cancelButtonText: 'No, don&#146t save',
                    confirmButtonText: 'Yes, save'
                }
            );

            if (value) {
                const { value } = await swal.fire(
                    {
                        html: '<h4>Please name your account</h4>',
                        input: 'text',
                        inputPlaceholder: 'name',
                        showCancelButton: true
                    }
                )

                if (value) {
                    this.store.dispatch(
                        new NewPaymentAccount({
                            network: account_issuer,
                            wallet: account_number,
                            userId: data.userId,
                            type: 'momo',
                            name: value
                        }, true)
                    );
                } else {
                    return swal.fire(
                        {
                            html: '<h4>Invalid name, please provide a valid name</h4>'
                        }
                    )
                }
            }
        }

        swal.fire({
                html: `
                    <div style="margin: 20px;">
                        You have requested to send an amount of Ghs${parseFloat(amount).toFixed(2)} to ${this.accounts.length} number of
                        recipients, Please confirm your transaction.
                    </div>
                `,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, Transfer'
            }
        ).then((result) => {
            if (result.value) {
                if (this.onHistory) {
                    return this.confirmOtp(data)
                }
                swal({
                        title: `<h4>Do you want to save your transfer for future</h4>`,
                        type: 'warning',
                        input: 'text',
                        inputPlaceholder: 'name',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, save!',
                        cancelButtonText: 'No, don\'t save'
                    }
                ).then(result => {
                    if (result.value) {
                        const name: string = result.value;
                        this.store.dispatch(
                            new SaveFavourite(
                                {
                                    userId: data.userId,
                                    name: name,
                                    accounts: accounts
                                },true
                            )
                        )
                    }

                    this.confirmOtp(data);

                })
            }
        })
    }


    getData=(data={})=>{
        const {company_id,_id} =this.store.selectSnapshot(AuthState.user);
        const type:string= this.store.selectSnapshot(AppState.account_type);
        let d={
            ...data,...this.formGroup.value,
            userId: _id,
            type: 'push-funds',
            account_type: type,
            company_id:company_id || null
        };
        return d;
    }



    confirmOtp = async (data) => {
        try {

            if (data.payment_type === 'momo') {
                const {
                    account_number
                }=data;
                this.store.dispatch(new Toggle(true));
                const response = await fetch(API.GET_OTP, {
                    method: 'POST',
                    headers:{
                        'Authorization':this.store.selectSnapshot(AuthState.token)
                    },
                    body: JSON.stringify(
                        {
                            phoneNumber: account_number
                        }
                    )
                }).then(res => res.json());
                if (response.success) {
                    const { value } = await swal.fire(
                        {
                            html: `<h4>Please Confirm transaction by entering OTP received on your phone with number ${account_number}</h4>`,
                            input: 'text',
                            inputPlaceholder: 'name',
                            showCancelButton: true
                        }
                    )
                    if (value) {
                        return this.store.dispatch(
                            [
                                new NewTransfers(
                                    {
                                        ...data,
                                        otp: value
                                    }
                                ),
                                new Toggle(false)
                            ]
                        )
                    } throw Error('Transaction failed');                   
                } throw Error(response.message);
            } else this.store.dispatch(new NewTransfers(data));
        } catch (err) {
            this.store.dispatch(new Toggle(false));
            swal.fire(
                {
                    html: `<h4>Sorry We could not process your transaction, ${err.message}</h4>`
                }
            )
        }
        
    }



    onSearch (e) {

        const value = e.target.value;
        this.clearArray();

        if (value !== '') {
            return this.temp.forEach(account => {
                const name = account['account_name'].toLowerCase();
                const _value = value.toLowerCase();
                const number = account['wallet'];

                if (name.indexOf(_value) > -1 || number.indexOf(_value) > -1) {
                    const accountForm = this.formBuilder.group(account);
                    this.accounts.push(accountForm);
                }

            });
        }

        this.temp.forEach(account => {
            const accountForm = this.formBuilder.group(account);
            this.accounts.push(accountForm);
        });

    }





    getStatus () {
        const creditRemaining: number = this.store.selectSnapshot(TransfersState.transferCredits);
        switch (this.getData().account_type) {
            case 'prepaid':
                if (creditRemaining > 0){
                    return true;
                }
                return false;
            case 'pay_as_you_go':
                return true;
            case 'company':
                return true;
            default:
                return false;
        }
    }


    // ADD ONE ROW
    addOne () {
        const status = this.getStatus();
        if (status) {
            return this.generateForms(1);
        }
        return swal(
            {
                html: '<h4>You don\'t have enough prepaid to perform transaction, Please topup your account</h4>',
            }
        )
    }

    onPaymentType (e) {
        const type: 'manual' | 'auto' | 'none' | 'card-account' = e.target.value;
        this.paymentType = type;
    }


    processPayment (e) {
        this.formGroup.patchValue(
            {
                cardDetails: e,
                payment_type: 'card'
            }
        )

        this.confirmTransfer();

    }

    enquire=()=>{
        if (this.accounts.length > 0){
            this.store.dispatch(new QueNameChecks(this.accounts.value))
                      .toPromise()
                      .then(()=>this.router.navigate(['namecheck'],{ relativeTo:this.route }))
        }
    }


    ngOnDestroy(): void {
        try {
            this._csvRecipients.unsubscribe();
            this._transferHistory$.unsubscribe();
            this.amount$.unsubscribe();
            this.store.dispatch(new ClearHistory());
        } catch (err) {

        }
    }


}
