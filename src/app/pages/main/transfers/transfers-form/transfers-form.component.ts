// import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
// import { FormGroup, FormControl, Validators, FormArray, FormBuilder } from '@angular/forms';
// import { Subscription, Observable } from 'rxjs';
// import { Select, Store } from '@ngxs/store';
// import { CheckImageModel } from 'src/app/components/check-image-component/check-image.model';
// import { TransferInterfaceModel } from 'src/app/models/transfers/transfers.model';
// import { AuthState } from 'src/app/states/auth/auth.state';
// import { IUser } from 'src/app/models/users';
// import banks from "../../../../utils/banks";


// @Component({
//     selector: 'app-transfers-form',
//     templateUrl: './transfers-form.component.html',
//     styleUrls: ['./transfers-form.scss']
// })
// export class TransfersFormComponent implements OnInit {


//     @Output() formEvent: EventEmitter<any> = new EventEmitter();
//     @Select(AuthState.user) user$: Observable<IUser>; 

//     recipients: any[] = [];
//     banks: any[]=banks;
//     networks: CheckImageModel[] = [
//         {
//             image: '../../../../../dashboard/assets/img/networks/mtn.jpg',
//             name: 'MTN'
//         },
//         {
//             image: '../../../../../dashboard/assets/img/networks/vodafone.png',
//             name: 'VODAFONE'
//         },
//         {
//             image: '../../../../../dashboard/assets/img/networks/tigo.png',
//             name: 'TIGO'
//         },
//         {
//             image: '../../../../../dashboard/assets/img/networks/Airtel-money.png',
//             name: 'AIRTEL'
//         }
//     ]



//     @Input() formData: TransferInterfaceModel = {
//         amount: null,
//         type: 'push-funds',
//         canceled: null,
//         account_issuer:null,
//         account_number:null
//     }


//     formGroup: FormGroup;
//     private subscription: Subscription;
//     private network: string;
//     isNetworkVodafone: boolean = false;


//     constructor (private store: Store, private formBuilder: FormBuilder) {};


//     ngOnInit(): void {

//         this.formGroup =  this.formBuilder.group(
//             {
//                 amount: new FormControl(this.formData.amount, Validators.required),
//                 account_number: new FormControl(this.formData.account_number),
//                 account_issuer: new FormControl(this.formData.account_issuer),
//                 account_voucher: new FormControl(this.formData.account_voucher),
//                 totalTransferAmount: 0
//             }
//         );


//         this.subscription = this.formGroup.valueChanges.subscribe(() => this.formEvent.emit(this.formGroup));



//     }


//     networkChange (e: string) {
//         let network: string = e.toLowerCase();
//         this.formGroup.patchValue(
//             {
//                 paymentAccount: {
//                     network: network
//                 }
//             }
//         );

//         this.isNetworkVodafone = network === 'vodafone' ? true : false;

//     }



//     get accounts () {
//         return this.formGroup.get('accounts') as FormArray;
//     }



//     clearArray () {
//         while (this.accounts.length !== 0) {
//             this.accounts.removeAt(0);
//         }
//     }



//     onRecipientChange (e) {

//         let count: number = e.target.value;
//         this.clearArray();
//         let account  = {
//             network: new FormControl(null, Validators.required),
//             voucherCode: null,
//             wallet: null,
//             amount: null,
//             description: null,
//             name: null
//         }

//         for (let index = 0; index < count; index++) {
//             const accountForm = this.formBuilder.group(account);
//             this.accounts.push(accountForm);
//         }

//     }


//     getCredit (credit: number) {
//         return new Array(credit);
//     }



//     removeAccount (index: number) {
//         this.accounts.removeAt(index);
//     }


//     calculateTransferAmount (e) {
//         let accounts = this.accounts.value;
//         let amount: number;
//         if (accounts.length > 1) {
//             amount = accounts.reduce((a,b) => parseInt(a['amount'] || 0) + parseInt(b['amount'] || 0));
//         } else {
//             amount = accounts[0]['amount'];
//         }
        
//         this.formGroup.patchValue(
//             {
//                 totalTransferAmount: amount.toString()
//             }
//         )
//     }


//     ngOnDestroy(): void {
//         this.subscription.unsubscribe();
//     }



// }
