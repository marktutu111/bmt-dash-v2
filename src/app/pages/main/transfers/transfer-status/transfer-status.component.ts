import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { AlertState } from 'src/app/states/alerts/alerts.state';
import { Observable, Subscription } from 'rxjs';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { IAlertAction } from 'src/app/states/alerts/alerts.actions';
import { ToggleTransfersEdit, GetTransferTransactions, SelectTransaction } from 'src/app/states/transfer/transfer.actions';
import { TransactionsModel } from 'src/app/models/transactions/transactions.model';

@Component(
    {
        selector: 'app-transfer-status',
        templateUrl: './transfer-status.component.html',
        styleUrls: ['./transfer-status.component.scss']
    }
)
export class TransferStatusComponent implements OnInit {

    @Select(TransfersState.transactions) transactions$: Observable<TransactionsModel[]>;
    @Select(TransfersState.selectedTransaction) transaction$: Observable<TransactionsModel>;
    @Select(AlertState.action) action$: Observable<IAlertAction>;


    onTransaction: boolean = false;

    private subscription: Subscription;


    constructor(private store: Store) {};


    ngOnInit(): void {

        const transferId: string = this.store.selectSnapshot(TransfersState.selected)._id;
        this.store.dispatch(
            new GetTransferTransactions(transferId)
        )

    };


    toggleTransaction (transaction) {
        this.onTransaction = true;
        this.store.dispatch(
            new SelectTransaction(transaction)
        )
    }



    back () {

        if (this.onTransaction) {
            return this.onTransaction = false;
        }

        this.store.dispatch(new ToggleTransfersEdit(false));

    }


    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.onTransaction = false;
    }




}
