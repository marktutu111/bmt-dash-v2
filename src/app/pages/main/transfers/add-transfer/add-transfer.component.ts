import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ToggleSideContent } from 'src/app/states/app/actions';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { NewTransfers } from 'src/app/states/transfer/transfer.actions';
import { AuthState } from 'src/app/states/auth/auth.state';


@Component({
    selector: 'app-add-transfer',
    templateUrl: './add-transfer.component.html',
    styleUrls: ['./add-transfer.component.scss']
})
export class AddTransferComponent implements OnInit {
    
    
    private form: FormGroup;

    constructor(private store: Store) {};

    ngOnInit(): void {};


    onFormChange(form: FormGroup) {
        this.form = form;
    }


    save () {

        const userId: string = this.store.selectSnapshot(AuthState.user)._id;

        if (this.form && this.form.valid) {

            let payload = {
                ...this.form.value,
                type: 'push-funds',
                userId: userId
            }

            this.store.dispatch(
                [
                    new NewTransfers(payload),
                    new ToggleSideContent()
                ]
            );
        }
    }


}
