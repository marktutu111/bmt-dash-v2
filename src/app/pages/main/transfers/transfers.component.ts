import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { ActivatePath } from 'src/app/states/app/actions';

@Component({
    selector: 'app-transfers',
    templateUrl: './transfers.component.html',
    styleUrls: ['./transfers.component.scss']
})
export class TransfersComponent implements OnInit {


    constructor (private store: Store) {};

    ngOnInit(): void {
        this.store.dispatch(
            new ActivatePath('transfers')
        )
    };


    
}
