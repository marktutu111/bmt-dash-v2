import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { Observable, Subscription } from 'rxjs';
import { TransactionsModel } from 'src/app/models/transactions/transactions.model';
import { GetTransferTransactions, GetIssuers, AuthorizePayment } from 'src/app/states/transfer/transfer.actions';
import { Location } from '@angular/common';
import { ExportToCsv } from 'export-to-csv';
import { Router, ActivatedRoute } from '@angular/router';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { AuthState } from 'src/app/states/auth/auth.state';
import { API } from 'src/app/constants/api';

declare const swal:any;

@Component({
    selector: 'app-transaction-list',
    templateUrl: './transaction-list.component.html',
    styleUrls: ['./transaction-list.component.scss'],
})
export class TransactionListComponent implements OnInit {
    


    constructor(
        private store: Store,  
        private location: Location,
        private router: Router,
        private route: ActivatedRoute,
       
       
    ) {};


    transactions: TransactionsModel[] = [];

    @Select(TransfersState.transactions) transactions$: Observable<TransactionsModel[]>;
    @Select(TransfersState.loadingTransactions) loading$: Observable<boolean>;
    @Select(TransfersState.issuers) issuers$:Observable<any[]>;
    @Select(TransfersState.selected) transfer: Observable<TransactionsModel[]>
    // transfer = this.store.selectSnapshot(TransfersState.selected);
    // alert()
    @Select(AuthState.user) user$:Observable<any>;

    page: number = 1;
      searchText;
      totalRec : number;
   

    private _transactions$: Subscription;
    // console.log(transfer);
        public selected_transfer;
    ngOnInit(): void {
        this._transactions$ = this.transactions$.subscribe(transctions => this.transactions = transctions);
        this.selected_transfer = this.store.selectSnapshot(TransfersState.selected);
        // console.log(selected_transfer);
        if (this.selected_transfer && this.selected_transfer['_id']) {
            this.store.dispatch(
                [
                    new GetTransferTransactions(this.selected_transfer._id),
                    new GetIssuers()
                ]
            );
        }

    };


    onSearch (search) {
        const _transactions = this.store.selectSnapshot(TransfersState.transactions);
        const filter = _transactions.filter(tran => {
            try {
                
                const name: string = tran['account_name'].toLowerCase();
                let phone: string = tran['account_number'];
                const phone_stripped = `0${phone.substring(4,phone.length)}`;
                const status: string = tran['status'];
                if (name.indexOf(search) > -1 || phone_stripped.indexOf(search) > -1 || status.indexOf(search) > -1) {
                    return tran;
                }

            } catch (err) {
                
            }
        });

        if (search === '') {
            this.transactions = _transactions;
        } else {
            this.transactions = filter;
        }

    }



    download () {

        const transactions = this.transactions.map(transaction => {
            return {
                Name: transaction['account_name'],
                Wallet: transaction['account_number'],
                'Amount(GHS)': transaction['amount'],
                Network: transaction['account_issuer'],
                Status: transaction['status'],
                Description: transaction['description'],
                Message: transaction.getCreditStatus(),
                Date: new Date(transaction.createdAt).toDateString()
            }
        })

        const options = { 
            fieldSeparator: ',',
            quoteStrings: '"',
            decimalSeparator: '.',
            showLabels: false, 
            showTitle: false,
            title: 'transactions.csv',
            useTextFile: false,
            useBom: true,
            useKeysAsHeaders: true,
            // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
        };
        
        const csvExporter = new ExportToCsv(options);
        csvExporter.generateCsv(transactions);


    }



    onStatusChange (e) {
        const status: string = e.target.value;
        const _transactions = this.store.selectSnapshot(TransfersState.transactions);

        if (status === 'none') {
           return this.transactions = _transactions;
        }

        const filter = _transactions.filter(transaction => transaction['status'] === status);
        this.transactions = filter;
    }




    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this._transactions$.unsubscribe();

    }



    back () {
        this.router.navigate([`0/transfers`]);
    }
    approve=async ()=>{
        const transfer = this.store.selectSnapshot(TransfersState.selected)
        const {
            phone,
            _id
        }=this.store.selectSnapshot(AuthState.user);
        let message=`
        <h3 style="margin: 20px;">
        You have requested to send an amount of Ghs${parseFloat(transfer.total_amount_failed.toString()).toFixed(2)} to ${transfer.tfailed} number of
        recipients, Please confirm your transaction.
    </h3>
        `
        
    
        swal.fire({
          title: 'Confirm Transaction',
          html: message,
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Proceed'
          }
        ).then(async ({ value })=>{
          if (value){
            try {
              const response = await fetch(API.GET_OTP, {
                  method: 'POST',
                  headers:{
                    'Authorization':this.store.selectSnapshot(AuthState.token)
                  },
                  body: JSON.stringify(
                      {
                          phoneNumber: phone
                      }
                  )
              }).then(res => res.json());
              if (response.success) {
                  const { value } = await swal.fire(
                      {
                          html: `<h4>Please Confirm transaction by entering OTP received on your phone with number ${phone}</h4>`,
                          input: 'text',
                          inputPlaceholder: 'code',
                          showCancelButton: true
                      }
                  )
                  if (value) {
                      this.store.dispatch(
                          new AuthorizePayment(
                              {
                                transfer:transfer._id,
                                otp:value,
                                user:_id,
                                control:'resend'
                              }
                          )
                      )
                      this.router.navigate(['../../transfers'],{ relativeTo: this.route })
    
                  } throw Error('Transaction failed');                   
              } throw Error(response.message);
            } catch (err) {
              swal.fire(
                  {
                      html: `<h4>Sorry We could not process your transaction, ${err.message}</h4>`
                  }
              )
            }
          }
        }).catch(err=>null);
      }



}
