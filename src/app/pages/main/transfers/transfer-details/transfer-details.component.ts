import { Component, OnInit, Input } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { IAlertAction } from '../../../../states/alerts/alerts.actions';
import { AlertState } from '../../../../states/alerts/alerts.state';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { DeleteTransfers, ToggleTransfersEdit, GetIssuers } from 'src/app/states/transfer/transfer.actions';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { Router, ActivatedRoute } from '@angular/router';

import * as moment from "moment";


@Component({
    selector: 'app-transfer-details',
    templateUrl: './transfer-details.component.html',
    styleUrls: ['./transfer-details.component.scss']
})
export class TransferDetailsComponent implements OnInit {


    @Select(TransfersState.selected) transfer$: Observable<TransferModel>;
    @Select(TransfersState.issuers) issuers$:Observable<any[]>;
    @Select(AlertState.action) action$: Observable<IAlertAction>;
    private delete$: Subscription;

    constructor(private store: Store, private router: Router, private route: ActivatedRoute) {};


    ngOnInit(): void {
        this.delete$ = this.action$.subscribe(({ type, data }) => {
            try {

                if (data['id'] === 'delete-transfer' && type === 'confirm') {
                    this.store.dispatch(
                        [
                            new DeleteTransfers(data['meta']),
                            new GetIssuers()
                        ]
                    );
                }

            } catch (err) {
                
                
            }
        });

    };


    toggleEdit () {
        const id: string = this.store.selectSnapshot(TransfersState.selected)._id;
        this.router.navigate([id], { relativeTo: this.route });
        // this.store.dispatch(new ToggleTransfersEdit(true));
        // this.store.dispatch(
        //     new ToggleListGrid(true)
        // )
    }


    delete (id: string) {
        // this.store.dispatch(new NewAlert(
        //     {
        //         message: 'Confirm Delete',
        //         toggle: true,
        //         type: 'prompt',
        //         data: {
        //             id: 'delete-transfer',
        //             meta: id
        //         }
        //     })
        // )
    }


    getTime (d) {
        return moment(d).format('HH:mm')
    }



    
    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.delete$.unsubscribe();

    }



}
