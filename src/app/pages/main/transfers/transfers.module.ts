import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransfersComponent } from './transfers.component';
import { TableComponent } from './table/table.component';
import { RouterModule } from '@angular/router';
import { routes } from './transfers.routing';
import { SideBarWideModule } from 'src/app/components/side-content-full/side-content-full.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { TransferDetailsComponent } from './transfer-details/transfer-details.component';
import { AddTransferComponent } from './add-transfer/add-transfer.component';
import { CheckImageModule } from 'src/app/components/check-image-component/check-image.module';
import { TransferStatusComponent } from './transfer-status/transfer-status.component';
import { TransactionListGridModule } from 'src/app/components/transaction-list-grid/transaction-list-grid.module';
import { TransactionListComponent } from './transaction-list/transaction-list.component';
import { SearchModule } from 'src/app/components/search-component/search.module';
import { DateFilterModule } from 'src/app/components/date-filter.component/date-filter.module';
import { Ng2SearchPipeModule } from 'ng2-search-filter';


@NgModule({
    declarations: [
        TransfersComponent,
        TableComponent,
        TransferDetailsComponent,
        AddTransferComponent,
        // TransfersFormComponent,
        TransferStatusComponent,
        TransactionListComponent
    ],
    imports: [
        CommonModule,
        SideBarWideModule,
        ReactiveFormsModule,
        FormsModule,
        NgxPaginationModule,
        CheckImageModule,
        TransactionListGridModule,
        RouterModule.forChild(routes),
        SearchModule,
        DateFilterModule,
        Ng2SearchPipeModule,
    ],
    exports: [
        TransferDetailsComponent,
        TransactionListComponent
    ],
    providers: [],
})
export class TransfersModule {}