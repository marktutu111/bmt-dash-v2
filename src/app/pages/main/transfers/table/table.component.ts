import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { SelectTransfers, GetTransfersByUser, GetTransferTransactions, FilterTransferDate, FilterUserTranfersByDate, Get_User_Transfer_Summary, FilterTransfers } from 'src/app/states/transfer/transfer.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-transfers-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {


      @Select(TransfersState.loading) loading$: Observable<boolean>;
      @Select(TransfersState.transfers) transfers$: Observable<TransferModel[]>;
      @Select(TransfersState.onEdit) onEdit$: Observable<boolean>;
      @Select(TransfersState.selected) selected$: Observable<TransferModel>;
      @Select(TransfersState.summary) summary$: Observable<any>;
      @Select(AuthState.user) user$:Observable<any>;
      

      p: number = 1;
      openTransactions: boolean = false;
      private userId: string;
      // page: number = 1;
      searchText;
      // totalRec : number;


      constructor(private  store: Store, private router: Router, private route: ActivatedRoute) {};
 

      ngOnInit() {
          const {
            _id,
            company_id,
            account_type,
            company_role
          } = this.store.selectSnapshot(AuthState.user);
          this.userId=_id;

          this.store.dispatch(
            [
              new GetTransfersByUser(_id),
              new Get_User_Transfer_Summary(this.userId)
            ]
          );

          if (account_type==='company' && company_role==='authorizer'){
            this.store.dispatch(
              new FilterTransfers(
                {
                  filter:{
                    company_id:company_id
                  }
                }
              ),
            )
          }

      }


      onSelected (transfer) {
        this.store.dispatch(new SelectTransfers(transfer));
      }


      onDateChange (v) {
        this.store.dispatch(
          new FilterUserTranfersByDate(
            {
              id: this.userId,
              data: v
            }
          )
        )
      }

      
}
