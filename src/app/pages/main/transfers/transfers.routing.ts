import { Routes } from "@angular/router";
import { TransfersComponent } from "./transfers.component";
import { TransactionListComponent } from "./transaction-list/transaction-list.component";



export const routes: Routes = [
    {
        path: '',
        component: TransfersComponent,
    },
    {
        path: ':id',
        component: TransactionListComponent
    }
]