import { Routes } from "@angular/router";
import { MainComponent } from "./main.component";

// import {} from './users/users.module'


export const mainRoutes: Routes = [
    {
        
        path: '',
        component: MainComponent,
        children: [
            {
                path: '',
                redirectTo: 'send-money',
                pathMatch: 'full'
            },
            {
                path: 'subscriptions',
                loadChildren: './subscriptions/subscriptions.module#SubscriptionsModule'
            },
            {
                path: 'transfers',
                loadChildren: './transfers/transfers.module#TransfersModule'
            },
            {
                path: 'buy-credit',
                loadChildren: './buy-credit/buy-credit.module#BuyCreditModule'
            },
            {
                path: 'send-money',
                loadChildren: './send-money/send-money.module#SendMoneyModule'
            },
            {
                path: 'wallets',
                loadChildren: './my-wallets/my-wallets.module#WalletsModule'
            },
            {
                path: 'favourites',
                loadChildren: './manage-fav/manage-fav.module#ManageFavModule'

            },
            {
                path: 'roles',
                loadChildren: './roles/roles.module#RolesModule'
            },
            {
                path: 'messages',
                loadChildren: './messages/messages.module#MessagesModule'
            },
            {
                path:'payments',
                loadChildren:'./payment/payment.module#PaymentModule'
            },
            {
                path:'users',
                loadChildren:'./users/users.module#UsersModule'
            },
            {
                path:'invoice',
                loadChildren:'./invoice/invoice.module#InvoiceModule'
            }

        ]
    },
]