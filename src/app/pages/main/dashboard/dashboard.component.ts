import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { DashboardState } from 'src/app/states/dahsboard/dashboard.state';
import { Observable } from 'rxjs';
import { DashboardInterfaceModel } from 'src/app/models/dashboard';
import { GetDashboard } from 'src/app/states/dahsboard/dashboard.actions';
import { AuthState } from 'src/app/states/auth/auth.state';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @Select(DashboardState.transfers) transfers$: Observable<DashboardInterfaceModel>;
  @Select(DashboardState.subscriptions) subscriptions$: Observable<DashboardInterfaceModel>;
  @Select(DashboardState.loading) loading$: Observable<boolean>;

  constructor(private store: Store) {}

  
  ngOnInit() {

    const user: string = this.store.selectSnapshot(AuthState.user)._id;
    this.store.dispatch(
      new GetDashboard(user)
    );

  }
  

}
