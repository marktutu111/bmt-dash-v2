
import { payroutes } from './payment.routing';
import { PaymentComponent } from './payment.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AngularPaginatorModule } from 'angular-paginator';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
    declarations: [
        PaymentComponent
    ],
    imports: [ 
        CommonModule,
        RouterModule.forChild(payroutes),
        // DemoMaterialModule
     ],
    exports: [],
    providers: [],
})
export class PaymentModule {}