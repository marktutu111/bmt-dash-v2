import { Component, OnInit, ViewChild } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { FilterTransfers, AuthorizePayment } from 'src/app/states/transfer/transfer.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { Observable } from 'rxjs';
import { API } from 'src/app/constants/api';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import * as moment from 'moment';
import { Route, Router, ActivatedRoute } from '@angular/router';



declare const swal:any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  @Select(TransfersState.pendingTransfers) transfers$:Observable<any[]>;
  @Select(TransfersState.loading) loading$: Observable<boolean>;
  constructor(private store:Store, private router:Router,private route: ActivatedRoute ) {};


  

  ngOnInit() {
    const {company_id,account_type}=this.store.selectSnapshot(AuthState.user);
    this.store.dispatch(
      new FilterTransfers(
        {
          filter:{
            account_type:account_type,
            control_approval_status:'pending',
            company_id:company_id
          }
        }
      )
    )
  };
  // onCancel=async()=>{

  // }
  getTime (d) {
    return moment(d).format('HH:mm')
}
// Redirect(){
//   this.router.navigate(['../../transfers'],{ relativeTo: this.route })
// }
  approve=async (transfer:TransferModel,control='approved')=>{
    const {
        phone,
        _id
    }=this.store.selectSnapshot(AuthState.user);
    let message=``
    switch (control) {
      case 'approved':
        message=`
          <h3 style="margin: 20px;">
              You have requested to send an amount of Ghs${parseFloat(transfer.amount).toFixed(2)} to ${transfer.total_transfer_count} number of
              recipients, Please confirm your transaction.
          </h3>
        `
        break;
      case 'resend':
        message=`
          <h3 style="margin: 20px;">
              You have requested to send an amount of Ghs${parseFloat(transfer.total_amount_failed.toString()).toFixed(2)} to ${transfer.tfailed} number of
              recipients, Please confirm your transaction.
          </h3>
        `
        break;
      default:
        message=`
          <h3 style="margin: 20px;">
              You have requested to cancel your transaction, Please confirm your request.
          </h3>
        `
        break;
    }

    swal.fire({
      title: 'Confirm Transaction',
      html: message,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed'
      }
    ).then(async ({ value })=>{
      if (value){
        try {
          const response = await fetch(API.GET_OTP, {
              method: 'POST',
              headers:{
                'Authorization':this.store.selectSnapshot(AuthState.token)
              },
              body: JSON.stringify(
                  {
                      phoneNumber: phone
                  }
              )
          }).then(res => res.json());
          if (response.success) {
              const { value } = await swal.fire(
                  {
                      html: `<h4>Please Confirm transaction by entering OTP received on your phone with number ${phone}</h4>`,
                      input: 'text',
                      inputPlaceholder: 'code',
                      showCancelButton: true
                  }
              )
              if (value) {
                  this.store.dispatch(
                      new AuthorizePayment(
                          {
                            transfer:transfer._id,
                            otp:value,
                            user:_id,
                            control:control
                          }
                      )
                  )
                  this.router.navigate(['../../transfers'],{ relativeTo: this.route })

              } throw Error('Transaction failed');                   
          } throw Error(response.message);
        } catch (err) {
          swal.fire(
              {
                  html: `<h4>Sorry We could not process your transaction, ${err.message}</h4>`
              }
          )
        }
      }
    }).catch(err=>null);
  }




}
