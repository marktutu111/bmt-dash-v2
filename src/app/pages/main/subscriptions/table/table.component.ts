import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ToggleSideContent } from '../../../../states/app/actions';
import { SubscriptionState } from 'src/app/states/subscriptions/subscriptions.state';
import { SubscriptionModel } from 'src/app/models/subscriptions/subscriptions.model';
import { SelectSubscriptions, ToggleSubscriptionsEdit, GetSubscriptionByUser, FilterDate } from 'src/app/states/subscriptions/subscriptions.actions';
import { AuthState } from 'src/app/states/auth/auth.state';



@Component({
  selector: 'app-subscriptions-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {


      @Select(SubscriptionState.loading) loading$: Observable<boolean>;
      @Select(SubscriptionState.subscriptions) subscriptions$: Observable<SubscriptionModel[]>;
      @Select(SubscriptionState.onEdit) onEdit$: Observable<boolean>;
      @Select(SubscriptionState.selected) selected$: Observable<SubscriptionModel>;

      p: number = 1;


      constructor(private  store: Store) {}


      ngOnInit() {
          let userId: string = this.store.selectSnapshot(AuthState.user)._id;
          this.store.dispatch(
            new GetSubscriptionByUser(userId)
          );
      }



      onSelected (subscription) {
        this.store.dispatch(
          [
            new SelectSubscriptions(subscription),
            new ToggleSubscriptionsEdit(false)
          ]
        );
      }


      newSubscription () {
        this.store.dispatch(
          new ToggleSideContent()
        );
      }


      
      onDateChange (v) {
        this.store.dispatch(
          new FilterDate(v)
        )
      }



}
