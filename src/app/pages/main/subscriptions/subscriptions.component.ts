import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { ActivatePath } from 'src/app/states/app/actions';



@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {

      constructor(private store: Store) {};


      ngOnInit() {
        this.store.dispatch(
          new ActivatePath('subscriptions')
        )
      };
     



}
