import { Component, OnInit, Input } from '@angular/core';
import { IUser } from '../../../../models/users';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { NewAlert, IAlertAction } from '../../../../states/alerts/alerts.actions';
import { AlertState } from '../../../../states/alerts/alerts.state';
import { SubscriptionState } from 'src/app/states/subscriptions/subscriptions.state';
import { SubscriptionModel } from 'src/app/models/subscriptions/subscriptions.model';
import { DeleteSubscriptions, ToggleSubscriptionsEdit } from 'src/app/states/subscriptions/subscriptions.actions';


@Component({
    selector: 'app-subscriptions-details',
    templateUrl: './subscriptions-details.component.html',
    styleUrls: ['./subscriptions-details.component.scss']
})
export class SubscriptionsDetailsComponent implements OnInit {


    @Select(SubscriptionState.selected) subscription$: Observable<SubscriptionModel>;
    @Select(AlertState.action) action$: Observable<IAlertAction>;
    private delete$: Subscription;

    constructor(private store: Store) {};


    ngOnInit(): void {
        this.delete$ = this.action$.subscribe(({ type, data }) => {
            try {
                if (data['id'] === 'delete-subscription' && type === 'confirm') {
                    this.store.dispatch(
                        new DeleteSubscriptions(data['meta'])
                    );
                }
            } catch (err) {
                
            }
        });


    };


    toggleEdit () {
        this.store.dispatch(new ToggleSubscriptionsEdit(true));
    }


    delete (id: string) {
        this.store.dispatch(new NewAlert(
            {
                message: 'Confirm Delete',
                toggle: true,
                type: 'prompt',
                data: {
                    id: 'delete-subscriptions',
                    meta: id
                }
            })
        )
    }


    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.delete$.unsubscribe();

    }

}
