import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { ToggleSideContent } from 'src/app/states/app/actions';
import { SubscriptionState } from 'src/app/states/subscriptions/subscriptions.state';
import { NewSubscriptions } from 'src/app/states/subscriptions/subscriptions.actions';
import { AuthState } from 'src/app/states/auth/auth.state';


@Component({
    selector: 'app-add-subscriptions',
    templateUrl: './add-subscriptions.component.html',
    styleUrls: ['./add-subscriptions.component.scss']
})
export class AddSubscriptionsComponent implements OnInit {

    @Select(SubscriptionState.loading) loading$: Observable<boolean>;
    private form: FormGroup;
    constructor(private store: Store) {};

    ngOnInit(): void {}

    onFormChange(form: FormGroup) {
        this.form = form;
    }

    save () {
        const userId: string = this.store.selectSnapshot(AuthState.user)._id;
        if (this.form.valid) {
            this.store.dispatch(
                [
                    new NewSubscriptions(
                        {
                            ...this.form.value,
                            userId: userId,
                        }
                    ),
                    new ToggleSideContent()
                ]
            );
        }
    }


}
