import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { SubscriptionsInterfaceModel } from 'src/app/models/subscriptions/subscriptions.model';
import { CheckImageModel } from 'src/app/components/check-image-component/check-image.model';
import { PackageState } from 'src/app/states/packages/packages.state';
import { PackageModel } from 'src/app/models/packages/packages.model';
import { GetPackages } from 'src/app/states/packages/packages.actions';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { GetIssuers } from 'src/app/states/transfer/transfer.actions';



@Component({
    selector: 'app-subscriptions-form',
    templateUrl: './subscriptions-form.component.html',
    styleUrls: ['./subscriptions-form.scss']
})
export class SubscriptionsFormComponent implements OnInit {


    @Select(PackageState.packages) packages$: Observable<PackageModel[]>;
    @Output() formEvent: EventEmitter<any> = new EventEmitter();
    @Select(TransfersState.issuers) issuers$:Observable<any[]>;

    packages: PackageModel[] = [];
    credit: string = '0';
    amount: string = '0';


    @Input() formData: SubscriptionsInterfaceModel = {
        amount: null,
        packageId: null,
        account_issuer:null,
        account_number:null
    }


    formGroup: FormGroup;

    private subscription: Subscription;
    private network: string;
    private packageSub$: Subscription;
    isNetworkVodafone: boolean = false;

    constructor (private store: Store, private formBuilder: FormBuilder) {};


    ngOnInit(): void {

        this.formGroup = this.formBuilder.group(
            {
                amount: new FormControl(this.formData.amount, Validators.required),
                packageId: new FormControl(this.formData.packageId, Validators.required),
                credit: null,
                account_issuer:new FormControl(this.formData.account_issuer,Validators.required),
                account_number:new FormControl(this.formData.account_number,Validators.required)
            }
        )

        this.subscription = this.formGroup.valueChanges.subscribe(() => this.formEvent.emit(this.formGroup));
        this.packageSub$ = this.packages$.subscribe(packs => this.packages = packs);
        this.store.dispatch(
            [
                new GetPackages(),
                new GetIssuers()
            ]
        );

    }


    networkChange (e: string) {
        let network: string = e.toLowerCase();
        this.formGroup.patchValue(
            {
                payment: {
                    network: network
                }
            }
        );

        this.formEvent.emit(this.formGroup);
        this.isNetworkVodafone = network === 'vodafone' ? true : false;


    }


    onValue (e) {

        let packId: string = e.target.value;
        let pack = this.packages.find(pack => pack['_id'] === packId);
        this.credit = pack['limit'];
        this.amount = pack['amount'];
        this.formGroup.patchValue(
            { 
                amount: pack['amount'],
                credit: this.credit
            }
        );

        this.formEvent.emit(this.formGroup);

    }




    ngOnDestroy(): void {
        this.subscription.unsubscribe();
        this.packageSub$.unsubscribe();
    }



}
