import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table/table.component';
import { RouterModule } from '@angular/router';
import { routes } from './subscriptions.routing';
import { SideBarWideModule } from '../../../components/side-content-full/side-content-full.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { SubscriptionsComponent } from './subscriptions.component';
import { SubscriptionsFormComponent } from './subscriptions-form/subscriptions-form.component';
import { SubscriptionsDetailsComponent } from './subscriptions-details/subscriptions-details.component';
import { AddSubscriptionsComponent } from './add-subscriptions/add-subscriptions.component';
import { CheckImageModule } from 'src/app/components/check-image-component/check-image.module';
import { SearchModule } from 'src/app/components/search-component/search.module';
import { DateFilterModule } from 'src/app/components/date-filter.component/date-filter.module';


@NgModule({
    declarations: [
        SubscriptionsComponent,
        TableComponent,
        SubscriptionsFormComponent,
        AddSubscriptionsComponent,
        SubscriptionsDetailsComponent
    ],
    imports: [ 
        CommonModule,
        SideBarWideModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        CheckImageModule,
        RouterModule.forChild(routes),
        DateFilterModule
    ],
    exports: [],
    providers: [],
})
export class SubscriptionsModule {}