import { Component, OnInit } from '@angular/core';
import { CheckImageModel } from 'src/app/components/check-image-component/check-image.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { Store, Select } from '@ngxs/store';
import { AuthState } from 'src/app/states/auth/auth.state';
import { NewSubscriptions } from 'src/app/states/subscriptions/subscriptions.actions';
import { GetAccounts, NewPaymentAccount } from 'src/app/states/payment-accounts/actions';
import { PaymentAccountState } from 'src/app/states/payment-accounts/state';
import { SubscriptionState } from 'src/app/states/subscriptions/subscriptions.state';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { GetIssuers } from 'src/app/states/transfer/transfer.actions';

declare const swal: any;

@Component({
    selector: 'app-buy-credit',
    templateUrl: './buy-credit.component.html',
    styleUrls: ['./buy-credit.component.scss']
})
export class BuyCreditComponent implements OnInit {

    @Select(PaymentAccountState.accounts) accounts$: Observable<any[]>;
    @Select(SubscriptionState.loading) loading$: Observable<boolean>;
    @Select(TransfersState.issuers) issuers$:Observable<any[]>;

    private PERCENTAGE: number = 10;
    // prepaid: number =0;
    formGroup: FormGroup;
    submitted=false;

    private subscription: Subscription;
    private _amount$: Subscription;
    private network: string;
    private network$: Subscription;

    isNetworkVodafone: boolean = false;
    paymentType: 'manual' | 'auto' | 'none' | 'card-account' = 'none';

    constructor (private store: Store, private formBuilder: FormBuilder) {};


    ngOnInit(): void {

        const userId: string = this.store.selectSnapshot(AuthState.user)._id;
        this.formGroup = this.formBuilder.group(
            {
                credit: 0,
                amount: new FormControl(null, [Validators.required,Validators.pattern('^[0-9]*$')]),
                account_issuer:new FormControl(null,Validators.required),
                account_number:new FormControl(null,Validators.required)
            }
        );

        // DETECT AMOUNT CHANGE
        // const  credit = parseInt(v) * this.PERCENTAGE;
        this._amount$ = this.formGroup.controls['amount'].valueChanges.subscribe((v: string) => {
            if (v && v !== '' && v !== '0') {
                return this.formGroup.patchValue(
                    {
                        credit: Math.round(parseInt(v) * 100) / 100
                    }
                )
            }
            this.formGroup.patchValue(
                {
                    credit: 0
                }
            )
        });

        // // DETECT NETWORK CHANGE AND TOGGLE VOUCHER CODE INPUT
        // this.network$ = this.formGroup.controls['payment'].valueChanges.subscribe(
        //     ({ network }) => {

        //         if (network === 'vodafone') {
        //             return this.isNetworkVodafone = true
        //         }

        //         this.isNetworkVodafone = false;

        //     }
        // )

        this.store.dispatch(
            [
                new GetAccounts(userId),
                new GetIssuers()
            ]
        )


    }

    get f() { return this.formGroup.controls; }
    networkChange (e: string) {
        let network: string = e.toLowerCase();
        this.formGroup.patchValue(
            {
                payment: {
                    network: network
                }
            }
        );

        this.isNetworkVodafone = network === 'vodafone' ? true : false;

    }


    async topup () {
        this.submitted=true;

        if (this.formGroup.valid) {
            const userId: string = this.store.selectSnapshot(AuthState.user)._id;
            if (this.paymentType === 'manual') {

                let { value } = await swal.fire(
                    {
                        html: '<h4>Would you like to save sender\'s account details?</h4>',
                        showCancelButton: true,
                        cancelButtonColor: 'red'
                    }
                );

                if (value) {

                    const {
                        account_number,
                        account_issuer
                    } = this.formGroup.value;

                    const issuer=this.store.selectSnapshot(TransfersState.issuers).find(issuer=>issuer.id===account_issuer);
                    if (!issuer || !issuer._id) return;

                    const { value } = await swal.fire(
                        {
                            html: '<h4>Please name account</h4>',
                            input: 'text',
                            inputPlaceholder: 'name',
                            showCancelButton: true
                        }
                    )

                    if (value) {

                        const data = {
                            account_issuer:account_issuer,
                            account_number:account_number,
                            userId: userId,
                            type: issuer.type,
                            name: value
                        }
    
                        this.store.dispatch(
                            new NewPaymentAccount(data, true)
                        )

                    } else {
                        return swal.fire(
                            {
                                html: '<h4>Invalid name, please provide a valid name</h4>'
                            }
                        )
                    }
                }
            }

            this.store.dispatch(
                new NewSubscriptions(
                    {
                        ...this.formGroup.value,
                        userId: userId,
                    }
                )
            );

        }

    }



    onPaymentAccount (e) {
        const _id = e.target.value;
        if (typeof _id === 'string' && _id !== '' && _id !== 'none') {
            const account = this.store.selectSnapshot(PaymentAccountState.accounts).find(account => account['_id'] === _id);
            if (!account || !account._id)return;
            this.formGroup.patchValue(
                {
                    account_issuer:account.account_issuer,
                    account_number:account.account_issuer
                }
            )
        }
    }


    onPaymentType (e) {
        const type: 'manual' | 'auto' | 'none' = e.target.value;
        this.paymentType = type;
    }



    ngOnDestroy(): void {
        try {
            this.subscription.unsubscribe();
            this._amount$.unsubscribe();
            // this.network$.unsubscribe();
        } catch (err) {
            
        }
    }





}
