import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BuyCreditComponent } from './buy-credit.component';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { CheckImageModule } from 'src/app/components/check-image-component/check-image.module';
import { TheTellerComponentModule } from 'src/app/components/theteller/theteller.component.module';


const route: Routes = [
    {
        path: '',
        component: BuyCreditComponent
    }
]


@NgModule({
    declarations: [
        BuyCreditComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        CheckImageModule,
        RouterModule.forChild(route),
        TheTellerComponentModule
    ],
    exports: [],
    providers: [],
})
export class BuyCreditModule {};