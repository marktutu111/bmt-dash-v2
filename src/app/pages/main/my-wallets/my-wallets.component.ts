import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { PaymentAccountState } from 'src/app/states/payment-accounts/state';
import { Observable } from 'rxjs';
import { PaymentAccountInterfaceModel } from 'src/app/models/payment-accounts/payment-accounts.model';
import { GetAccounts, DeleteAccount, NewPaymentAccount } from 'src/app/states/payment-accounts/actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { GetIssuers } from 'src/app/states/transfer/transfer.actions';
import { TransfersState } from 'src/app/states/transfer/transfer.state';


declare const swal: any;

@Component({
    selector: 'app-my-wallets',
    templateUrl: './my-wallets.component.html',
    styleUrls: ['./my-wallets.component.scss']
})
export class MyWalletsComponent implements OnInit {

    @Select(PaymentAccountState.loading) loading$: Observable<boolean>;
    @Select(PaymentAccountState.accounts) accounts$: Observable<PaymentAccountInterfaceModel[]>;
    constructor(private store: Store) {};

    ngOnInit(): void {
        const _id: string = this.store.selectSnapshot(AuthState.user)._id;
        this.store.dispatch(
            [
                new GetAccounts(_id),
                new GetIssuers()
            ]
        )
    };


    onDelete (id: string) {
        swal.fire(
            {
                html: '<h4>Do you want to delete account?</h4>',
                showCancelButton: true,
                cancelButtonColor: 'rgb(218, 94, 94)'
            }
        ).then(({ value }) => {
            if (value) {
                return this.store.dispatch(
                    new DeleteAccount(id)
                )
            }

        })
    }


    addAccount () {
        // PROMPT USER TO ENTER WALLET NUMBER
        swal.fire(
            {
                html: '<h4>Enter Account Number</h4>',
                input: 'tel',
                inputPlaceholder: '',
                showCancelButton: true,
                cancelButtonColor: 'rgb(218, 94, 94)'
            }
        ).then(({ value }) => {
            const wallet = value;
            if (typeof value === 'string' && value !== '' && value.length >= 10) {
                let options={};
                const issuers=this.store.selectSnapshot(TransfersState.issuers);
                issuers.forEach(issuer=>{ options[issuer.id]=issuer.name });
                return swal.fire(
                    {
                        html: '<h4 style="font-size:12px">Select Account Issuer</h4>',
                        styles:[ 'inputOptions{font-size:12px}' ] ,
                        input: 'select',
                        inputOptions: options,
                        inputPlaceholder: 'Select your issuer',
                        showCancelButton: true
                    }
                ).then(({ value }) => {
                    if (typeof value === 'string' && value !== '') {
                        const issuer=issuers.find(i=>i.id===value);
                        if (!issuer || !issuer._id)return;
                        const userId: string = this.store.selectSnapshot(AuthState.user)._id;
                        // PROMPT USER TO NAME ACCOUNT TYPE
                        return swal.fire(
                            {
                                html: '<h4>Please name account</h4>',
                                input: 'text',
                                inputPlaceholder: 'name',
                                showCancelButton: true
                            }
                        ).then(({ value }) => {
                            const name = value;
                            if (typeof name === 'string' && name !== '' && name !== undefined) {
                                const data = {
                                    account_number:wallet,
                                    account_issuer:issuer.id,
                                    userId: userId,
                                    type:issuer.type || 'bank',
                                    name: name
                                }
                                return this.store.dispatch(
                                    new NewPaymentAccount(data)
                                )
                            }
                        })
                    }
                })
            }

            swal.fire(
                {
                    html: '<h4>Invalid number</h4>'
                }
            )
        })


    }


}
