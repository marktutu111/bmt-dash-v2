import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MyWalletsComponent } from './my-wallets.component';
import { MomoWalletModule } from 'src/app/components/momo-wallet-component/momo-wallet.module';



const routing: Routes = [
    {
        path: '',
        component: MyWalletsComponent
    }
]

@NgModule({
    declarations: [
        MyWalletsComponent
    ],
    imports: [ 
        CommonModule,
        MomoWalletModule,
        RouterModule.forChild(routing)
    ],
    exports: [],
    providers: [],
})
export class WalletsModule {


}