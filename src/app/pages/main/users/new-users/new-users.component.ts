import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { AddUser } from 'src/app/states/users/users.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { Router } from '@angular/router';
import { AlertState } from 'src/app/states/alerts/alerts.state';
import { Observable, Subscription } from 'rxjs';
import { LAlert } from 'src/app/components/lazy-alert/model';
declare const swal:any;

@Component({
  selector: 'app-new-users',
  templateUrl: './new-users.component.html',
  styleUrls: ['./new-users.component.css']
})
export class NewUsersComponent implements OnInit {

  @Select(AlertState.alert) alert$:Observable<LAlert>;
  formGroup:FormGroup;

  private subscription:Subscription;

  constructor(private store:Store, private route:Router, private _location: Location) {};

  ngOnInit() {
    this.formGroup=new FormGroup(
      {
        fullname:new FormControl(null,Validators.required),
        email:new FormControl(null,Validators.required),
        phone:new FormControl(null,Validators.required),
        password:new FormControl(null,Validators.required),
        company_role: new FormControl('payer',Validators.required)
      }
    )

    this.subscription=this.alert$.subscribe(alert=>{
      if (alert.success){
        this._location.back();
      }
    })

  }

  clearForm(){
    this.formGroup.reset();
    this._location.back();
  }


  save=()=>{
    if (this.formGroup.valid){
      const id:string = this.store.selectSnapshot(AuthState.user).company_id;
      const logo:string=this.store.selectSnapshot(AuthState.user).company_logo;
      const name:string=this.store.selectSnapshot(AuthState.user).company_name;
      this.store.dispatch(
        new AddUser(
          {
            ...this.formGroup.value,
            account_type:'company',
            company_id:id,
            company_logo:logo,
            company_name:name
          }
        )
      );
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }
  

}
