import { DateFilterModule } from './../../../components/date-filter.component/date-filter.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { usersroutes } from './users.routing';
import { NewUsersComponent } from './new-users/new-users.component';
import {NgxPaginationModule} from 'ngx-pagination';
// import { UserlistComponent } from './userlist/userlist.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
    declarations: [
        UsersComponent,
        NewUsersComponent,
        // UserlistComponent
       
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        Ng2SearchPipeModule,
        NgxPaginationModule,
        RouterModule.forChild(usersroutes),
        DateFilterModule
    ],
    exports: [],
    providers: [],
})
export class UsersModule {}