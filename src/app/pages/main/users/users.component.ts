import { FilterUsersByDate } from './../../../states/users/users.actions';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { UsersState } from 'src/app/states/users/users.state';
import { Observable } from 'rxjs';
import { UserModel } from 'src/app/models/users';
import { FilterUsers } from 'src/app/states/users/users.actions';
import { AuthState } from 'src/app/states/auth/auth.state';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  @Select(UsersState.users) users$:Observable<UserModel[]>;
  @Select(UsersState.loading) loading$: Observable<boolean>;

  constructor(private store:Store) {};
  private userId: string;
  page: number = 1;
  totalRec : number;
  searchText;

  edit(id:number){
    this
    // this.store.dispatch(new onFavouriteUpdate(true));
  }

  ngOnInit() {
    this.userId = this.store.selectSnapshot(AuthState.user)._id;
    const { company_id } = this.store.selectSnapshot(AuthState.user);
    this.store.dispatch(
      new FilterUsers(
        {
          company_id:company_id,
          // company_role:'payer',
          account_type:'company',

        }
      )
    )
    
  }
  onDateChange (v) {
    this.store.dispatch(
      new FilterUsersByDate(
        {
          id: this.userId,
          data: v
        }
      )
    )
  }

}
