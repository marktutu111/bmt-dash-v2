import { NewUsersComponent } from './new-users/new-users.component';
import { UsersComponent } from './users.component';
// import { UserAccountsComponent } from './../../bsystems-dashboard/user-accounts/user-accounts.component';
import { Routes, RouterModule } from '@angular/router';


export const usersroutes: Routes = [
    { 
        path: '', 
        component: UsersComponent
    },
    {
        path:'new',
        component:NewUsersComponent
    }
   

    
];


