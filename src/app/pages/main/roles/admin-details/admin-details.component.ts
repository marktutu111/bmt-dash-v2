import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { IAdmin } from 'src/app/models/auth';
import { ToggleUserEdit, DeleteRole } from 'src/app/states/users/users.actions';
import { UsersState } from 'src/app/states/users/users.state';


declare const swal: any;

@Component({
    selector: 'app-admin-details',
    templateUrl: './admin-details.component.html',
    styleUrls: ['./admin-details.component.scss']
})
export class AdminDetailsComponent implements OnInit {


    @Select(UsersState.selected) user$: Observable<IAdmin>;


    constructor (private store: Store) {}

    ngOnInit(): void {

    }


    toggleEdit () {
        this.store.dispatch(new ToggleUserEdit());
    }


    async deleteUser (id: string) {

        const { value } = await swal.fire(
            {
                html: '<h4>Delete account?</h4>',
                showCancelButton: true,
                cancelButtonColor: 'red'
            }
        );

        if (value) {
            this.store.dispatch(
                new DeleteRole(id)
            )
        }

    }

}


