import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RolesComponent } from './roles.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddAdminComponent } from './add-admin/add-admin.component';
import { AdminDetailsComponent } from './admin-details/admin-details.component';
import { EditAdminComponent } from './edit-admin/edit-admin.component';
import { RoleTableComponent } from './table/table.component';
import { RoleUserFormComponent } from './admin-user-form/admin-user-form.component';
import { SideBarWideModule } from 'src/app/components/side-content-full/side-content-full.module';
import { NgxPaginationModule } from 'ngx-pagination';




const route: Routes = [
    {
        path: '',
        component: RolesComponent
    }
]



@NgModule({
    declarations: [
        RolesComponent,
        AddAdminComponent,
        AdminDetailsComponent,
        EditAdminComponent,
        RoleTableComponent,
        RoleUserFormComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(route),
        SideBarWideModule,
        NgxPaginationModule
    ],
    exports: [],
    providers: [],
})
export class RolesModule {}