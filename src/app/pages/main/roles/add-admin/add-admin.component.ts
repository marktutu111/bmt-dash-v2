import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AddAdmin } from 'src/app/states/admin/actions';
import { ToggleSideContent } from 'src/app/states/app/actions';
import { AddRole, NewSignup } from 'src/app/states/users/users.actions';
import { AuthState } from 'src/app/states/auth/auth.state';

@Component({
    selector: 'app-add-admin',
    templateUrl: './add-admin.component.html',
    styleUrls: ['./add-admin.component.scss']
})
export class AddAdminComponent implements OnInit {

    form: FormGroup;

    constructor (private store: Store) {};


    ngOnInit(): void {

    };

    onFormData (form: FormGroup) {
        this.form = form;
    }


    save () {
        try {
            
            if (this.form && this.form.valid) {
    
                const {
                    country,
                    company_id
                } = this.store.selectSnapshot(AuthState.user);

                if (!company_id && !country) {
                    return;
                }
        
                this.form.patchValue(
                    {
                        country: country,
                        corporate_id: company_id
                    }
                )

                this.store.dispatch([
                    new NewSignup(this.form.value),
                    new ToggleSideContent()
                ]);
            }

        } catch (err) {
            
        }


    }


}
