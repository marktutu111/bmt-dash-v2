import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { UsersState } from 'src/app/states/users/users.state';

@Component({
    selector: 'app-roles',
    templateUrl: './roles.component.html',
    styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {


    @Select(UsersState.onEdit) onEdit$: Observable<boolean>;

    constructor() {};


    ngOnInit(): void {};


    

}


