import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ToggleSideContent } from '../../../../states/app/actions';
import { GetAdmins, SelectAdmin } from '../../../../states/admin/actions';
import { Observable } from 'rxjs';
import { IAdmin } from 'src/app/models/auth';
import { UsersState } from 'src/app/states/users/users.state';
import { AuthState } from 'src/app/states/auth/auth.state';
import { GetRoles, SelectUser } from 'src/app/states/users/users.actions';


@Component({
  selector: 'app-admin-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class RoleTableComponent implements OnInit {

    @Select(UsersState.roles) roles$: Observable<IAdmin[]>;
    @Select(UsersState.selected) selected$: Observable<IAdmin>;
    @Select(UsersState.loading) loading$: Observable<boolean>;

    p: number = 1;


    constructor(private  store: Store) {};


    ngOnInit() {

      const {
        company_id,
        _id
      } = this.store.selectSnapshot(AuthState.user);

      this.store.dispatch(
        new GetRoles(_id,company_id)
      );

    }


    onSelected (user) {
      this.store.dispatch(new SelectUser(user));
    };



    addUser () {
      this.store.dispatch(new ToggleSideContent());
    }
      

}
