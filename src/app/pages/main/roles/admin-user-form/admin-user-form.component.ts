import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IAdmin } from 'src/app/models/auth';


@Component({
    selector: 'app-admin-user-form',
    templateUrl: './admin-user-form.component.html',
    styleUrls: ['./admin-user-form.component.scss']
})
export class RoleUserFormComponent implements OnInit, OnDestroy {


    @Output() formEvent: EventEmitter<FormGroup> = new EventEmitter();
    @Input() formData: IAdmin = {
        password: null,
        role: null,
        fullname: null,
        email: null,
        blocked: false
    }
    
    formGroup: FormGroup;
    private subsription: Subscription;


    constructor () {}

    ngOnInit(): void {
        this.formGroup = new FormGroup(
            {
                fullname: new FormControl(this.formData['fullname'], Validators.required),
                email: new FormControl(this.formData['email'], Validators.required),
                corporate_account_type: new FormControl(this.formData['corporate_account_type'], Validators.required),
                password: new FormControl(this.formData['password'], Validators.required)
            }
        );

        this.subsription = this.formGroup.valueChanges.subscribe(() => this.formEvent.emit(this.formGroup));

    };

    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subsription.unsubscribe();

    }

}
