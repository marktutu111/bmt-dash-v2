import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { IAdmin } from 'src/app/models/auth';
import { NewAlert } from 'src/app/states/alerts/alerts.actions';
import { UsersState } from 'src/app/states/users/users.state';
import { ToggleUserEdit } from 'src/app/states/users/users.actions';


declare const swal: any;


@Component({
    selector: 'app-edit-admin',
    templateUrl: './edit-admin.component.html',
    styleUrls: ['./edit-admin.component.scss']
})
export class EditAdminComponent implements OnInit {

    @Select(UsersState.selected) selected$: Observable<IAdmin>;
    private alertAction$: Subscription;

    form: FormGroup;

    constructor (private store: Store) {};



    ngOnInit(): void {};



    onFormData (form: FormGroup) {
        this.form = form;
    }


    back () {
        this.store.dispatch(new ToggleUserEdit(false));
    }



}
