import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule } from '@angular/router';
import { mainRoutes } from './main.routing';
import { MainComponent } from './main.component';
import { LazyAlertModule } from '../../components/lazy-alert/lazy-alert.module';
import { SearchModule } from 'src/app/components/search-component/search.module';
import { InternetConnectionModule } from 'src/app/components/internet-connection/internet-connection.module';
import { FormsModule } from '@angular/forms';
import { SidebarModule } from 'src/app/components/sidebar/sidebar.module';
import { NavbarModule } from 'src/app/components/navbar/navbar.module';
import { ProgressBarModule } from 'src/app/components/progress-bar/progress-bar.module';


@NgModule({
    declarations: [
        MainComponent,
        DashboardComponent,
        // InvoiceComponent,
        // PaymentComponent,
        // UsersComponent,
    ],
    imports: [ 
        CommonModule,
        LazyAlertModule,
        SearchModule,
        FormsModule,
        InternetConnectionModule,
        SidebarModule,
        NavbarModule,
        RouterModule.forChild(mainRoutes),
        ProgressBarModule
    ],
    exports: [],
    providers: []
})
export class MainModule {}