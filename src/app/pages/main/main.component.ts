import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { GetTransferCredit, GetTotal } from 'src/app/states/transfer/transfer.actions';
import { AuthState } from 'src/app/states/auth/auth.state';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

    constructor(private store: Store) {};

    ngOnInit(): void {
        const userId: string = this.store.selectSnapshot(AuthState.user)._id;
        this.store.dispatch(
            [
                new GetTransferCredit(userId),
                new GetTotal(userId)
            ]
        )
    };



}
