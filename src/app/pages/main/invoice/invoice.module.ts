import { InvoiceComponent } from './invoice.component';
import { routes } from './../../../app.routing';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Invoiceroutes } from './invoice.routing';

@NgModule({
    declarations: [
        InvoiceComponent
    ],
    imports: [ 
        CommonModule,
        RouterModule.forChild(Invoiceroutes)
     ],
    exports: [],
    providers: [],
})
export class InvoiceModule {}