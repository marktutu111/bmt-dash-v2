import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { Observable } from 'rxjs';
import { FilterTransfers, GetTransfers, PayInvoice } from 'src/app/states/transfer/transfer.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
declare const swal:any;

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  @Select(TransfersState.transfers) transfers$: Observable<any[]>;
  @Select(AuthState.user) user$:Observable<any>;
  selected:TransferModel = null;

  constructor(private store: Store) {};

  ngOnInit() {
    const {company_id,account_type}=this.store.selectSnapshot(AuthState.user);
    switch (account_type) {
      case 'bsystems-admin':
        this.store.dispatch(
          new GetTransfers()
        )
        break;
      default:
        this.store.dispatch(
          new FilterTransfers(
            {
              filter:{
                company_id:company_id
              }
            }
          ),
        )
        break;
    }
  };

  pay(){
    swal.fire({
          title: 'Confirm Transaction',
          html: `
              <h3 style="margin: 20px;">
                  You have requested to make payment of Ghs${parseFloat(this.selected.amount).toFixed(2)} for
                  ${this.selected.getUser('company_name')}, Please confirm your request.
              </h3>
          `,
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Transfer'
      }
    ).then(v=>{
      if (v){
          const {_id}=this.store.selectSnapshot(AuthState.user);
          return this.store.dispatch(
              new PayInvoice(
                {
                  userId:_id,
                  transferId:this.selected._id
                }
              )
          )
      }
    })
  }


}
