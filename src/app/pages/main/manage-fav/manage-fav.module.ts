import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ManageFavComponent } from './manage-fav.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddFavComponent } from './add-fav/add-fav.component';
import { AddFavBarModule } from 'src/app/components/add-fav-bar/add-fav-bar.module';
import { ContactsFormModule } from 'src/app/components/contacts-form/contacts-form.module';


const route: Routes = [
    {
        path: '',
        component: ManageFavComponent
    },
    {
        path: 'new',
        component: AddFavComponent
    }
]



@NgModule({
    declarations: [
        ManageFavComponent,
        AddFavComponent
    ],
    imports: [ 
        CommonModule,
        RouterModule.forChild(route),
        ReactiveFormsModule,
        AddFavBarModule,
        ContactsFormModule
    ],
    exports: [],
    providers: [],
})
export class ManageFavModule {}