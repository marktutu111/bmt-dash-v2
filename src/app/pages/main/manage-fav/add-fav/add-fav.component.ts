import { Component, OnInit } from '@angular/core';
import { Store, Selector, Select } from '@ngxs/store';
import { AuthState } from 'src/app/states/auth/auth.state';
import { FavState } from 'src/app/states/favourites/favourites.actions';
import { Observable } from 'rxjs';
import { SaveFavourite, onFavouriteUpdate, UpdateFavourite } from 'src/app/states/favourites/favourites.state';

declare const swal: any;

@Component({
    selector: 'app-add-fav',
    templateUrl: './add-fav.component.html',
    styleUrls: ['./add-fav.component.scss']
})
export class AddFavComponent implements OnInit {

    

    @Select(FavState.loading) loading$: Observable<boolean>;


    accounts: any[] = [];
    search: string = '';

    private data: any[] = [];


    constructor(private store: Store) {};



    ngOnInit(): void {
        const selected = this.store.selectSnapshot(FavState.selected);
        if (selected && selected.accounts) {
            this.onChange(selected.accounts);
        }
    };
    


    onChange (accounts) {
        this.accounts = accounts;
    }

    onSearch (e) {
        this.search = e;
    }

    onFormChange (accounts) {
        this.data = accounts;
    }

    onSave () {
        const onUpdate: boolean = this.store.selectSnapshot(FavState.onUpdate);
        if (onUpdate) {
            this.update();
        } else {
            this.save();
        }
    }


    async save () {
        try {

            const empty = this.data.filter(d => Object.keys(d).find(key => d[key] === '' || d[key] == null));
            if (this.data.length > 0 && !empty[0]) {

                const userId: string = this.store.selectSnapshot(AuthState.user)._id;
                const { value } = await swal.fire(
                    {
                        title: `<h4>Enter favourite name</h4>`,
                        type: 'info',
                        input: 'text',
                        inputPlaceholder: 'name',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Yes, save!',
                        cancelButtonText: 'No, don\'t save'
                    }
                );
    
    
                if (value) {
    
                    const data = {
                        userId: userId,
                        name: value,
                        accounts: this.data
                    }
    
                    this.store.dispatch(new SaveFavourite(data));
    
                }
    
            }

        } catch (err) {

            
            
        }

    }


    update () {
        try {

            const {
                _id
            } = this.store.selectSnapshot(FavState.selected);

            this.store.dispatch(
                new UpdateFavourite(
                    {
                        id: _id,
                        data: {
                            accounts: this.data
                        }
                    }
                )
            )
            
        } catch (err) {
            
            
        }
    }




    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.store.dispatch(
            new onFavouriteUpdate(false)
        )

    }



}
