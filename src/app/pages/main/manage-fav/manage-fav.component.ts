import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AuthState } from 'src/app/states/auth/auth.state';
import { FavState } from 'src/app/states/favourites/favourites.actions';
import { GetFavourites, onFavouriteUpdate, DeleteFavourite, SelectFavourite } from 'src/app/states/favourites/favourites.state';
import { Router, ActivatedRoute } from '@angular/router';
import { GetIssuers } from 'src/app/states/transfer/transfer.actions';
import { TransfersState } from 'src/app/states/transfer/transfer.state';


declare const swal: any;

@Component({
    selector: 'app-manage-fav',
    templateUrl: './manage-fav.component.html',
    styleUrls: ['./manage-fav.component.scss']
})
export class ManageFavComponent implements OnInit {


    @Select(FavState.favourites) history$: Observable<any[]>;
    @Select(FavState.loading) loading$: Observable<boolean>;
    @Select(FavState.selected) selected$: Observable<any>;
    @Select(TransfersState.issuers) issuers$:Observable<any[]>;

    private userId: string;
    constructor(private store: Store, private router: Router, private route: ActivatedRoute) {};

    ngOnInit(): void {
        this.userId = this.store.selectSnapshot(AuthState.user)._id;
        this.store.dispatch(
            [
                new GetFavourites(this.userId),
                new GetIssuers()
            ]
        )

    };

    onHistoryChange (e) {
        const id = e.target.value;
        const favourites = this.store.selectSnapshot(FavState.favourites);
        const selected = favourites.find(_fav => _fav['_id'] === id);
        this.store.dispatch(
            new SelectFavourite(selected)
        )
    }

    edit (id: string) {
        this.router.navigate(['new'], { relativeTo: this.route });
        this.store.dispatch(new onFavouriteUpdate(true));
        
    }

    async delete (id) {
        const  { value } = await swal.fire(
            {
                html: `<h4>Delete favourite?</h4>`,
                showCancelButton: true,
                cancelButtonColor: 'red'
            }
        )

        if (value) {
            this.store.dispatch(
                new DeleteFavourite(id)
            )
        }
    }


    download () {
        return null;
    }


    clear () {
        this.store.dispatch(
            new SelectFavourite(null)
        )
    }


}
