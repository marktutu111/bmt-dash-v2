
export interface SidebarInterfaceModel {
    path: string;
    name: string;
}

export const prepaid_routes = [
    {
        path: 'buy-credit',
        name: 'Buy Prepaid',
        icon: "fa fa-money"
    },
    {
        path: 'send-money',
        name: 'Send Money',
        icon: "fa fa-paper-plane"
    },
    {
        path: 'subscriptions',
        name: 'Prepaid History',
        icon: "fa fa-credit-card"
    },
    {
        path: 'transfers',
        name: 'Transaction History',
        icon: "fa fa-clock-o"
    },
    {
        path: 'wallets',
        name: 'My saved accounts',
        icon: "fa fa-cubes"
    },
    {
        path: 'favourites',
        name: 'Manage Favourites',
        icon: 'fa fa-heart'
    },
]


export const pay_as_you_go_routes = [
    {
        path: 'send-money',
        name: 'Send Money',
        icon: "fa fa-paper-plane"
    },
    {
        path: 'transfers',
        name: 'Transaction History',
        icon: "fa fa-clock-o"
    },
    {
        path: 'wallets',
        name: 'My Saved accounts',
        icon: "fa fa-cubes"
    },
    {
        path: 'favourites',
        name: 'Manage Favourites',
        icon: 'fa fa-heart'
    },
   
]

export const coporate_routes = [
    {
        path: 'buy-credit',
        name: 'Buy Prepaid',
        icon: "fa fa-money",
        role:[
            'authorizer'
        ]
    },
    {
        path: 'send-money',
        name: 'Send Money',
        icon: "fa fa-paper-plane",
        role:[
            'payer',
        ]
    },
    {
        path: 'transfers',
        name: 'Transaction History',
        icon: "fa fa-clock-o",
        role:[
            'payer',
            'authorizer'
        ]
    },
    {
        path: 'wallets',
        name: 'My Saved accounts',
        icon: "fa fa-cubes",
        role:[
            'payer'
        ]
    },
    {
        path: 'users',
        name: 'Manage Users',
        icon: 'fa fa-user',
        role:[
            'authorizer'
        ]
    },
    {
        path: 'payments',
        name: 'Manage Payment',
        icon: 'fa fa-cubes',
        role:[
            'authorizer'
        ]
    },
    {
        path: 'favourites',
        name: 'Manage Favourites',
        icon: 'fa fa-heart',
        role:[
            'payer'
        ]
    },
    {
        path: 'invoice',
        name: 'Invoice',
        icon: 'fa fa-money',
        role:[
            'authorizer'
        ]
    }
  
   
]

export const bsystems_routes = [
    {
        path: 'transactions',
        name: 'Transactions',
        icon: "fa fa-clock-o"
    },
    {
        path: 'accounts',
        name: 'User Accounts',
        icon: 'fa fa-users'
    },
   
    {
        path: 'roles',
        name: 'Create company',
        icon: "fa fa-user"
    },
    {
        path: 'history',
        name: 'Transaction History',
        icon: "fa fa-history"
    },
    {
        path: 'settings',
        name: 'Settings',
        icon: "fa fa-cog"
    },
    {
        path: 'invoice',
        name: 'List of Invoice',
        icon: "fa fa-money"
    }
]