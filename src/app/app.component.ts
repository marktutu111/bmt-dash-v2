import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { WebSocketService } from './websockets';
import { NotificationService } from './services/notifications/notifications.service';
import { GetUserFromStore } from './states/auth/auth.actions';
import { SessionService } from './services/sessions/sessions.service';
import { AuthState } from './states/auth/auth.state';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


    constructor (
        private store: Store, 
        private router: Router, 
        private socket: WebSocketService,
        private notifications: NotificationService,
        private session: SessionService) {};

    ngOnInit(): void {

      this.store.select(AuthState).subscribe(({ user, token }) => {
        if (user && user['_id']) {
          this.socket.start(token);
          this.notifications.start();
        }
      });


      this.store.dispatch(
        new GetUserFromStore()
      )

      


    }

    




}
