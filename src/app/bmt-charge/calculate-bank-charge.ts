
const  BANK_CHARGE: number = 8;


const calculate_bank_charge = (count: string | number) => {
    if (typeof count === 'string') {
        return 8 * parseInt(count);
    }

    return 8 * count;
}


export default calculate_bank_charge;