
const charge_system = (transfer_amount) => {

    const CHARGE_PERCENT: number = 1.5;
    const _transfer_amount: number = parseFloat(transfer_amount);
    const percentage_amount = CHARGE_PERCENT / 100;
    const charge = percentage_amount * _transfer_amount;

    if (_transfer_amount < 9) {
        return 0.1;
    } else {
        return parseFloat(charge.toFixed(1));
    }

    
}




export default charge_system;