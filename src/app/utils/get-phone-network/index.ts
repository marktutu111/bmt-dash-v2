export const GetPhoneNetwork =  (num: string) => {

    if (num.length === 10) {

        const l = num.length - 9;
        const number = num.substring(l,parseInt(num));
        const networkCode = number.slice(0,-7).toString();

        if (networkCode.charAt(0).match(/[2,5]/) && networkCode.charAt(1).match(/[4,5]/)) {
                return 'mtn';
        }

        if (networkCode.charAt(0).match(/[2,5]/) && networkCode.charAt(1).match(/0/)) {
                return 'vodafone';
        }

        if (networkCode.charAt(0).match(/[2,5]/) && networkCode.charAt(1).match(/7/)) {
                return 'tigo';
        }

        if (networkCode.charAt(0).match(/[2,5]/) && networkCode.charAt(1).match(/6/)) {
                return 'airtel';
        }

        return 'unknown';

    }
    
    return null;


}