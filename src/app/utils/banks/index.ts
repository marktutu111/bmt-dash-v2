
const banks = [
    {
        'id':34,
        'Bank_Name':'Airtel Money',
        'Code':'airtel',
        'type':'momo'
    },
    {
        'id':31,
        'Bank_Name':'Vodafone Cash',
        'Code':'vodafone',
        'type':'momo'
    },
    {
        'id':32,
        'Bank_Name':'Mtn Mobile Money',
        'Code':'mtn',
        'type':'momo'
    },
    {
        'id':33,
        'Bank_Name':'Tigo Cash',
        'Code':'tigo',
        'type':'momo'
    },
    {
        "id": 1,
        "Bank_Name": "Access Bank",
        "Code":"ACB"
    },
    {
        "id":7,
        "Bank_Name":"Agricultural Development Bank",
        "Code":"ADB"
    },
    
    {
        "id":28,
        "Bank_Name": "ARB Apx Bank Ltd Bank",
        "Code":"APB"
    },
    {
        "id":22,
        "Bank_Name": "BSIC Bank",
        "Code":"BSI"
    },
    {
        "id": 3,
        "Bank_Name": "Barclays Bank",
        "Code":"BCY"
    },
    {
        "id":18,
        "Bank_Name": "Bank Of Africa",
        "Code":"BOF"
    },
    { 
        "id":30,
        "Bank_Name": "Bank Of Baroda",
        "Code":"BOB"
    },
    {
        "id":29,
        "Bank_Name": "Bank Of Ghana",
        "Code": "GOB"
    },
    {
        "id":20,
        "Bank_Name": "C Trust Bank",
        "Code":"GTB"
    },
    {
        "id":13,
        "Bank_Name": "Cal Bank",
        "Code":"CAL"
    },
    {
        "id":27,
        "Bank_Name": "Capital Bank",
        "Code":"CBG"
    },
    {
        "id":12,
        "Bank_Name": "Ecobank Bank",
        "Code":"ECO"
    },
    {
        "id":24,
        "Bank_Name": "Energy Bank",
        "Code":"ENB"
    },
    {
        "id":17,
        "Bank_Name": "FBN Bank Ghana",
        "Code":"FBN"
    },

    {
        "id":21,
        "Bank_Name": "Fidelity Bank",
        "Code":"FDL"
    },
    {
        "id": 5,
        "Bank_Name":"First Atlantic Bank",
        "Code":"FAB"
    },
    {
        "id": 4,
        "Bank_Name":"Ghana Commericial Bank",
        "Code":"GCB"
    },
    {
        "id":26,
        "Bank_Name": "GN Bank",
        "Code":"GNB"
    },
    {
        "id":10,
        "Bank_Name": "HFC Bank",
        "Code":"HFC"
    },
   
    {
        "id": 6,
        "Bank_Name": "National Investment Bank",
        "Code":"NIB"
    },
    {
        "id":15,
        "Bank_Name": "Prudential Bank",
        "Code":"PRD"
    },
    
    {
        "id":9,
        "Bank_Name": "Societe Generale",
        "Code":"SGG"
    },
    {
        "id":16,
        "Bank_Name": "Stanbic Bank",
        "Code":"STB"
    },
    {   
        "id": 2,
        "Bank_Name":"Standard Charted Bank",
        "Code":"SCB"
    },
    {
        "id":25,
        "Bank_Name": "The Royal Bank",
        "Code":"TRB"
    },
    {
        "id":19,
        "Bank_Name": "Unibank Ghana",
        "Code":"UNB"
    },
    {
        "id":8,
        "Bank_Name": "Universal Merchant Bank",
        "Code":"UMB"
    },
    {
        "id":14,
        "Bank_Name": "UT  Bank",
        "Code":"UTB"
    },
    {
        "id":11,
        "Bank_Name": "Zenith Bank",
        "Code":"ZEN"
    },
 {
        "id":23,
        "Bank_Name": "United Bank of Africa",
        "Code":"UBA"
    },

]



export default banks;

