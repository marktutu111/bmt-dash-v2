import { Injectable } from '@angular/core';
import * as Nes from "nes";
import { environment } from 'src/environments/environment';


@Injectable()
export class WebSocketService {

    private url: string;
    private readonly localhost: string = 'ws://localhost:3019/bmt';
    private readonly _prod: string = 'wss://bulkmoneytransfer.com/bmt';
    client: Nes.Client;

    constructor () {
        this.url = environment.production ? this._prod : this.localhost; 
    }

    async start (token:string) {
        try {
            this.client = new Nes.Client(this.url);
            this.client.onError =(err)=>null;
            await this.client.connect(
                { auth: { 
                        headers: { authorization: `Bearer ${token}` } 
                    } 
                }
            );
        } catch (err) {

        }
    }



}