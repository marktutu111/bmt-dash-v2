import { Routes } from "@angular/router";
import { HomeGuard } from "./guards/home.guard";
import { BsystemsRouteGuard } from "./guards/bguard";



export const routes: Routes = [
    {
        path: '',
        redirectTo: 'bsystems',
        pathMatch: 'full'
    },
    {
        path: 'login',
        loadChildren: './pages/login/login.module#AuthModule'
    },
    {
        path: 'signup',
        loadChildren: './pages/signup/signup.module#SignupModule'
    },
    {
        path: '0',
        canActivate: [
            HomeGuard
        ],
        loadChildren: './pages/main/main.module#MainModule'
    },
    {
        path: 'bsystems',
        canActivate: [
            BsystemsRouteGuard
        ],
        loadChildren: './pages/bsystems-dashboard/bsystems-dashboard.module#BsystemsDashboardModule'
    },
    {
        path: 'redirect',
        loadChildren: './pages/redirect/redirect.module#RedirectModule'
    },
    // {
    //     path: '**',
    //     redirectTo: '0'
    // },
    {
        path: 'forgotpassword',
        loadChildren:'./pages/forgot-password/forgot-password.module#forgotPasswordModule'
    },
    {
        path: 'resetpassword',
        loadChildren:'./pages/reset-password/reset-password.module#resetPasswordModule'
    }
]