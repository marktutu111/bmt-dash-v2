import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';


@Injectable()
export class PaymentAccountService {


    constructor (private http: HttpClient) {};


    getAccounts (id: string): Promise<any> {
        return this.http.get(`${API.PAYMENT_ACCOUNT}/get/${id}`)
                        .toPromise();

    }

    deleteAccount (id: string): Promise<any> {
        return this.http.delete(`${API.PAYMENT_ACCOUNT}/delete/${id}`)
                        .toPromise();
    }

    newAccount (data): Promise<any> {
        return this.http.post(`${API.PAYMENT_ACCOUNT}/new`, data)
                        .toPromise();
    }



}