import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';

@Injectable()
export class FavouriteService {

    
    constructor (private http: HttpClient) {};


    save (data): Promise<any> {
        return this.http.post(`${API.HISTORY}/save`, data)
                        .toPromise();
    }

    getUserHistory (userId: string): Promise<any> {
        return this.http.get(`${API.HISTORY}/get/${userId}`)
                        .toPromise();
    }


    updateHistory (data): Promise<any> {
        return this.http.put(`${API.HISTORY}/update`, data)
                        .toPromise();
    }


    deleteHistory (id: string): Promise<any> {
        return this.http.delete(`${API.HISTORY}/delete/${id}`)
                        .toPromise();
    }


}
