import { Injectable } from '@angular/core';
import { WebSocketService } from 'src/app/websockets';
import { Store } from '@ngxs/store';
import { TransferModel } from 'src/app/models/transfers/transfers.model';
import { UpdateTransferStatus, UpdateTransactionStatus, GetTransferCredit, GetTotal, GetTransfersByUser, SelectTransfers } from 'src/app/states/transfer/transfer.actions';
import { TransactionsModel } from 'src/app/models/transactions/transactions.model';
import { UpdateSubscriptionStatus } from 'src/app/states/subscriptions/subscriptions.actions';
import { IUser } from 'src/app/models/users';
import { SubscriptionsInterfaceModel, SubscriptionModel } from 'src/app/models/subscriptions/subscriptions.model';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthState } from 'src/app/states/auth/auth.state';
import { Get_Unseen } from 'src/app/components/sidebar/state/actions';
import { Toggle } from 'src/app/components/progress-bar/progress-bar.actions';

declare const iziToast: any;
declare const swal: any;



interface SubscriptionNotificationInterface {
    subscription: SubscriptionsInterfaceModel;
    user: IUser;
}



@Injectable()
export class NotificationService {

    constructor (
        private socket: WebSocketService, 
        private store: Store,
        private router: Router,
        private route: ActivatedRoute
    ) {};


    start () {
        const userId: string = this.store.selectSnapshot(AuthState.user)._id;
        this.socket.client.subscribe(`/bmt/transfers/${userId}`, (transfer) => this.onTransfer(new TransferModel(transfer)));
        this.socket.client.subscribe(`/bmt/subscriptions/${userId}`, (data) => this.onSubscription(new SubscriptionModel(data)));
        this.socket.client.subscribe(`/bmt/transactions/${userId}`, (transaction) => this.onTransaction(new TransactionsModel(transaction)));
    }


    onTransfer (transfer: TransferModel) {
        const userId: string = transfer['userId']['_id'];
        this.store.dispatch(
            [
                new SelectTransfers(transfer),
                new UpdateTransferStatus(transfer),
                new Get_Unseen('transfers'),
                new GetTransferCredit(userId),
                new GetTotal(userId),
                new GetTransfersByUser(userId),
                new Toggle(false)
            ]
        ); 

        swal(
            `<h2>Transaction status</h2>`,
            `<h5>${transfer.getDebitStatus()}</h5>`,
            'info'
        );

        this.router.navigate([`0/transfers/${transfer['_id']}`]);

    }


    onTransaction (transaction: TransactionsModel) {
        this.store.dispatch(
            [
                new UpdateTransactionStatus(transaction),
                new GetTransferCredit(transaction['userId']['_id']),
                new GetTotal(transaction['userId']['_id'])
            ]
        );

        iziToast.info(
            {
                title: 'Transaction Status',
                message: transaction.getCreditStatus(),
            }
        );

    }


    onSubscription (subscription) {
        subscription.new = true;
        this.store.dispatch(
            [
                new GetTransferCredit(subscription['userId']['_id']),
                new UpdateSubscriptionStatus(subscription)
            ]
        );

        swal(
            `<h2>Transaction status</h2>`,
            `<h5>${subscription.getStatus()}</h5>`,
            'info'
        )

    }

}