import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';

@Injectable()
export class DashboardService {

    constructor (private http: HttpClient) {};

    getDashboard (user: string): Promise<any> {
        return this.http.get(`${API.DASHBAORD}/get/${user}`).toPromise();
    }


}