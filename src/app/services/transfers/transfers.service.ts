import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';
import { SubscriptionUpdateModel } from 'src/app/models/subscriptions/subscriptions.model';
import { TransferInterfaceModel } from 'src/app/models/transfers/transfers.model';



@Injectable()
export class TransferService {


    constructor (private http: HttpClient) {};


    get (): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get`)
                        .toPromise();
    }

    getIssuers (): Promise<any> {
        return this.http.get(`${API.ISSUERS}/get`)
                        .toPromise();
    }

    getTotal (id: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/total/${id}`)
                        .toPromise();
    }

    
    getById (id: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/${id}`)
                        .toPromise();
    }

    getByUser (id: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/user/${id}`)
                        .toPromise();
    }

    delete (id: string): Promise<any> {
        return this.http.delete(`${API.TRANSFERS}/delete/${id}`)
                        .toPromise();
    }


    new (subject: TransferInterfaceModel): Promise<any> {
        return this.http.post(`${API.TRANSFERS}/new`, subject)
                        .toPromise();
    }


    update (data: SubscriptionUpdateModel): Promise<any> {
        return this.http.put(`${API.TRANSFERS}/update`, data)
                        .toPromise();
    }

    payinvoice (data: SubscriptionUpdateModel): Promise<any> {
        return this.http.post(`${API.TRANSFERS}/company/pay`, data)
                        .toPromise();
    }

    
    search (search): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/search?${search}`)
                        .toPromise();
    }


    getTransactions (id: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/transactions/${id}`)
                        .toPromise();
    }

    getExcel (data): Promise<any> {
        return this.http.post(`${API.CONVERT}/convert/csv`, data)
                        .toPromise();
    }


    saveTransfer (data): Promise<any> {
        return this.http.post(`${API.HISTORY}/save`, data)
                        .toPromise();
    }


    getUserHistory (userId: string): Promise<any> {
        return this.http.get(`${API.HISTORY}/get/${userId}`)
                        .toPromise();
    }


    updateHistory (data): Promise<any> {
        return this.http.put(`${API.HISTORY}/update`, data)
                        .toPromise();
    }

    authorizePayment (data): Promise<any> {
        return this.http.put(`${API.TRANSFERS}/payment/authorize`, data)
                        .toPromise();
    }


    deleteHistory (id: string): Promise<any> {
        return this.http.delete(`${API.HISTORY}/delete/${id}`)
                        .toPromise();
    }


    getTransferHistory (transferId: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/${transferId}`)
                        .toPromise();
    }

    getTransfer(id:string):Promise<any>{
        return this.http.get(`${API.TRANSFERS}/get/${id}`)
                        .toPromise();
    }

    
    filter_by_date (data): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/filter/date/${data[0]}/${data[1]}`)
                        .toPromise();
    }

    filter (data): Promise<any> {
        return this.http.post(`${API.TRANSFERS}/filter`,data)
                        .toPromise();
    }




    filter_date_by_user ({ id, data }): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/filter/user/date/${id}/${data[0]}/${data[1]}`)
                        .toPromise();
    }

    get_summary (): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/summary/data`)
                        .toPromise();
    }


    get_user_summary (id: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/summary/data/${id}`)
                        .toPromise();
    }

    
    search_filter (search: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/search/filter/${search}`)
                        .toPromise();
    }

    
    
    handlerVbv (data): Promise<any> {
        return this.http.post(`${API.TRANSFERS}/theteller/vbv`, data)
                        .toPromise();
    }


    
}