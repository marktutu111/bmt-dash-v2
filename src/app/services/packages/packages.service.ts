import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';
import { PackageInterfaceModel, PackageUpdateInterface } from 'src/app/models/packages/packages.model';




@Injectable()
export class PackageService {


    constructor (private http: HttpClient) {};


    get (): Promise<any> {
        return this.http.get(`${API.PACKAGES}/get`)
                        .toPromise();
    }


    
    getById (id: string): Promise<any> {
        return this.http.get(`${API.PACKAGES}/get/${id}`)
                        .toPromise();
    }


    delete (id: string): Promise<any> {
        return this.http.delete(`${API.PACKAGES}/delete/${id}`)
                        .toPromise();
    }


    newPackage (subject: PackageInterfaceModel): Promise<any> {
        return this.http.post(`${API.PACKAGES}/new`, subject)
                        .toPromise();
    }


    update (data: PackageUpdateInterface): Promise<any> {
        return this.http.put(`${API.PACKAGES}`, data)
                        .toPromise();
    }

    search (search): Promise<any> {
        return this.http.get(`${API.PACKAGES}/search?${search}`)
                        .toPromise();
    }


    
}