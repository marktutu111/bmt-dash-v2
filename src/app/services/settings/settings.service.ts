import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';

@Injectable()
export class SettingsService {

    constructor (private http: HttpClient) {};

    getSettings = ():Promise<any> => this.http.get(`${API.SETTINGS}/get`)
                        .toPromise();

    updateSettings = (data):Promise<any> => this.http.post(`${API.SETTINGS}/new`, data)
                        .toPromise();


}