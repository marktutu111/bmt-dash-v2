import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';

@Injectable()
export class SearchService {

    constructor (private http: HttpClient) {};

    search (id: string, text: string): Promise<any> {
        return this.http.get(`${API.SEARCH}/new/${id}/${text}`)
                        .toPromise();
    }


    searchUser (id: string, user: string, text: string): Promise<any> {
        return this.http.get(`${API.SEARCH}/user/${id}/${user}/${text}`)
                        .toPromise();
    }

}