import { Injectable } from '@angular/core';
declare const Stripe: any;


@Injectable()
export class StripeService {

    stripe: any;

    constructor () {
        this.stripe = Stripe('pk_test_TYooMQauvdEDq54NiTphI7jx');
    };

    createToken (card) {
        return this.stripe.createToken(card);
    }

}