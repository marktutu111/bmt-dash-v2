import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';
import { SubscriptionsInterfaceModel, SubscriptionUpdateModel } from 'src/app/models/subscriptions/subscriptions.model';



@Injectable()
export class SubscriptionService {


    constructor (private http: HttpClient) {};


    getSubscriptions (): Promise<any> {
        return this.http.get(`${API.SUBSCRIPTIONS}/get`)
                        .toPromise();
    }


    getById (id: string): Promise<any> {
        return this.http.get(`${API.SUBSCRIPTIONS}/get/${id}`)
                        .toPromise();
    }


    getByUser (id: string): Promise<any> {
        return this.http.get(`${API.SUBSCRIPTIONS}/get/user/${id}`)
                        .toPromise();
    }


    delete (id: string): Promise<any> {
        return this.http.delete(`${API.SUBSCRIPTIONS}/delete/${id}`)
                        .toPromise();
    }


    newSubscription (subject: SubscriptionsInterfaceModel): Promise<any> {
        return this.http.post(`${API.SUBSCRIPTIONS}/new`, subject)
                        .toPromise();
    }


    update (data: SubscriptionUpdateModel): Promise<any> {
        return this.http.put(`${API.SUBSCRIPTIONS}`, data)
                        .toPromise();
    }

    search (search): Promise<any> {
        return this.http.get(`${API.SUBSCRIPTIONS}/search?${search}`)
                        .toPromise();
    }


    topup (topup): Promise<any> {
        return this.http.post(`${API.SUBSCRIPTIONS}/topup`, topup)
                        .toPromise();
    }


    filter_by_date (data): Promise<any> {
        return this.http.get(`${API.SUBSCRIPTIONS}/filter/date/${data[0]}/${data[1]}`)
                        .toPromise();
    }


    
}