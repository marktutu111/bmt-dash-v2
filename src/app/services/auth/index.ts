import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { API } from '../../constants/api';
import { LoginPayload } from '../../models/auth';

@Injectable()
export class AuthService {


    constructor (private http: HttpClient) {};

    

    login (data: LoginPayload): Promise<any> {
        return this.http.post(`${API.USERS}/login`, data)
                        .toPromise()
    }


    set_last_seen (id): Promise<any> {
        return this.http.put(`${API.USERS}/last-seen`, { id: id })
                        .toPromise();
    }

    forgotPassword (data): Promise<any> {
        return this.http.post(`${API.USERS}/forgot-password`, data)
                        .toPromise();

    }

    updatePassword (data): Promise<any> {
        return this.http.post(`${API.USERS}/update-password`, data)
                        .toPromise();
    }


    resetPassword (data): Promise<any> {
        return this.http.post(`${API.USERS}/reset-password`, data)
                        .toPromise();
    }



}