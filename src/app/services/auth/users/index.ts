import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { API } from '../../../constants/api';
import { IUser } from '../../../models/users';
import { SingupInterfaceModel} from '../../../models/auth';

@Injectable()
export class UserService {

    constructor (private http: HttpClient) {};

    getUsers (): Promise<{}> {
        return this.http.get(`${API.USERS}/get`).toPromise();
    }

    getUserById (id: string): Promise<any> {
        return this.http.get(`${API.USERS}/get/${id}`).toPromise();
    }

    addUser (user: IUser): Promise<any> {
        return this.http.post(`${API.USERS}/new`, user).toPromise();
    }
    newSignup (data: SingupInterfaceModel): Promise<any> {
        return this.http.post(`${API.USERS}/new`, data)
                        .toPromise();
    }

    deleteUser (id: string): Promise<any> {
        return this.http.delete(`${API.USERS}/delete/${id}`).toPromise();
    }

    updateUser (data: {id: string, data: IUser}): Promise<any> {
        return this.http.post(`${API.USERS}/update`, data).toPromise();
    }


    addRole (data): Promise<any> {
        return this.http.post(`${API.USERS}/new`, data)
                        .toPromise();
    }

    filterUser (data):Promise<any>{
        return this.http.post(`${API.USERS}/filter`,data).toPromise();
    }

    get_roles (userId,corporateId): Promise<any> {
        return this.http.get(`${API.USERS}/get/corporate/accounts/${userId}/${corporateId}`)
                        .toPromise();
    }

    
    get_summary (): Promise<any> {
        return this.http.get(`${API.USERS}/get/summary/data`)
                        .toPromise();
    }


    get_bsystems_admin (): Promise<any> {
        return this.http.get(`${API.USERS}/get/bsystems/accounts`)
                        .toPromise();
    }


}