import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { API } from '../../../constants/api';
import { IAdmin } from 'src/app/models/auth';

@Injectable()
export class AdminService {

    constructor (private http: HttpClient) {};

    getAdmins (): Promise<{}> {
        return this.http.get(`${API.ADMINS}/get`).toPromise();
    }

    getAdminById (id: string): Promise<{}> {
        return this.http.get(`${API.ADMINS}/get/${id}`).toPromise();
    }

    addAdmin (user: IAdmin): Promise<any> {
        return this.http.post(`${API.ADMINS}/new`, user).toPromise();
    }

    deleteAdmin (id): Promise<any> {
        return this.http.delete(`${API.ADMINS}/delete/${id}`).toPromise();
    }

    updateAdmin (data): Promise<any> {
        return this.http.post(`${API.ADMINS}/update`, data).toPromise();
    }

}