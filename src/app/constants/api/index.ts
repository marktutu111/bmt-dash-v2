import { environment } from "src/environments/environment";
let BASE_URL: string;

const LIVE: string = 'http://18.224.63.55/bmt';
const TEST: string = 'https://bulkmoneytransfer.com/bmt';
const DEV: string = 'http://localhost:3119/bmt';

if (environment.production) {
    BASE_URL = TEST;
} else {
    BASE_URL = DEV;
}


export const API = {
    // AUTH: `${BASE_URL}/api/v1/auth/admin`,
    // USERS: `${BASE_URL}/api/v1/users`,
    // ADMINS: `${BASE_URL}/api/v1/admin`,
    // SUBSCRIPTIONS: `${BASE_URL}/api/v1/subscriptions`,
    // TOPUP: `${BASE_URL}/api/v1/topup`,
    // PACKAGES: `${BASE_URL}/api/v1/packages`,
    // TRANSFERS: `${BASE_URL}/api/v1/transfers`,
    // TRANSACTIONS: `${BASE_URL}/api/v1/transactions`,
    // ISSUERS: `${BASE_URL}/peoplepay/issuers`,
    // DASHBAORD: `${BASE_URL}/api/v1/dashboard`,
    // SEARCH: `${BASE_URL}/api/v1/search`,
    // CONVERT: `${BASE_URL}/api/v1/convertor`,
    // PAYMENT_ACCOUNT: `${BASE_URL}/api/v1/payment-accounts`,
    // HISTORY: `${BASE_URL}/api/v1/history`,
    // CONFIG: `${BASE_URL}/api/v1/config`,
    // SETTINGS: `${BASE_URL}/api/v1/settings`,
    // SEND_SMS: `${BASE_URL}/infobib/sms/send`,
    // GET_OTP: `${BASE_URL}/api/v1/otp/sendotp`

    AUTH: `${BASE_URL}/api/v1/auth/admin`,
    USERS: `${BASE_URL}/api/v1/users`,
    ADMINS: `${BASE_URL}/api/v1/admin`,
    SUBSCRIPTIONS: `${BASE_URL}/api/v1/subscriptions`,
    TOPUP: `${BASE_URL}/api/v1/topup`,
    PACKAGES: `${BASE_URL}/api/v1/packages`,
    TRANSFERS: `${BASE_URL}/api/v1/transfers`,
    TRANSACTIONS: `${BASE_URL}/api/v1/transactions`,
    ISSUERS: `${BASE_URL}/api/v1/issuers`,
    DASHBAORD: `${BASE_URL}/api/v1/dashboard`,
    SEARCH: `${BASE_URL}/api/v1/search`,
    CONVERT: `${BASE_URL}/api/v1/convertor`,
    PAYMENT_ACCOUNT: `${BASE_URL}/api/v1/payment-accounts`,
    HISTORY: `${BASE_URL}/api/v1/history`,
    CONFIG: `${BASE_URL}/api/v1/config`,
    SETTINGS: `${BASE_URL}/api/v1/settings`,
    SEND_SMS: `http://3.135.217.52/infobib/sms/send`,
    GET_OTP: `${BASE_URL}/api/v1/otp/sendotp`,
    NEC: `${BASE_URL}/api/v1/nec`
}
