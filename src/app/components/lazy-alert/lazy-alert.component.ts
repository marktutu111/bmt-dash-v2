import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LAlert } from './model';
import { Select, Store } from '@ngxs/store';
import { AlertState } from '../../states/alerts/alerts.state';
import { Observable } from 'rxjs';
import { AlertAction, CancelAlert } from '../../states/alerts/alerts.actions';


@Component({
    selector: 'app-lazy-alert',
    templateUrl: './lazy-alert.component.html',
    styleUrls: ['./lazy-alert.component.scss']
})
export class LazyAlertComponent implements OnInit {

    @Select(AlertState.alert) alert$: Observable<LAlert>;
    
    constructor (private store: Store) {};


    ngOnInit(): void {};


    onKay (data: any) {
        this.store.dispatch([
            new CancelAlert(),
            new AlertAction({
                data: data,
                type: 'ok'
            })
        ]);
    }

    onCancel (data: any) {
        this.store.dispatch([
            new CancelAlert(),
            new AlertAction({
                data: data,
                type: 'cancel'
            })
        ]);
    }

    onContinue (data: any) {
        this.store.dispatch([
            new CancelAlert(),
            new AlertAction({
                data: data,
                type: 'confirm'
            })
        ]);
    }

}
