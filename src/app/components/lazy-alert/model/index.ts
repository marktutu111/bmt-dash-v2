
export interface LAlertData {
    id: string;
    meta?: any;
}


export interface LAlert {
    message?: string;
    toggle: boolean;
    type?: 'alert' | 'prompt' | 'loader' | 'default',
    data?: LAlertData;
    success?: boolean;
}