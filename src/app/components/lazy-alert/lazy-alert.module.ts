import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyAlertComponent } from './lazy-alert.component';



@NgModule({
    declarations: [
        LazyAlertComponent
    ],
    imports: [ 
        CommonModule 
    ],
    exports: [
        LazyAlertComponent
    ],
    providers: [],
})
export class LazyAlertModule {}