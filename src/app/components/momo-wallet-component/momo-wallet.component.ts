import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MomoWalletInterfaceModel } from './momo-wallet.model';
import { Select } from '@ngxs/store';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { Observable } from 'rxjs';
// import {} from '../../../assets/img/networks/'

@Component({
    selector: 'app-momo-wallet',
    templateUrl: './momo-wallet.component.html',
    styleUrls: ['./momo-wallet.component.scss']
})
export class MomoWalletComponent implements OnInit {

    @Select(TransfersState.issuers) issuers$:Observable<any[]>;
    @Input() data: MomoWalletInterfaceModel = {};
    @Output() deleteEvent: EventEmitter<string> = new EventEmitter();


    networks: any[] = [
        {
            image: '../../../dashboard/assets/img/networks/mtn-logo.png',
            name: 'mtn',
            color:'rgb(245, 245, 010)'
        },
        {
            image: '../../../dashboard/assets/img/networks/vodafone_ghana.jpg',
            name: 'vodafone',
            color:'rgb(207, 9, 9)'
        },
        {
            image: '../../../dashboard/assets/img/networks/Tigo.jpg',
            name: 'tigo',
            color:'rgb(4, 110, 197)',
        },
        {
            image: '../../../dashboard/assets/img/networks/airtel.jpg',
            name: 'airtel',
            color:'rgb(207, 9, 9)'
        }
    ]
    constructor() {};


    ngOnInit(): void {};


    delete (id: string) {
        this.deleteEvent.emit(id);
    }



}
