import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MomoWalletComponent } from './momo-wallet.component';

@NgModule({
    declarations: [
        MomoWalletComponent
    ],
    imports: [ CommonModule ],
    exports: [
        MomoWalletComponent
    ],
    providers: [],
})
export class MomoWalletModule {}