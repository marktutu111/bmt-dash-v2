
export interface MomoWalletInterfaceModel {
    wallet?: string;
    network?: string;
    _id?: string;
    createdAt?: string;
    name?: string;
    account_number?:string;
}