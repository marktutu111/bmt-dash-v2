import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'app-theteller',
    templateUrl: './theteller.component.html',
    styleUrls: ['./theteller.component.scss']
})
export class TheTellerComponent implements OnInit {

    @Output ()
    process: EventEmitter<any> = new EventEmitter();

    


    constructor() {};

    ngOnInit(): void {};

    
    processPayment (e) {
        this.process.emit(e);
    }

}
