import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TheTellerComponent } from './theteller.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {CardModule} from 'ngx-card/ngx-card';
import {  NgPaymentCardModule } from 'ng-payment-card';


@NgModule({
    declarations: [
        TheTellerComponent
    ],
    imports: [ 
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CardModule,
        NgPaymentCardModule
    ],
    exports: [
        TheTellerComponent
    ],
    providers: [],
})
export class TheTellerComponentModule {}