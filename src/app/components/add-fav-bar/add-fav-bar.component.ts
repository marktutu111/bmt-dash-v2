import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Location } from '@angular/common';
declare const swal: any;




@Component({
    selector: 'app-add-fav-bar',
    templateUrl: './add-fav-bar.component.html',
    styleUrls: ['./add-fav-bar.component.scss']
})
export class AddFavBarComponent implements OnInit {

    activate: boolean = false;

    @Input()
    set activate_save (value) {
        this.activate = value;
    }

    @Output() changeEvent: EventEmitter<any> = new EventEmitter();
    @Output() searchEvent: EventEmitter<any> = new EventEmitter();
    @Output() saveEvent: EventEmitter<any> = new EventEmitter();

    constructor (private location: Location) {}

    ngOnInit(): void {};


    onCSVChange (e) {
        try {
            

            const {
                name
            } = e.target.files[0];

            const file_type = name.split('.')[1];
            if (file_type === 'csv') {


                const fileReader = new FileReader();
                fileReader.onloadend = () => {

                    const buffer = fileReader.result;
                    const csv = buffer.toString().split(/\r\n|\n/);

                    let temp: any[] = [];

                    csv.forEach(account => {

                        const _account = account.split(',');
                        if (_account.length === 5) {

                            let wallet = _account[1] ? _account[1].toString() : '';
                            if (wallet[0] !== '0') wallet = `0${wallet}`;
                            wallet.substring(0,10);

                            const d = {
                                account_name: _account[0],
                                account_number: wallet,
                                amount: _account[2],
                                account_issuer: _account[3].toString().toLowerCase(),
                                description: _account[4]
                            }

                            temp.push(d);

                        }

                    });

                    this.changeEvent.emit(temp);
        
                }
        
                return fileReader.readAsBinaryString(e.target.files[0]);
    
            } throw Error('Unsupported file type');

        } catch (err) {
            swal.fire(
                {
                    html: `<h4>${err.message}</h4>`
                }
            )
        }

    }


    generateForms (count: number) {
        let accounts = [];
        const account = {
            account_issuer: null,
            account_number: null,
            amount: null,
            description: null,
            account_name: null
        }

        for (let index = 0; index < count; index++) {
            accounts.push(account);
        }

        return accounts;

    }


    addMore () {
        swal.fire(
            {
                title: '<h4><b>How many people would you like to add?</b></h4>',
                input: 'number',
                inputPlaceholder: '0',
                showCancelButton: true
            }
        ).then(({ value }) => {
            if (!value) {
                return;
            }
            const accounts = this.generateForms(value);
            return this.changeEvent.emit(accounts);

        });

    }

    addOne () {
        const form = this.generateForms(1);
        this.changeEvent.emit(form);
    }

    onSearch (e) {
        const value = e.target.value;
        this.searchEvent.emit(value);
    }

    save () {
        this.saveEvent.emit();
    }
    

    back () {
        this.location.back();
    }

}
