import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddFavBarComponent } from './add-fav-bar.component';


@NgModule({
    declarations: [
        AddFavBarComponent
    ],
    imports: [ CommonModule ],
    exports: [
        AddFavBarComponent
    ],
    providers: [],
})
export class AddFavBarModule {}