import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppState } from '../../states/app/state';
import { ToggleSideContent } from '../../states/app/actions';


@Component({
    selector: 'app-sidebar-wide',
    templateUrl: './side-content-full.component.html',
    styleUrls: ['./side-content-full.component.scss']
})
export class SideBarWideComponent implements OnInit {

    @Select(AppState.toggled) onToggle$: Observable<boolean>;

    constructor(private store: Store) {}

    ngOnInit(): void {}

    clicked () {
        this.store.dispatch(new ToggleSideContent());
    }

}
