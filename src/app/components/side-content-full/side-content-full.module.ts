import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SideBarWideComponent } from './side-content-full.component';


@NgModule({
    declarations: [
        SideBarWideComponent
    ],
    imports: [ CommonModule ],
    exports: [
        SideBarWideComponent
    ],
    providers: [],
})
export class SideBarWideModule {}