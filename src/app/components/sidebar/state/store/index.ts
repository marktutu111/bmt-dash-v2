import { State, Action, StateContext, Selector, Store } from '@ngxs/store';
import { SideBar_New_Notification, Get_Unseen } from '../actions';
import { SidebarService } from '../../services/sidebar.service';
import { AuthState } from 'src/app/states/auth/auth.state';


interface StateModel {
    notifications: any;
}


@State<StateModel>({
    name: 'SideBar',
    defaults: {
        notifications: {}
    }
})
export class SidebarState {


    constructor (private service: SidebarService, private store: Store) {};


    @Selector()
    static notifications (state: StateModel) {
        return state.notifications;
    }


    @Action(SideBar_New_Notification)
    sideBar_New_Notification ({ patchState, getState }: StateContext<StateModel>, { type }: SideBar_New_Notification) {

        let notifications = getState().notifications;
        !notifications[type] ? notifications[type] = 1 : notifications[type] += 1;
        patchState(
            {
                notifications: notifications
            }
        )
        
    }


    @Action(Get_Unseen)
    async getUnseen ({ patchState, getState }: StateContext<StateModel>, { type }: Get_Unseen) {
        try {

            const id: string = this.store.selectSnapshot(AuthState.user)._id;
            let notifications = getState().notifications;

            switch (type) {
                case 'transfers':
                    const response = await this.service.get_transfer_unseen(id);
                    notifications['transfers'] = response['data'];
                    return patchState(
                        {
                            notifications: notifications
                        }
                    )
                default:
                    break;
            }
            
        } catch (err) {
            
        }
    }



}
