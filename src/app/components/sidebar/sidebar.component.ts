import { Component, OnInit, Input } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { SidebarInterfaceModel, prepaid_routes, pay_as_you_go_routes, coporate_routes, bsystems_routes } from 'src/app/config/sidebar/sidebar.config';
import { AppState } from 'src/app/states/app/state';
import { Observable } from 'rxjs';
import { SidebarState } from './state/store';
import { Get_Unseen } from './state/actions';
import { AuthState } from 'src/app/states/auth/auth.state';



@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    @Select(AppState.account_type) account_type$: Observable<'prepaid' | 'pay_as_you_go' | 'corporate' | 'bsystems' | 'company' | 'user'>;
    @Select(SidebarState.notifications) notifications$: Observable<any>;
    @Input() sidebar: SidebarInterfaceModel[]=[];
    date: string = new Date().getFullYear().toString();


    constructor(private store: Store) {};


    ngOnInit() {
      this.account_type$.subscribe(account => {
        const role=this.store.selectSnapshot(AuthState.user).company_role;
        switch (account) {
          case 'prepaid':
            this.sidebar = prepaid_routes;
            break;
          case 'pay_as_you_go':
            this.sidebar = pay_as_you_go_routes;
            break;
          case 'corporate':
            this.sidebar = coporate_routes;
            break;
          case 'company':
            coporate_routes.filter((route,i)=>{
              route.role.forEach(r=>{
                if (r === role){
                  this.sidebar.push(route);
                }
              })
            })
            break;
          case 'bsystems':
            this.sidebar = bsystems_routes;
            break;
          default:
            break;
        }
      })

      this.store.dispatch(new Get_Unseen('transfers'));


    }



    onClicked (path: string) {};



}
