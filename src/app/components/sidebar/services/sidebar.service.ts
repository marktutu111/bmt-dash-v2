import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from 'src/app/constants/api';

@Injectable()
export class SidebarService {

    constructor (private http: HttpClient) {};

    get_transfer_unseen (id: string): Promise<any> {
        return this.http.get(`${API.TRANSFERS}/get/unseen/${id}`)
                        .toPromise();
    }
    

}