import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-internet-connection',
    templateUrl: './internet-connection.component.html',
    styleUrls: ['./internet-connection.component.scss']
})
export class InterConnectionComponent implements OnInit {

    toggle: boolean = false;

    constructor() {};

    ngOnInit(): void {
        window.ononline = () => {
            this.toggle = false
        }

        window.onoffline = () => {
            this.toggle = true;
        }

    };




}
