import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InterConnectionComponent } from './internet-connnection.component';


@NgModule({
    declarations: [
        InterConnectionComponent
    ],
    imports: [ CommonModule ],
    exports: [
        InterConnectionComponent
    ],
    providers: [],
})
export class InternetConnectionModule {}