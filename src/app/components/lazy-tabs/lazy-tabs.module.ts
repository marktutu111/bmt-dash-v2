import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyTabsComponent } from "./lazy-tabs.component";

@NgModule({
    declarations: [
        LazyTabsComponent
    ],
    imports: [ CommonModule ],
    exports: [
        LazyTabsComponent
    ],
    providers: [],
})
export class LazyTabsModule {}