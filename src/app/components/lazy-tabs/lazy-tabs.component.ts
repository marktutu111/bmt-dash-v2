import { 
  Component,  
  Output, 
  EventEmitter, 
  Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


// import { TAB_INDEX } from "../models";


@Component({
  selector: 'lazy-tabs',
  templateUrl: './lazy-tabs.component.html',
  styleUrls: ['./lazy-tabs.component.css']
})
export class LazyTabsComponent {

        @Input() 
        selected: number;
        
        @Input() 
        tabs: string[] = [];

        @Output() 
        onTabChanged: EventEmitter<number | string> = new EventEmitter();

        @Input() 
        eventType: 'index' | 'tabs' = 'index';

        @Input()
        enableChange: boolean = true;


        constructor(private router: Router, private route: ActivatedRoute) {

              this.selected = 0;

        }

        tabChanged (index: number,tab: string) {

          if (this.enableChange) {

              this.selected = index;
              
              if (this.eventType === 'index') {
                this.onTabChanged.emit(index);
              } else {
                this.onTabChanged.emit(tab);
                this.router.navigate(
                  [
                    tab.toLowerCase()
                  ],
                  {
                    relativeTo: this.route
                  }
                );
              }

          }


        }


}
