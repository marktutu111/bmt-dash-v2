import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionListGridComponent } from './transaction-list-grid.component';


@NgModule({
    declarations: [
        TransactionListGridComponent
    ],
    imports: [ CommonModule ],
    exports: [
        TransactionListGridComponent
    ],
    providers: [],
})
export class TransactionListGridModule {}