import { Component, OnInit, Input } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ListGridState } from './transaction-list-grid.state';
import { Observable } from 'rxjs';
import { ToggleListGrid } from './transaction-list-grid.actions';

@Component({
    selector: 'app-transaction-list-grid',
    templateUrl: './transaction-list-grid.component.html',
    styleUrls: ['./transaction-list-grid.component.scss']
})
export class TransactionListGridComponent implements OnInit {

    @Select(ListGridState.toggle) toggle$: Observable<boolean>;
    constructor(private store: Store) {};

    ngOnInit(): void {};

    close () {
        this.store.dispatch(
            new ToggleListGrid(false)
        )
    }


}
