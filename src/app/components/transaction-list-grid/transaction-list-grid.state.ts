import { State, Selector, Action, StateContext } from '@ngxs/store';
import { ToggleListGrid } from './transaction-list-grid.actions';

interface ListGridModel {
    toggle: boolean;
}

@State<ListGridModel>({
    name: 'ListGrid',
    defaults: {
        toggle: false
    }
})
export class ListGridState {

    @Selector()
    static toggle (state: ListGridModel) {
        return state.toggle;
    }

    @Action(ToggleListGrid)
    toggleListGrid ({ patchState }: StateContext<ListGridModel>, { event }: ToggleListGrid) {
        patchState(
            {
                toggle: event
            }
        )
    }

}
