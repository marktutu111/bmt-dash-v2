

export class ToggleListGrid {
    static readonly type = '[ListGrid] ToggleListGrid';
    constructor(public readonly event: boolean) {};
}