import { InterfaceActivePath } from "src/app/states/app/state";


export class NewSearch {
    static readonly type = '[Search] NewSearch';
    constructor (public id: InterfaceActivePath, public text: string) {};
}
