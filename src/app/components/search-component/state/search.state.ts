import { State, Selector, Action, StateContext, Store } from '@ngxs/store';
import { SearchService } from 'src/app/services/search/search.service';
import { NewSearch } from './search.action';
import { PushTransferSearchResults } from 'src/app/states/transfer/transfer.actions';
import { PushSubscriptionSearchResults } from 'src/app/states/subscriptions/subscriptions.actions';
import { AuthState } from 'src/app/states/auth/auth.state';


export interface SearchStateModel {
    onSearch: boolean;
}


@State<SearchStateModel>({
    name: 'Search',
    defaults: {
        onSearch: false
    }
})
export class SearchState {

    @Selector()
    static onSearch (state: SearchStateModel) {
        return state.onSearch;
    }


    constructor (private service: SearchService, private store: Store) {};




    @Action(NewSearch)
    async newSearch ({ patchState, dispatch }: StateContext<SearchStateModel>, { id,text }: NewSearch) {
        try {

            patchState(
                { 
                    onSearch: true 
                }
            );

            
            const userId: string = this.store.selectSnapshot(AuthState.user)._id;
            const response = await this.service.searchUser(id,userId,text);


            if (response.success) {

                switch (id) {
                    case 'subscriptions':
                        dispatch(
                            new PushSubscriptionSearchResults(response.data)
                        )
                        break;
                    case 'transfers':
                        dispatch(
                            new PushTransferSearchResults(response.data)
                        );
                        break;
                    default:
                        break;
                }

                return patchState(
                    { 
                        onSearch: false 
                    }
                );
                
            }


            throw Error(response.message);   

            
        } catch (err) {
            
            patchState(
                {
                    onSearch: false
                }
            );

        }
    }
    
    

}
