import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { NewSearch } from './state/search.action';
import { SearchState } from './state/search.state';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/states/app/state';
import { GetUsers } from 'src/app/states/users/users.actions';
import { GetAdmins } from 'src/app/states/admin/actions';
import { GetSubscriptions } from 'src/app/states/subscriptions/subscriptions.actions';
import { GetTransfers } from 'src/app/states/transfer/transfer.actions';



@Component({
    selector: 'app-search-component',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

    @Select(SearchState.onSearch) onSearch$: Observable<boolean>;
    @Output() searchEvent: EventEmitter<any> = new EventEmitter();

    text: string = "";


    constructor(private store: Store) {};


    ngOnInit(): void {

    };



    search () {
        const path = this.store.selectSnapshot(AppState.path);
        // this.store.dispatch(
        //     new NewSearch(path,this.text)
        // );
    };


    onKeyUp (e) {
        const value = e.target.value;
        this.searchEvent.emit(value);
        // const path = this.store.selectSnapshot(AppState.path);
        // if (this.text === '') {
        //     switch (path) {
        //         case 'subscriptions':
        //             this.store.dispatch(
        //                 new GetSubscriptions()
        //             );
        //             break;
        //         case 'transfers':
        //             this.store.dispatch(
        //                 new GetTransfers()
        //             );
        //             break;
        //         default:
        //             break;
        //     }
        // }
    }


    
}
