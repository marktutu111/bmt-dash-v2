import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { Observable } from 'rxjs';
import { Logout } from 'src/app/states/auth/auth.actions';
import { AuthState } from 'src/app/states/auth/auth.state';
import { IUser } from 'src/app/models/users';
import { AppState } from 'src/app/states/app/state';
import { AccountType } from 'src/app/states/app/actions';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {


    @Select(TransfersState.transferCredits) credits$: Observable<number>;
    @Select(TransfersState.totalTransfers) totalTransfers$: Observable<number>;
    @Select(AuthState.user) user$: Observable<IUser>;
    @Select(AppState.account_type) account_type: Observable<'prepaid' | 'pay_as_you_go' | 'corperate'>;

    constructor (private router: Router, private store: Store, private route: ActivatedRoute) {}

    ngOnInit() {};

    logout () {
      this.store.dispatch(
        new Logout()
      )
    }


    onAccountChange (e) {
      const value: 'prepaid' | 'pay_as_you_go' = e.target.value;
      this.store.dispatch(
        new AccountType(value)
      );

      this.router.navigate(['./send-money'], { relativeTo: this.route });
      

    }

    onReset(){
      this.router.navigate(['./resetpassword']);
      // console.log('You clicked me')
    }


}
