import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import * as moment from "moment";


@Component({
    selector: 'date-filter',
    templateUrl: './date-filter.component.html',
    styleUrls: ['./date-filter.component.scss']
})
export class DateFilterComponent implements OnInit {

    @Output() dateChange: EventEmitter<any> = new EventEmitter();
    startDate: any = moment().format('YYYY-MM-DD');
    endDate: any = moment().format('YYYY-MM-DD');


    constructor() {}

    ngOnInit(): void {}

    
    onStartChange (e) {
        this.startDate = e.target.value;
        this.dateChange.emit([this.startDate,this.endDate]);
    }



    onDateEndChange (e) {
        this.endDate = e.target.value;
        this.dateChange.emit([this.startDate,this.endDate]);

    }



}
