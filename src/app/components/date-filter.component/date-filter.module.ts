import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DateFilterComponent } from './date-filter.component';
import { FormsModule } from '@angular/forms';


@NgModule({
    declarations: [
        DateFilterComponent
    ],
    imports: [ 
        CommonModule,
        FormsModule
    ],
    exports: [
        DateFilterComponent
    ],
    providers: [],
})
export class DateFilterModule {}