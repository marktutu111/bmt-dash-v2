import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CheckImageModel } from "./check-image.model";

@Component({
    selector: 'app-check-image',
    templateUrl: './check-image.component.html',
    styleUrls: ['./check-image.component.scss']
})
export class CheckImageComponent implements OnInit {

    @Input()
    data: CheckImageModel[] = [];

    @Input()
    active: number;

    @Output()
    onChange: EventEmitter<any> = new EventEmitter();

    constructor() {}

    ngOnInit(): void {}


    onClicked (index,name) {
        this.active = index;
        this.onChange.emit(name);
    }


}
