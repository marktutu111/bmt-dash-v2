import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckImageComponent } from './check-image.component';


@NgModule({
    declarations: [
        CheckImageComponent
    ],
    imports: [ CommonModule ],
    exports: [
        CheckImageComponent
    ],
    providers: [],
})
export class CheckImageModule {}