export interface CheckImageModel {
    image: string;
    name: string;
}