import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactsFormComponent } from './contacts-form.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
    declarations: [
        ContactsFormComponent
    ],
    imports: [ 
        CommonModule,
        ReactiveFormsModule
    ],
    exports: [
        ContactsFormComponent
    ],
    providers: [],
})
export class ContactsFormModule {}