import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { Subscription, Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { TransfersState } from 'src/app/states/transfer/transfer.state';
import { GetIssuers } from 'src/app/states/transfer/transfer.actions';


declare const swal: any;

@Component({
    selector: 'app-contacts-form',
    templateUrl: './contacts-form.component.html',
    styleUrls: ['./contacts-form.component.scss']
})
export class ContactsFormComponent implements OnInit {

    @Output() formEvent: EventEmitter<any> = new EventEmitter();
    @Select(TransfersState.issuers) issuers$:Observable<any[]>;

    formGroup: FormGroup;
    private temp: any[] = [];
    private subscription$: Subscription;

    @Input()
    set data (data) {
        setTimeout(() => {
            this.populateTable(data);
        }, 100 / 2);
        
    }

    @Input()
    set search (text) {
        this.onSearch(text);
    }


    constructor (private formBuilder: FormBuilder,private store:Store) {};

    ngOnInit(): void {
        this.formGroup = this.formBuilder.group(
            {
                accounts: this.formBuilder.array([])
            }
        )

        this.subscription$ = this.formGroup.valueChanges.subscribe(({ accounts }) => this.formEvent.emit(accounts));
        this.store.dispatch(
            new GetIssuers()
        )
    };


    get accounts () {
        if (this.formGroup) {
            return this.formGroup.get('accounts') as FormArray;
        }
    }

    clearArray () {
        if (this.accounts) {
            while (this.accounts.length !== 0) {
                this.accounts.removeAt(0);
            }
        }
        
    }

    populateTable (data) {
        data.forEach(d => {{
            const accountForm = this.formBuilder.group(d);
            this.accounts.push(accountForm);
            this.temp.push(d);
            this.temp;
        }})
    }
    
    removeAccount (index: number) {
        // Get row from array
        const account = this.accounts.value.find((account, i) => i === index);
        // Check if all input fields are empty
        const not_empty = Object.keys(account).find(key => account[key] !== null);
        if (typeof not_empty === 'string') {
            return swal(
                {
                    html: '<h4><b>Are you sure want to remove account?</b></h4>',
                    showCancelButton: true,
                    cancelButtonColor: 'red'
                }
            ).then(({ value }) => {
                if (value) {
                    this.accounts.removeAt(index);
                }
            });

        }

        this.accounts.removeAt(index);
        

    }


    onSearch (value) {

        this.clearArray();
        if (value !== '') {
            return this.temp.forEach(account => {
                try {

                    const name = account['name'].toLowerCase();
                    const _value = value.toLowerCase();
                    const number = account['account_number'];
                    const amount = account['amount'];
                    const network = account['account_issuer'];
    
                    if (name.indexOf(_value) > -1 || number.indexOf(_value) > -1 || amount.indexOf(_value) > -1 || network.indexOf(_value) > -1) {
                        const accountForm = this.formBuilder.group(account);
                        this.accounts.push(accountForm);
                    }
                    
                } catch (err) {

                    
                }
            });
        }


        this.temp.forEach(account => {
            const accountForm = this.formBuilder.group(account);
            this.accounts.push(accountForm);
        });

        
    }


    ngOnDestroy(): void {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.subscription$.unsubscribe();
        
    }



}
